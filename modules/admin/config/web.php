<?php


return [
    'components' => [
        'request' => [
            'class' => yii\web\Request::class,
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'asda876kjbasd&^8kjhasd7',
        ],
        'response'   => [
            'class' => yii\web\Response::class,
            'format' => yii\web\Response::FORMAT_HTML
        ],
        'admin' => [
            'class' => yii\web\User::class,
            'identityClass' => \app\modules\admin\models\AdminUser::class,
            'loginUrl' => '/admin/login'
        ],
    ]
];