<?php

namespace app\modules\admin\controllers\traits;

use app\modules\admin\models\forms\BaseForm;
use yii\helpers\Url;
use yii\web\Controller;

/**
 * Trait CreateTrait
 * @package app\modules\admin\controllers
 * @mixin Controller
 */
trait CreateTrait
{
    /**
     * @return string|\yii\web\Response
     * @throws \Exception
     */
    public function actionCreate()
    {
        /** @var BaseForm $model */
        $model = new $this->formModel([
            'objectClass' => $this->objectClass
        ]);
        $request = \Yii::$app->request;

        if ($request->isPost && $model->load($request->post()) && $model->create()) {
            \Yii::$app->session->setFlash('success', 'Запись успешно создана');
            return $this->redirect(Url::to('/' . $this->getUniqueId()));
        } else {
            if ($request->isPost) {
                \Yii::$app->session->setFlash('error', 'Ошибка при создани записи');
            }

            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }
}