<?php

namespace app\modules\admin\controllers\traits;

use app\modules\admin\controllers\AdminBaseController;
use yii\db\ActiveRecordInterface;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;

/**
 * Trait DeleteTrait
 * @package app\modules\admin\controllers\traits
 * @mixin AdminBaseController
 */
trait DeleteTrait
{
    /**
     * @throws NotFoundHttpException
     */
    public function actionDelete()
    {
        /** @var ActiveRecordInterface $item */
        $item = $this->getRequestedObject(\Yii::$app->request->get('id'));

        try {
            $item->delete();
            \Yii::$app->session->setFlash('success', 'Запись удалена');
            return $this->redirect(Url::to('/' . $this->getUniqueId()));
        } catch (\Exception $e) {
            \Yii::$app->session->setFlash('error', 'Ошибка при удалении записи');
            return $this->redirect(Url::to('/' . $this->getUniqueId()));
        }
    }
}