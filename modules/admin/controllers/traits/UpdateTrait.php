<?php

namespace app\modules\admin\controllers\traits;

use app\modules\admin\controllers\AdminBaseController;
use yii\base\Model;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;

/**
 * Trait UpdateTrait
 * @package app\modules\admin\controllers
 * @mixin AdminBaseController
 */
trait UpdateTrait
{
    /**
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionEdit()
    {
        $item = $this->getRequestedObject(\Yii::$app->request->get('id'));

        /** @var Model $model */
        $model = new $this->formModel();
        $model->import($item);
        $request = \Yii::$app->request;

        if ($request->isPost && $model->load($request->post()) && $model->edit($item)) {
            \Yii::$app->session->setFlash('success', 'Изменения сохранены');
            return $this->redirect(Url::to('/' . $this->getUniqueId()));
        } else {
            if ($request->isPost) {
                \Yii::$app->session->setFlash('error', 'Ошибка при редактировании записи');
            }

            return $this->render('edit', [
                'model' => $model
            ]);
        }
    }
}