<?php

namespace app\modules\admin\controllers;

use app\modules\admin\controllers\traits\CreateTrait;
use app\modules\admin\controllers\traits\DeleteTrait;
use app\modules\admin\controllers\traits\UpdateTrait;
use app\modules\admin\models\forms\NewsForm;
use app\models\db\News;
use app\modules\admin\models\search\BaseSearch;
use app\modules\admin\models\search\NewsImageSearch;
use yii\web\NotFoundHttpException;

class NewsController extends AdminBaseController
{
    use CreateTrait, DeleteTrait, UpdateTrait;

    public $formModel   = NewsForm::class;
    public $objectClass = News::class;
    public $searchModel = BaseSearch::class;

    /**
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionGallery($id)
    {
        if (!$news = News::findOne(['id' => $id])) {
            throw new NotFoundHttpException('Не найдена новость');
        }

        $searchModel = new NewsImageSearch([
            'id_news' => $news->id
        ]);
        $dataProvider = $searchModel->search(\Yii::$app->request->get());

        return $this->render('gallery', [
            'news' => $news,
            'dataProvider' => $dataProvider,
        ]);
    }
}