<?php

namespace app\modules\admin\controllers;

use app\models\db\Department;
use app\modules\admin\controllers\traits\CreateTrait;
use app\modules\admin\controllers\traits\DeleteTrait;
use app\modules\admin\controllers\traits\UpdateTrait;
use app\modules\admin\models\forms\DepartmentForm;
use app\modules\admin\models\search\BaseSearch;
use app\modules\admin\models\search\StaffSearch;
use yii\web\NotFoundHttpException;

class DepartmentController extends AdminBaseController
{
    public $searchModel = BaseSearch::class;
    public $formModel = DepartmentForm::class;
    public $objectClass = Department::class;

    use CreateTrait, DeleteTrait, UpdateTrait;

    /**
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     * @throws \yii\base\InvalidConfigException
     */
    public function actionStaff($id)
    {
        if (!$department = Department::findOne(['id' => $id])) {
            throw new NotFoundHttpException('Не найден отдел');
        }

        $searchModel = new StaffSearch([
            'id_department' => $department->id
        ]);
        $dataProvider = $searchModel->search(\Yii::$app->request->get());

        return $this->render('staff', [
            'department' => $department,
            'dataProvider' => $dataProvider,
        ]);
    }
}