<?php

namespace app\modules\admin\controllers;

use app\models\db\MapItem;
use app\modules\admin\controllers\traits\CreateTrait;
use app\modules\admin\controllers\traits\DeleteTrait;
use app\modules\admin\controllers\traits\UpdateTrait;
use app\modules\admin\models\forms\MapItemForm;
use app\modules\admin\models\search\BaseSearch;

class MapItemController extends AdminBaseController
{
    public $searchModel = BaseSearch::class;
    public $formModel = MapItemForm::class;
    public $objectClass = MapItem::class;

    use CreateTrait, DeleteTrait, UpdateTrait;

}