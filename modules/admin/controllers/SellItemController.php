<?php

namespace app\modules\admin\controllers;

use app\models\db\SellItem;
use app\modules\admin\controllers\traits\CreateTrait;
use app\modules\admin\controllers\traits\DeleteTrait;
use app\modules\admin\controllers\traits\UpdateTrait;
use app\modules\admin\models\forms\SellItemForm;
use app\modules\admin\models\search\BaseSearch;

class SellItemController extends AdminBaseController
{
    public $searchModel = BaseSearch::class;
    public $formModel = SellItemForm::class;
    public $objectClass = SellItem::class;

    use CreateTrait, DeleteTrait, UpdateTrait;

}