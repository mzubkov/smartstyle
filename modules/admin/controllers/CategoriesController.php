<?php

namespace app\modules\admin\controllers;

use app\modules\admin\controllers\traits\CreateTrait;
use app\modules\admin\controllers\traits\DeleteTrait;
use app\modules\admin\controllers\traits\UpdateTrait;
use app\modules\admin\models\Category;
use app\modules\admin\models\forms\CategoryForm;
use app\modules\admin\models\search\CategorySearch;

class CategoriesController extends AdminBaseController
{
    public $searchModel = CategorySearch::class;
    public $formModel = CategoryForm::class;
    public $objectClass = Category::class;

    use CreateTrait, DeleteTrait, UpdateTrait;
}