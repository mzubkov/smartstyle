<?php

namespace app\modules\admin\controllers;

use app\models\db\CarMap;
use app\models\db\CarModel;
use app\modules\admin\models\forms\CarMapForm;
use app\modules\admin\models\search\BaseSearch;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;

class CarMapController extends AdminBaseController
{
    public $searchModel = BaseSearch::class;
    public $formModel = CarMapForm::class;
    public $objectClass = CarMap::class;

    /**
     * @param $id
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionCreate($id)
    {
        if (!$carModel = CarModel::findOne(['id' => $id])) {
            throw new NotFoundHttpException('Не найдена модель автомобиля');
        }

        $model = new CarMapForm([
            'objectClass' => $this->objectClass,
            'id_model' => $carModel->id
        ]);
        $request = \Yii::$app->request;

        if ($request->isPost && $model->load($request->post()) && $model->create()) {
            \Yii::$app->session->setFlash('success', 'Запись успешно создана');
            return $this->redirect(Url::to(['maps/files', 'id' => $carModel->id]));
        } else {
            if ($request->isPost) {
                \Yii::$app->session->setFlash('error', 'Ошибка при создани записи');
            }

            return $this->render('create', [
                'model' => $model,
                'brand' => $carModel->brand,
                'carModel' => $carModel
            ]);
        }
    }

    /**
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionEdit()
    {
        /** @var CarMap $item */
        $item = $this->getRequestedObject(\Yii::$app->request->get('id'));

        $model = new CarMapForm(['id_model' => $item->id_model]);
        $model->import($item);
        $request = \Yii::$app->request;

        if ($request->isPost && $model->load($request->post()) && $model->edit($item)) {
            \Yii::$app->session->setFlash('success', 'Изменения сохранены');
            return $this->redirect(Url::to(['maps/files', 'id' => $item->id_model]));
        } else {
            if ($request->isPost) {
                \Yii::$app->session->setFlash('error', 'Ошибка при редактировании записи');
            }

            return $this->render('edit', [
                'model' => $model,
                'carModel' => $item->model,
                'brand' => $item->model->brand
            ]);
        }
    }

    /**
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     * @throws \Throwable
     */
    public function actionDelete()
    {
        /** @var CarMap $item */
        $item = $this->getRequestedObject(\Yii::$app->request->get('id'));

        try {
            $item->delete();
            \Yii::$app->session->setFlash('success', 'Запись удалена');
            return $this->redirect(Url::to(['maps/files', 'id' => $item->id_model]));
        } catch (\Exception $e) {
            \Yii::$app->session->setFlash('error', 'Ошибка при удалении записи');
            return $this->redirect(Url::to(['maps/files', 'id' => $item->id_model]));
        }
    }
}