<?php

namespace app\modules\admin\controllers;

use app\modules\admin\AdminModule;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use app\modules\admin\models\LoginForm;

class AdminController extends Controller
{
    /** @var AdminModule */
    public $module;

    /** @var string  */
    public $layout = 'main';

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
                'user' => $this->module->admin
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * @return string
     */
    public function actionError()
    {
        $exception = Yii::$app->errorHandler->exception;
        return $this->render('error', [
            'name' => 'Ошибка',
            'message' => $exception->getMessage()
        ]);
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        $user = $this->module->admin;
        if (!$user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->renderPartial('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        $this->module->admin->logout();

        return $this->goHome();
    }

    /**
     * @param null $defaultUrl
     * @return \yii\console\Response|Response
     */
    public function goBack($defaultUrl = null)
    {
        return Yii::$app->getResponse()->redirect($this->module->admin->getReturnUrl($defaultUrl));
    }
}
