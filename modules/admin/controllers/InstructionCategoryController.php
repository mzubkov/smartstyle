<?php

namespace app\modules\admin\controllers;

use app\models\db\InstructionCategory;
use app\modules\admin\models\forms\BaseForm;
use app\modules\admin\models\forms\InstructionCategoryForm;
use yii\base\Model;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;

class InstructionCategoryController extends AdminBaseController
{
    public $formModel = InstructionCategoryForm::class;
    public $objectClass = InstructionCategory::class;

    /**
     * @param $id
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionCreate()
    {
        $id = \Yii::$app->request->get('id');
        if ($id) {
            if (!$category = InstructionCategory::findOne(['id' => $id])) {
                throw new NotFoundHttpException('Не найден раздел');
            }

            /** @var BaseForm $model */
            $model = new InstructionCategoryForm([
                'objectClass' => $this->objectClass,
                'id_parent' => $category->id
            ]);
        } else {
            /** @var BaseForm $model */
            $model = new InstructionCategoryForm([
                'objectClass' => $this->objectClass
            ]);

            $category = null;
        }

        $request = \Yii::$app->request;

        if ($request->isPost && $model->load($request->post()) && $model->create()) {
            \Yii::$app->session->setFlash('success', 'Запись успешно создана');
            if (!$category) {
                return $this->redirect(Url::to(['instructions/index']));
            } else {
                return $this->redirect(Url::to(['instructions/category', 'id' => $category->id]));
            }

        } else {
            if ($request->isPost) {
                \Yii::$app->session->setFlash('error', 'Ошибка при создани записи');
            }

            return $this->render('create', [
                'model' => $model,
                'category' => $category
            ]);
        }
    }

    /**
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionEdit()
    {
        /** @var InstructionCategory $item */
        $item = $this->getRequestedObject(\Yii::$app->request->get('id'));

        /** @var Model $model */
        $model = new InstructionCategoryForm(['id_parent' => $item->id_parent]);
        $model->import($item);
        $request = \Yii::$app->request;

        if ($request->isPost && $model->load($request->post()) && $model->edit($item)) {
            \Yii::$app->session->setFlash('success', 'Изменения сохранены');
            if (!$item->id_parent) {
                return $this->redirect(Url::to(['instructions/index']));
            } else {
                return $this->redirect(Url::to(['instructions/category', 'id' => $item->id_parent]));
            }
        } else {
            if ($request->isPost) {
                \Yii::$app->session->setFlash('error', 'Ошибка при редактировании записи');
            }

            return $this->render('edit', [
                'model' => $model,
                'category' => $item->parent
            ]);
        }
    }

    /**
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     * @throws \Throwable
     */
    public function actionDelete()
    {
        /** @var InstructionCategory $item */
        $item = $this->getRequestedObject(\Yii::$app->request->get('id'));

        try {
            $item->delete();
            \Yii::$app->session->setFlash('success', 'Запись удалена');
        } catch (\Exception $e) {
            \Yii::$app->session->setFlash('error', 'Ошибка при удалении записи');
        }

        if (!$item->id_parent) {
            return $this->redirect(Url::to(['instructions/index']));
        } else {
            return $this->redirect(Url::to(['instructions/category', 'id' => $item->id_parent]));
        }

    }
}