<?php

namespace app\modules\admin\controllers;

use app\models\db\Admin;
use app\modules\admin\controllers\traits\CreateTrait;
use app\modules\admin\controllers\traits\DeleteTrait;
use app\modules\admin\models\forms\EditProfileForm;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;
use yii\helpers\Url;

class ProfileController extends AdminBaseController
{
    use CreateTrait, DeleteTrait;

    public $formModel   = EditProfileForm::class;
    public $objectClass = Admin::class;

    public function actionIndex()
    {

    }

    /**
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException
     * @throws \Throwable
     */
    public function actionEdit()
    {
        /** @var Admin $item */
        $item = $this->getRequestedObject($this->module->admin->identity->getId());

        $model = new EditProfileForm();
        $request = \Yii::$app->request;

        if ($request->isPost){
            $file = UploadedFile::getInstance($model, 'file');
            if ($model->load($request->post(), null, $file) && $model->save($item)) {
                \Yii::$app->session->setFlash('success', 'Изменения сохранены.');
                return $this->redirect(Url::to('/admin/news'));
            } else {
                \Yii::$app->session->setFlash('error', 'Ошибка при создании записи:<br>' . $model->error);
            }
        } else {
            $model->fillFromDB($item->getAttributes());
        }
        return $this->render('edit', [
            'model' => $model,
        ]);
    }
}