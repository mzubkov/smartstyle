<?php

namespace app\modules\admin\controllers;

use app\models\db\InstructionCategory;
use app\models\db\InstructionItem;
use app\modules\admin\models\search\InstructionCategorySearch;
use app\modules\admin\models\search\InstructionFileSearch;
use app\modules\admin\models\search\InstructionItemSearch;
use yii\web\NotFoundHttpException;

class InstructionsController extends AdminBaseController
{
    public $searchModel = InstructionCategorySearch::class;
    public $objectClass = InstructionCategory::class;

    /**
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new InstructionCategorySearch([
            'id_parent' => null
        ]);
        $dataProvider = $searchModel->search(\Yii::$app->request->get());
        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'category' => null,
        ]);
    }


    /**
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionCategory($id)
    {
        if (!$category = InstructionCategory::findOne(['id' => $id])) {
            throw new NotFoundHttpException('Не найдена категория');
        }

        $searchModel = new InstructionCategorySearch([
            'id_parent' => $category->id
        ]);
        $dataProvider = $searchModel->search(\Yii::$app->request->get());
        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'category' => $category
        ]);
    }

    /**
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     * @throws \yii\base\InvalidConfigException
     */
    public function actionItems($id)
    {
        if (!$category = InstructionCategory::findOne(['id' => $id])) {
            throw new NotFoundHttpException('Не найдена категория');
        }

        $searchModel = new InstructionItemSearch([
            'id_category' => $category->id
        ]);
        $dataProvider = $searchModel->search(\Yii::$app->request->get());

        return $this->render('items', [
            'category' => $category,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionFiles($id)
    {
        if (!$item = InstructionItem::findOne(['id' => $id])) {
            throw new NotFoundHttpException('Не найден продукт');
        }

        $searchModel = new InstructionFileSearch([
            'id_item' => $item->id
        ]);
        $dataProvider = $searchModel->search(\Yii::$app->request->get());

        return $this->render('files', [
            'category' => $item->category,
            'item' => $item,
            'dataProvider' => $dataProvider,
        ]);
    }
}