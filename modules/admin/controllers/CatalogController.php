<?php

namespace app\modules\admin\controllers;

use app\models\db\Brand;
use app\models\db\Catalog;
use app\modules\admin\controllers\traits\CreateTrait;
use app\modules\admin\controllers\traits\DeleteTrait;
use app\modules\admin\controllers\traits\UpdateTrait;
use app\modules\admin\models\forms\BaseForm;
use app\modules\admin\models\forms\CatalogForm;
use yii\base\Model;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;

class CatalogController extends AdminBaseController
{
    public $formModel = CatalogForm::class;
    public $objectClass = Catalog::class;

    use CreateTrait, DeleteTrait, UpdateTrait;

    /**
     * @return string
     */
    public function actionIndex()
    {
        $this->redirect(Url::to(['brand/index']));
    }

    /**
     * @param $id
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionCreate($id)
    {
        if (!$brand = Brand::findOne(['id' => $id])) {
            throw new NotFoundHttpException('Не найден бренд');
        }

        /** @var BaseForm $model */
        $model = new CatalogForm([
            'objectClass' => $this->objectClass,
            'id_brand' => $brand->id
        ]);
        $request = \Yii::$app->request;

        if ($request->isPost && $model->load($request->post()) && $model->create()) {
            \Yii::$app->session->setFlash('success', 'Запись успешно создана');
            return $this->redirect(Url::to(['brand/catalog', 'id' => $brand->id]));
        } else {
            if ($request->isPost) {
                \Yii::$app->session->setFlash('error', 'Ошибка при создани записи');
            }

            return $this->render('create', [
                'model' => $model,
                'brand' => $brand
            ]);
        }
    }

    /**
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionEdit()
    {
        /** @var Catalog $item */
        $item = $this->getRequestedObject(\Yii::$app->request->get('id'));

        /** @var Model $model */
        $model = new CatalogForm(['id_brand' => $item->id_brand]);
        $model->import($item);
        $request = \Yii::$app->request;

        if ($request->isPost && $model->load($request->post()) && $model->edit($item)) {
            \Yii::$app->session->setFlash('success', 'Изменения сохранены');
            return $this->redirect(Url::to(['brand/catalog', 'id' => $item->id_brand]));
        } else {
            if ($request->isPost) {
                \Yii::$app->session->setFlash('error', 'Ошибка при редактировании записи');
            }

            return $this->render('edit', [
                'model' => $model,
                'brand' => $item->brand
            ]);
        }
    }

    /**
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     * @throws \Throwable
     */
    public function actionDelete()
    {
        /** @var Catalog $item */
        $item = $this->getRequestedObject(\Yii::$app->request->get('id'));

        try {
            $item->delete();
            \Yii::$app->session->setFlash('success', 'Запись удалена');
            return $this->redirect(Url::to(['brand/catalog', 'id' => $item->id_brand]));
        } catch (\Exception $e) {
            \Yii::$app->session->setFlash('error', 'Ошибка при удалении записи');
            return $this->redirect(Url::to(['brand/catalog', 'id' => $item->id_brand]));
        }
    }
}