<?php

namespace app\modules\admin\controllers;

use app\models\db\InstructionFile;
use app\models\db\CarModel;
use app\models\db\InstructionItem;
use app\modules\admin\models\forms\InstructionFileForm;
use app\modules\admin\models\search\BaseSearch;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;

class InstructionFileController extends AdminBaseController
{
    public $searchModel = BaseSearch::class;
    public $formModel = InstructionFileForm::class;
    public $objectClass = InstructionFile::class;

    /**
     * @param $id
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionCreate($id)
    {
        if (!$item = InstructionItem::findOne(['id' => $id])) {
            throw new NotFoundHttpException('Не найден продукт');
        }

        $model = new InstructionFileForm([
            'objectClass' => $this->objectClass,
            'id_item' => $item->id
        ]);
        $request = \Yii::$app->request;

        if ($request->isPost && $model->load($request->post()) && $model->create()) {
            \Yii::$app->session->setFlash('success', 'Запись успешно создана');
            return $this->redirect(Url::to(['instructions/files', 'id' => $item->id]));
        } else {
            if ($request->isPost) {
                \Yii::$app->session->setFlash('error', 'Ошибка при создани записи');
            }

            return $this->render('create', [
                'model' => $model,
                'category' => $item->category,
                'item' => $item
            ]);
        }
    }

    /**
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionEdit()
    {
        /** @var InstructionFile $file */
        $file = $this->getRequestedObject(\Yii::$app->request->get('id'));

        $model = new InstructionFileForm(['id_item' => $file->id_item]);
        $model->import($file);
        $request = \Yii::$app->request;

        if ($request->isPost && $model->load($request->post()) && $model->edit($file)) {
            \Yii::$app->session->setFlash('success', 'Изменения сохранены');
            return $this->redirect(Url::to(['instructions/files', 'id' => $file->id_item]));
        } else {
            if ($request->isPost) {
                \Yii::$app->session->setFlash('error', 'Ошибка при редактировании записи');
            }

            return $this->render('edit', [
                'model' => $model,
                'category' => $file->item->category,
                'item' => $file->item
            ]);
        }
    }

    /**
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     * @throws \Throwable
     */
    public function actionDelete()
    {
        /** @var InstructionFile $file */
        $file = $this->getRequestedObject(\Yii::$app->request->get('id'));

        try {
            $file->delete();
            \Yii::$app->session->setFlash('success', 'Запись удалена');
        } catch (\Exception $e) {
            \Yii::$app->session->setFlash('error', 'Ошибка при удалении записи');
        }

        return $this->redirect(Url::to(['instructions/files', 'id' => $file->id_item]));
    }
}