<?php

namespace app\modules\admin\controllers;

use app\models\db\Brand;
use app\models\db\BrandVideo;
use app\modules\admin\controllers\traits\CreateTrait;
use app\modules\admin\controllers\traits\DeleteTrait;
use app\modules\admin\controllers\traits\UpdateTrait;
use app\modules\admin\models\forms\BrandVideoForm;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;

class BrandVideoController extends AdminBaseController
{
    public $formModel = BrandVideoForm::class;
    public $objectClass = BrandVideo::class;

    use CreateTrait, DeleteTrait, UpdateTrait;

    /**
     * @return string
     */
    public function actionIndex()
    {
        $this->redirect(Url::to(['brand/index']));
    }

    /**
     * @param $id
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionCreate($id)
    {
        if (!$brand = Brand::findOne(['id' => $id])) {
            throw new NotFoundHttpException('Не найден бренд');
        }

        /** @var BrandVideoForm $model */
        $model = new BrandVideoForm([
            'objectClass' => $this->objectClass,
            'id_brand' => $brand->id
        ]);
        $request = \Yii::$app->request;

        if ($request->isPost && $model->load($request->post()) && $model->create()) {
            \Yii::$app->session->setFlash('success', 'Запись успешно создана');
            return $this->redirect(Url::to(['brand/video', 'id' => $brand->id]));
        } else {
            if ($request->isPost) {
                \Yii::$app->session->setFlash('error', 'Ошибка при создани записи');
            }

            return $this->render('create', [
                'model' => $model,
                'brand' => $brand
            ]);
        }
    }

    /**
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionEdit()
    {
        /** @var BrandVideo $video */
        $video = $this->getRequestedObject(\Yii::$app->request->get('id'));

        /** @var BrandVideoForm $model */
        $model = new BrandVideoForm(['id_brand' => $video->id_brand]);
        $model->import($video);
        $model->video_code = $video->getUrl();
        $request = \Yii::$app->request;

        if ($request->isPost && $model->load($request->post()) && $model->edit($video)) {
            \Yii::$app->session->setFlash('success', 'Изменения сохранены');
            return $this->redirect(Url::to(['brand/video', 'id' => $video->id_brand]));
        } else {
            if ($request->isPost) {
                \Yii::$app->session->setFlash('error', 'Ошибка при редактировании записи');
            }

            return $this->render('edit', [
                'model' => $model,
                'brand' => $video->brand
            ]);
        }
    }

    /**
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     * @throws \Throwable
     */
    public function actionDelete()
    {
        /** @var BrandVideo $video */
        $video = $this->getRequestedObject(\Yii::$app->request->get('id'));

        try {
            $video->delete();
            \Yii::$app->session->setFlash('success', 'Запись удалена');
        } catch (\Exception $e) {
            \Yii::$app->session->setFlash('error', 'Ошибка при удалении записи');
        }

        return $this->redirect(Url::to(['brand/video', 'id' => $video->id_brand]));
    }
}