<?php

namespace app\modules\admin\controllers;

use app\models\db\CarBrand;
use app\modules\admin\controllers\traits\CreateTrait;
use app\modules\admin\controllers\traits\DeleteTrait;
use app\modules\admin\controllers\traits\UpdateTrait;
use app\modules\admin\models\forms\CarBrandForm;
use app\modules\admin\models\search\BaseSearch;
use yii\helpers\Url;

class CarBrandController extends AdminBaseController
{
    public $searchModel = BaseSearch::class;
    public $formModel = CarBrandForm::class;
    public $objectClass = CarBrand::class;

    use CreateTrait, DeleteTrait, UpdateTrait;

    /**
     * @return string|\yii\web\Response
     */
    public function actionIndex()
    {
        return $this->redirect(Url::to(['maps/index']));
    }
}