<?php

namespace app\modules\admin\controllers;

use app\models\db\CarBrand;
use app\models\db\CarModel;
use app\modules\admin\models\search\BaseSearch;
use app\modules\admin\models\search\CarMapSearch;
use app\modules\admin\models\search\CarModelSearch;
use yii\web\NotFoundHttpException;

class MapsController extends AdminBaseController
{
    public $searchModel = BaseSearch::class;
    public $objectClass = CarBrand::class;

    /**
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     * @throws \yii\base\InvalidConfigException
     */
    public function actionModels($id)
    {
        if (!$brand = CarBrand::findOne(['id' => $id])) {
            throw new NotFoundHttpException('Не найдена марка автомобиля');
        }

        $searchModel = new CarModelSearch([
            'id_brand' => $brand->id
        ]);
        $dataProvider = $searchModel->search(\Yii::$app->request->get());

        return $this->render('models', [
            'brand' => $brand,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionFiles($id)
    {
        if (!$model = CarModel::findOne(['id' => $id])) {
            throw new NotFoundHttpException('Не найдена модель автомобиля');
        }

        $searchModel = new CarMapSearch([
            'id_model' => $model->id
        ]);
        $dataProvider = $searchModel->search(\Yii::$app->request->get());

        return $this->render('files', [
            'brand' => $model->brand,
            'model' => $model,
            'dataProvider' => $dataProvider,
        ]);
    }
}