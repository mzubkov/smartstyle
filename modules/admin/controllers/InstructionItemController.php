<?php

namespace app\modules\admin\controllers;

use app\models\db\InstructionCategory;
use app\models\db\InstructionItem;
use app\modules\admin\models\forms\InstructionItemForm;
use app\modules\admin\models\search\BaseSearch;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;

class InstructionItemController extends AdminBaseController
{
    public $searchModel = BaseSearch::class;
    public $formModel = InstructionItemForm::class;
    public $objectClass = InstructionItem::class;

    /**
     * @param $id
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionCreate($id)
    {
        if (!$category = InstructionCategory::findOne(['id' => $id])) {
            throw new NotFoundHttpException('Не найдена категория');
        }

        $model = new InstructionItemForm([
            'objectClass' => $this->objectClass,
            'id_category' => $category->id
        ]);
        $request = \Yii::$app->request;

        if ($request->isPost && $model->load($request->post()) && $model->create()) {
            \Yii::$app->session->setFlash('success', 'Запись успешно создана');
            return $this->redirect(Url::to(['instructions/items', 'id' => $category->id]));
        } else {
            if ($request->isPost) {
                \Yii::$app->session->setFlash('error', 'Ошибка при создани записи');
            }

            return $this->render('create', [
                'model' => $model,
                'category' => $category
            ]);
        }
    }

    /**
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionEdit()
    {
        /** @var InstructionItem $item */
        $item = $this->getRequestedObject(\Yii::$app->request->get('id'));

        $model = new InstructionItemForm(['id_category' => $item->id_category]);
        $model->import($item);
        $request = \Yii::$app->request;

        if ($request->isPost && $model->load($request->post()) && $model->edit($item)) {
            \Yii::$app->session->setFlash('success', 'Изменения сохранены');
            return $this->redirect(Url::to(['instructions/items', 'id' => $item->id_category]));
        } else {
            if ($request->isPost) {
                \Yii::$app->session->setFlash('error', 'Ошибка при редактировании записи');
            }

            return $this->render('edit', [
                'model' => $model,
                'category' => $item->category
            ]);
        }
    }

    /**
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     * @throws \Throwable
     */
    public function actionDelete()
    {
        /** @var InstructionItem $item */
        $item = $this->getRequestedObject(\Yii::$app->request->get('id'));

        try {
            $item->delete();
            \Yii::$app->session->setFlash('success', 'Запись удалена');
        } catch (\Exception $e) {
            \Yii::$app->session->setFlash('error', 'Ошибка при удалении записи');
        }

        return $this->redirect(Url::to(['instructions/items', 'id' => $item->id_category]));
    }
}