<?php

namespace app\modules\admin\controllers;

use app\models\db\WorkItem;
use app\modules\admin\controllers\traits\CreateTrait;
use app\modules\admin\controllers\traits\DeleteTrait;
use app\modules\admin\controllers\traits\UpdateTrait;
use app\modules\admin\models\forms\WorkItemForm;
use app\modules\admin\models\search\BaseSearch;

class WorkItemController extends AdminBaseController
{
    public $searchModel = BaseSearch::class;
    public $formModel = WorkItemForm::class;
    public $objectClass = WorkItem::class;

    use CreateTrait, DeleteTrait, UpdateTrait;

}