<?php

namespace app\modules\admin\controllers;

use app\modules\admin\controllers\traits\CreateTrait;
use app\modules\admin\controllers\traits\DeleteTrait;
use app\modules\admin\controllers\traits\UpdateTrait;
use app\modules\admin\models\AdminUser;
use app\modules\admin\models\forms\UsersForm;
use app\modules\admin\models\search\UsersSearch;

class UsersController extends AdminBaseController
{
    public $searchModel = UsersSearch::class;
    public $formModel = UsersForm::class;
    public $objectClass = AdminUser::class;

    use CreateTrait, DeleteTrait, UpdateTrait;

    /**
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new $this->searchModel();
        $dataProvider = $searchModel->search(\Yii::$app->request->get());
        return $this->render('index', [
            'dataProvider' => $dataProvider
        ]);
    }
}