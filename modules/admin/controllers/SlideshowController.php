<?php

namespace app\modules\admin\controllers;

use app\models\db\Slideshow;
use app\modules\admin\controllers\traits\CreateTrait;
use app\modules\admin\controllers\traits\DeleteTrait;
use app\modules\admin\controllers\traits\UpdateTrait;
use app\modules\admin\models\forms\SlideshowForm;
use app\modules\admin\models\search\BaseSearch;

class SlideshowController extends AdminBaseController
{
    public $searchModel = BaseSearch::class;
    public $formModel = SlideshowForm::class;
    public $objectClass = Slideshow::class;

    use CreateTrait, DeleteTrait, UpdateTrait;

}