<?php

namespace app\modules\admin\controllers;

use app\models\db\TextBlock;
use app\modules\admin\controllers\traits\UpdateTrait;
use app\modules\admin\models\forms\TextBlockForm;
use app\modules\admin\models\search\TextBlockSearch;
use yii\base\Model;
use yii\db\ActiveRecordInterface;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;

class TextBlockController extends AdminBaseController
{
    public $searchModel = TextBlockSearch::class;
    public $formModel = TextBlockForm::class;
    public $objectClass = TextBlock::class;

    use UpdateTrait;

    /**
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new $this->searchModel();
        $dataProvider = $searchModel->search(\Yii::$app->request->get());
        return $this->render('index', [
            'dataProvider' => $dataProvider,
            //'searchModel' => isset($searchModel) ? $searchModel : null,
        ]);
    }

    /**
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionEdit()
    {
        $item = $this->getRequestedObject(\Yii::$app->request->get('id'));

        /** @var Model $model */
        $model = new $this->formModel();
        $model->setAttributes($item->getAttributes());
        $request = \Yii::$app->request;

        if ($request->isPost && $model->load($request->post()) && $model->edit($item)) {
            \Yii::$app->session->setFlash('success', 'Изменения сохранены');
            return $this->redirect(Url::to('/' . $this->getUniqueId()));
        } else {
            if ($request->isPost) {
                \Yii::$app->session->setFlash('error', 'Ошибка при редактировании записи');
            }

            return $this->render('edit', [
                'model' => $model
            ]);
        }
    }

    /**
     * @param $id
     * @return mixed|ActiveRecordInterface
     * @throws NotFoundHttpException
     */
    protected function getRequestedObject($id)
    {
        if (!$id || !$item = TextBlock::findOne(['identifier' => $id])) {
            $blocks = $blocks = require_once(\Yii::getAlias('@app/config/settings/text_blocks.php'));
            if (!isset($blocks[$id])) {
                throw new NotFoundHttpException();
            }

            $item = new TextBlock([
                'identifier' => $id,
                'name'  => $blocks[$id]['name'],
                'content' => $blocks[$id]['content'],
                'column_type' => $blocks[$id]['column_type']
            ]);
        }

        return $item;
    }
}