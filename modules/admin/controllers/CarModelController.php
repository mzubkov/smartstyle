<?php

namespace app\modules\admin\controllers;

use app\models\db\CarBrand;
use app\models\db\CarModel;
use app\modules\admin\models\forms\CarModelForm;
use app\modules\admin\models\search\BaseSearch;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;

class CarModelController extends AdminBaseController
{
    public $searchModel = BaseSearch::class;
    public $formModel = CarModelForm::class;
    public $objectClass = CarModel::class;

    /**
     * @param $id
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionCreate($id)
    {
        if (!$brand = CarBrand::findOne(['id' => $id])) {
            throw new NotFoundHttpException('Не найдена марка автомобиля');
        }

        $model = new CarModelForm([
            'objectClass' => $this->objectClass,
            'id_brand' => $brand->id
        ]);
        $request = \Yii::$app->request;

        if ($request->isPost && $model->load($request->post()) && $model->create()) {
            \Yii::$app->session->setFlash('success', 'Запись успешно создана');
            return $this->redirect(Url::to(['maps/models', 'id' => $brand->id]));
        } else {
            if ($request->isPost) {
                \Yii::$app->session->setFlash('error', 'Ошибка при создани записи');
            }

            return $this->render('create', [
                'model' => $model,
                'brand' => $brand
            ]);
        }
    }

    /**
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionEdit()
    {
        /** @var CarModel $item */
        $item = $this->getRequestedObject(\Yii::$app->request->get('id'));

        $model = new CarModelForm(['id_brand' => $item->id_brand]);
        $model->import($item);
        $request = \Yii::$app->request;

        if ($request->isPost && $model->load($request->post()) && $model->edit($item)) {
            \Yii::$app->session->setFlash('success', 'Изменения сохранены');
            return $this->redirect(Url::to(['maps/models', 'id' => $item->id_brand]));
        } else {
            if ($request->isPost) {
                \Yii::$app->session->setFlash('error', 'Ошибка при редактировании записи');
            }

            return $this->render('edit', [
                'model' => $model,
                'brand' => $item->brand
            ]);
        }
    }

    /**
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     * @throws \Throwable
     */
    public function actionDelete()
    {
        /** @var CarModel $item */
        $item = $this->getRequestedObject(\Yii::$app->request->get('id'));

        try {
            $item->delete();
            \Yii::$app->session->setFlash('success', 'Запись удалена');
            return $this->redirect(Url::to(['maps/models', 'id' => $item->id_brand]));
        } catch (\Exception $e) {
            \Yii::$app->session->setFlash('error', 'Ошибка при удалении записи');
            return $this->redirect(Url::to(['maps/models', 'id' => $item->id_brand]));
        }
    }
}