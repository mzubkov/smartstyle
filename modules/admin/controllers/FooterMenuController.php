<?php

namespace app\modules\admin\controllers;

use app\models\db\FooterMenuFolder;
use app\modules\admin\controllers\traits\CreateTrait;
use app\modules\admin\controllers\traits\DeleteTrait;
use app\modules\admin\controllers\traits\UpdateTrait;
use app\modules\admin\models\forms\FooterMenuFolderForm;
use app\modules\admin\models\search\BaseSearch;
use app\modules\admin\models\search\FooterMenuItemSearch;
use yii\web\NotFoundHttpException;

class FooterMenuController extends AdminBaseController
{
    public $searchModel = BaseSearch::class;
    public $formModel = FooterMenuFolderForm::class;
    public $objectClass = FooterMenuFolder::class;

    use CreateTrait, DeleteTrait, UpdateTrait;

    /**
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     * @throws \yii\base\InvalidConfigException
     */
    public function actionItems($id)
    {
        if (!$folder = FooterMenuFolder::findOne(['id' => $id])) {
            throw new NotFoundHttpException('Не найден раздел');
        }

        $searchModel = new FooterMenuItemSearch([
            'id_folder' => $folder->id
        ]);
        $dataProvider = $searchModel->search(\Yii::$app->request->get());

        return $this->render('items', [
            'folder' => $folder,
            'dataProvider' => $dataProvider,
        ]);
    }
}