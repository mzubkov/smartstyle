<?php

namespace app\modules\admin\controllers;

use app\models\db\Partner;
use app\modules\admin\controllers\traits\CreateTrait;
use app\modules\admin\controllers\traits\DeleteTrait;
use app\modules\admin\controllers\traits\UpdateTrait;
use app\modules\admin\models\forms\PartnerForm;
use app\modules\admin\models\search\BaseSearch;

class PartnerController extends AdminBaseController
{
    public $searchModel = BaseSearch::class;
    public $formModel = PartnerForm::class;
    public $objectClass = Partner::class;

    use CreateTrait, DeleteTrait, UpdateTrait;

}