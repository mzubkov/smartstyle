<?php

namespace app\modules\admin\controllers;

use app\models\db\Navigation;
use app\modules\admin\controllers\traits\CreateTrait;
use app\modules\admin\controllers\traits\DeleteTrait;
use app\modules\admin\controllers\traits\UpdateTrait;
use app\modules\admin\models\forms\NavigationForm;
use app\modules\admin\models\search\BaseSearch;

class NavigationController extends AdminBaseController
{
    public $searchModel = BaseSearch::class;
    public $formModel = NavigationForm::class;
    public $objectClass = Navigation::class;

    use CreateTrait, DeleteTrait, UpdateTrait;

}