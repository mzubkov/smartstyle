<?php

namespace app\modules\admin\controllers;

use app\modules\admin\AdminModule;
use yii\db\ActiveRecordInterface;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

abstract class AdminBaseController extends Controller
{
    /** @var AdminModule */
    public $module;

    /** @var string  */
    public $layout = 'main';

    public $searchModel;
    public $formModel;

    /** @var ActiveRecordInterface */
    public $objectClass;

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
                'user' => $this->module->admin
            ],
        ];
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new $this->searchModel([
            'modelClass' => $this->objectClass
        ]);
        $dataProvider = $searchModel->search(\Yii::$app->request->get());
        return $this->render('index', [
            'dataProvider' => $dataProvider,
            //'searchModel' => isset($searchModel) ? $searchModel : null,
        ]);
    }


    /**
     * @param $id
     * @return mixed|ActiveRecordInterface
     * @throws NotFoundHttpException
     */
    protected function getRequestedObject($id)
    {
        if (!$id || !$item = $this->objectClass::findOne(['id' => $id])) {
            throw new NotFoundHttpException();
        }

        return $item;
    }

}