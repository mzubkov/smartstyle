<?php

namespace app\modules\admin\controllers;

use app\models\db\News;
use app\models\db\NewsImage;
use app\modules\admin\controllers\traits\CreateTrait;
use app\modules\admin\controllers\traits\DeleteTrait;
use app\modules\admin\controllers\traits\UpdateTrait;
use app\modules\admin\models\forms\BaseForm;
use app\modules\admin\models\forms\NewsImageForm;
use yii\base\Model;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;

class NewsImageController extends AdminBaseController
{
    public $formModel = NewsImageForm::class;
    public $objectClass = NewsImage::class;

    use CreateTrait, DeleteTrait, UpdateTrait;

    /**
     * @return string
     */
    public function actionIndex()
    {
        $this->redirect(Url::to(['news/index']));
    }

    /**
     * @param $id
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionCreate($id)
    {
        if (!$news = News::findOne(['id' => $id])) {
            throw new NotFoundHttpException('Не найдена новость');
        }

        /** @var BaseForm $model */
        $model = new NewsImageForm([
            'objectClass' => $this->objectClass,
            'id_news' => $news->id
        ]);
        $request = \Yii::$app->request;

        if ($request->isPost && $model->load($request->post()) && $model->create()) {
            \Yii::$app->session->setFlash('success', 'Запись успешно создана');
            return $this->redirect(Url::to(['news/gallery', 'id' => $news->id]));
        } else {
            if ($request->isPost) {
                \Yii::$app->session->setFlash('error', 'Ошибка при создани записи');
            }

            return $this->render('create', [
                'model' => $model,
                'news' => $news
            ]);
        }
    }

    /**
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionEdit()
    {
        /** @var NewsImage $item */
        $item = $this->getRequestedObject(\Yii::$app->request->get('id'));

        $model = new NewsImageForm(['id_news' => $item->id_news]);
        $model->import($item);
        $request = \Yii::$app->request;

        if ($request->isPost && $model->load($request->post()) && $model->edit($item)) {
            \Yii::$app->session->setFlash('success', 'Изменения сохранены');
            return $this->redirect(Url::to(['news/staff', 'id' => $item->id_news]));
        } else {
            if ($request->isPost) {
                \Yii::$app->session->setFlash('error', 'Ошибка при редактировании записи');
            }

            return $this->render('edit', [
                'model' => $model,
                'news' => $item->news
            ]);
        }
    }

    /**
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     * @throws \Throwable
     */
    public function actionDelete()
    {
        /** @var NewsImage $item */
        $item = $this->getRequestedObject(\Yii::$app->request->get('id'));

        try {
            $item->delete();
            \Yii::$app->session->setFlash('success', 'Запись удалена');
            return $this->redirect(Url::to(['news/staff', 'id' => $item->id_news]));
        } catch (\Exception $e) {
            \Yii::$app->session->setFlash('error', 'Ошибка при удалении записи');
            return $this->redirect(Url::to(['news/staff', 'id' => $item->id_news]));
        }
    }
}