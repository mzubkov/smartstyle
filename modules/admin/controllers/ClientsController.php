<?php

namespace app\modules\admin\controllers;

use app\models\ClientUser;
use app\modules\admin\controllers\traits\CreateTrait;
use app\modules\admin\controllers\traits\DeleteTrait;
use app\modules\admin\controllers\traits\UpdateTrait;
use app\modules\admin\models\forms\ClientForm;
use app\modules\admin\models\search\BaseSearch;
use Codeception\Module\Cli;

class ClientsController extends AdminBaseController
{
    public $searchModel = BaseSearch::class;
    public $formModel = ClientForm::class;
    public $objectClass = ClientUser::class;

    use CreateTrait, DeleteTrait, UpdateTrait;
}