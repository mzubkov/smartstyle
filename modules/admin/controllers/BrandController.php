<?php

namespace app\modules\admin\controllers;

use app\models\db\Brand;
use app\models\db\Catalog;
use app\modules\admin\controllers\traits\CreateTrait;
use app\modules\admin\controllers\traits\DeleteTrait;
use app\modules\admin\controllers\traits\UpdateTrait;
use app\modules\admin\models\forms\BrandForm;
use app\modules\admin\models\search\BaseSearch;
use app\modules\admin\models\search\BrandAdvantageSearch;
use app\modules\admin\models\search\BrandItemSearch;
use app\modules\admin\models\search\BrandQuoteSearch;
use app\modules\admin\models\search\BrandVideoSearch;
use app\modules\admin\models\search\CatalogProductSearch;
use app\modules\admin\models\search\CatalogSearch;
use yii\web\NotFoundHttpException;

class BrandController extends AdminBaseController
{
    public $searchModel = BaseSearch::class;
    public $formModel = BrandForm::class;
    public $objectClass = Brand::class;

    use CreateTrait, UpdateTrait, DeleteTrait;

    /**
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionAdvantages($id)
    {
        if (!$brand = Brand::findOne(['id' => $id])) {
            throw new NotFoundHttpException('Не найден бренд');
        }

        $searchModel = new BrandAdvantageSearch([
            'id_brand' => $brand->id
        ]);
        $dataProvider = $searchModel->search(\Yii::$app->request->get());

        return $this->render('advantages', [
            'brand' => $brand,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionVideo($id)
    {
        if (!$brand = Brand::findOne(['id' => $id])) {
            throw new NotFoundHttpException('Не найден бренд');
        }

        $searchModel = new BrandVideoSearch([
            'id_brand' => $brand->id
        ]);
        $dataProvider = $searchModel->search(\Yii::$app->request->get());

        return $this->render('video', [
            'brand' => $brand,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionQuote($id)
    {
        if (!$brand = Brand::findOne(['id' => $id])) {
            throw new NotFoundHttpException('Не найден бренд');
        }

        $searchModel = new BrandQuoteSearch([
            'id_brand' => $brand->id
        ]);
        $dataProvider = $searchModel->search(\Yii::$app->request->get());

        return $this->render('quote', [
            'brand' => $brand,
            'dataProvider' => $dataProvider,
        ]);
    }


    /**
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionItems($id)
    {
        if (!$brand = Brand::findOne(['id' => $id])) {
            throw new NotFoundHttpException('Не найден бренд');
        }

        $searchModel = new BrandItemSearch([
            'id_brand' => $brand->id
        ]);
        $dataProvider = $searchModel->search(\Yii::$app->request->get());

        return $this->render('items', [
            'brand' => $brand,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionCatalog($id)
    {
        if (!$brand = Brand::findOne(['id' => $id])) {
            throw new NotFoundHttpException('Не найден бренд');
        }

        $searchModel = new CatalogSearch([
            'id_brand' => $brand->id
        ]);
        $dataProvider = $searchModel->search(\Yii::$app->request->get());

        return $this->render('catalog', [
            'brand' => $brand,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionProducts($id)
    {
        if (!$catalog = Catalog::findOne(['id' => $id])) {
            throw new NotFoundHttpException('Не найден каталог');
        }

        $searchModel = new CatalogProductSearch([
            'id_catalog' => $catalog->id
        ]);
        $dataProvider = $searchModel->search(\Yii::$app->request->get());

        return $this->render('products', [
            'catalog' => $catalog,
            'brand' => $catalog->brand,
            'dataProvider' => $dataProvider,
        ]);
    }
}