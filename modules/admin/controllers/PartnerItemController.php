<?php

namespace app\modules\admin\controllers;

use app\models\db\PartnerItem;
use app\modules\admin\controllers\traits\CreateTrait;
use app\modules\admin\controllers\traits\DeleteTrait;
use app\modules\admin\controllers\traits\UpdateTrait;
use app\modules\admin\models\forms\PartnerItemForm;
use app\modules\admin\models\search\BaseSearch;

class PartnerItemController extends AdminBaseController
{
    public $searchModel = BaseSearch::class;
    public $formModel = PartnerItemForm::class;
    public $objectClass = PartnerItem::class;

    use CreateTrait, DeleteTrait, UpdateTrait;

}