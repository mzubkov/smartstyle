<?php

namespace app\modules\admin\controllers;

use app\models\db\Brand;
use app\models\db\Catalog;
use app\models\db\CatalogProduct;
use app\modules\admin\controllers\traits\CreateTrait;
use app\modules\admin\controllers\traits\DeleteTrait;
use app\modules\admin\controllers\traits\UpdateTrait;
use app\modules\admin\models\forms\BaseForm;
use app\modules\admin\models\forms\CatalogProductForm;
use yii\base\Model;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;

class CatalogProductController extends AdminBaseController
{
    public $formModel = CatalogProductForm::class;
    public $objectClass = CatalogProduct::class;

    use CreateTrait, DeleteTrait, UpdateTrait;

    /**
     * @return string
     */
    public function actionIndex()
    {
        $this->redirect(Url::to(['brand/index']));
    }

    /**
     * @param $id
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionCreate($id)
    {
        if (!$catalog = Catalog::findOne(['id' => $id])) {
            throw new NotFoundHttpException('Не найден каталог');
        }

        /** @var BaseForm $model */
        $model = new CatalogProductForm([
            'objectClass' => $this->objectClass,
            'id_catalog' => $catalog->id
        ]);
        $request = \Yii::$app->request;

        if ($request->isPost && $model->load($request->post()) && $model->create()) {
            \Yii::$app->session->setFlash('success', 'Запись успешно создана');
            return $this->redirect(Url::to(['brand/products', 'id' => $catalog->id]));
        } else {
            if ($request->isPost) {
                \Yii::$app->session->setFlash('error', 'Ошибка при создани записи');
            }

            return $this->render('create', [
                'model' => $model,
                'brand' => $catalog->brand,
                'catalog' => $catalog
            ]);
        }
    }

    /**
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionEdit()
    {
        /** @var CatalogProduct $item */
        $item = $this->getRequestedObject(\Yii::$app->request->get('id'));

        /** @var Model $model */
        $model = new CatalogProductForm(['id_catalog' => $item->id_catalog]);
        $model->import($item);
        $request = \Yii::$app->request;

        if ($request->isPost && $model->load($request->post()) && $model->edit($item)) {
            \Yii::$app->session->setFlash('success', 'Изменения сохранены');
            return $this->redirect(Url::to(['brand/products', 'id' => $item->id_catalog]));
        } else {
            if ($request->isPost) {
                \Yii::$app->session->setFlash('error', 'Ошибка при редактировании записи');
            }

            return $this->render('edit', [
                'model' => $model,
                'brand' => $item->catalog->brand,
                'catalog' => $item->catalog
            ]);
        }
    }

    /**
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     * @throws \Throwable
     */
    public function actionDelete()
    {
        /** @var CatalogProduct $item */
        $item = $this->getRequestedObject(\Yii::$app->request->get('id'));

        try {
            $item->delete();
            \Yii::$app->session->setFlash('success', 'Запись удалена');
            return $this->redirect(Url::to(['brand/products', 'id' => $item->id_catalog]));
        } catch (\Exception $e) {
            \Yii::$app->session->setFlash('error', 'Ошибка при удалении записи');
            return $this->redirect(Url::to(['brand/products', 'id' => $item->id_catalog]));
        }
    }
}