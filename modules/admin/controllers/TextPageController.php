<?php

namespace app\modules\admin\controllers;

use app\models\db\TextPage;
use app\modules\admin\controllers\traits\CreateTrait;
use app\modules\admin\controllers\traits\DeleteTrait;
use app\modules\admin\controllers\traits\UpdateTrait;
use app\modules\admin\models\forms\TextPageForm;
use app\modules\admin\models\search\BaseSearch;

class TextPageController extends AdminBaseController
{
    public $searchModel = BaseSearch::class;
    public $formModel = TextPageForm::class;
    public $objectClass = TextPage::class;

    use CreateTrait, DeleteTrait, UpdateTrait;

}