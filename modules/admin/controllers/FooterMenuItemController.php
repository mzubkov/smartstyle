<?php

namespace app\modules\admin\controllers;

use app\models\db\FooterMenuFolder;
use app\models\db\FooterMenuItem;
use app\modules\admin\controllers\traits\CreateTrait;
use app\modules\admin\controllers\traits\DeleteTrait;
use app\modules\admin\controllers\traits\UpdateTrait;
use app\modules\admin\models\forms\FooterMenuItemForm;
use yii\base\Model;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;

class FooterMenuItemController extends AdminBaseController
{
    public $formModel = FooterMenuItemForm::class;
    public $objectClass = FooterMenuItem::class;

    use CreateTrait, DeleteTrait, UpdateTrait;

    /**
     * @return string
     */
    public function actionIndex()
    {
        $this->redirect(Url::to(['footer-menu/index']));
    }

    /**
     * @param $id
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionCreate($id)
    {
        if (!$folder = FooterMenuFolder::findOne(['id' => $id])) {
            throw new NotFoundHttpException('Не найден раздел');
        }

        $model = new FooterMenuItemForm([
            'objectClass' => $this->objectClass,
            'id_folder' => $folder->id
        ]);
        $request = \Yii::$app->request;

        if ($request->isPost && $model->load($request->post()) && $model->create()) {
            \Yii::$app->session->setFlash('success', 'Запись успешно создана');
            return $this->redirect(Url::to(['footer-menu/items', 'id' => $folder->id]));
        } else {
            if ($request->isPost) {
                \Yii::$app->session->setFlash('error', 'Ошибка при создани записи');
            }

            return $this->render('create', [
                'model' => $model,
                'folder' => $folder
            ]);
        }
    }

    /**
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionEdit()
    {
        /** @var FooterMenuItem $item */
        $item = $this->getRequestedObject(\Yii::$app->request->get('id'));

        /** @var Model $model */
        $model = new FooterMenuItemForm(['id_folder' => $item->id_folder]);
        $model->import($item);
        $request = \Yii::$app->request;

        if ($request->isPost && $model->load($request->post()) && $model->edit($item)) {
            \Yii::$app->session->setFlash('success', 'Изменения сохранены');
            return $this->redirect(Url::to(['footer-menu/items', 'id' => $item->id_folder]));
        } else {
            if ($request->isPost) {
                \Yii::$app->session->setFlash('error', 'Ошибка при редактировании записи');
            }

            return $this->render('edit', [
                'model' => $model,
                'folder' => $item->folder
            ]);
        }
    }

    /**
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     * @throws \Throwable
     */
    public function actionDelete()
    {
        /** @var FooterMenuItem $item */
        $item = $this->getRequestedObject(\Yii::$app->request->get('id'));

        try {
            $item->delete();
            \Yii::$app->session->setFlash('success', 'Запись удалена');
        } catch (\Exception $e) {
            \Yii::$app->session->setFlash('error', 'Ошибка при удалении записи');
        }

        return $this->redirect(Url::to(['footer-menu/items', 'id' => $item->id_folder]));
    }
}