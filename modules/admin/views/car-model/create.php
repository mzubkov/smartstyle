<?php

use app\models\db\CarBrand;
use app\modules\admin\models\forms\CarModelForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model CarModelForm */
/* @var $brand CarBrand */

$this->title = 'Добавление марки';
$this->params['breadcrumbs'][] = ['label' => 'Тех. карты', 'url' => Url::to(['maps/index'])];
$this->params['breadcrumbs'][] = ['label' => $brand->name, 'url' => Url::to(['maps/models', 'id' => $brand->id])];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box">
    <div class="box-body">
        <?= $this->render('_form', [
            'model' => $model,
            'brand' => $brand
        ]) ?>
    </div>
</div>
