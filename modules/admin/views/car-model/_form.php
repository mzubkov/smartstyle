<?php

use app\models\db\CarBrand;
use app\modules\admin\models\forms\CarModelForm;
use yii\helpers\Html;
use yii\helpers\Url;
use app\modules\admin\widgets\Form;

/* @var $this yii\web\View */
/* @var $model CarModelForm */
/* @var $brand CarBrand */

?>

<div>

    <?php $form = Form::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'name')->textInput() ?>
    <?= $form->imageField($model, 'image')->fileInput() ?>
    <?= $form->field($model, 'is_published')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Отмена',  Url::to(['maps/models', 'id' => $brand->id]), ['class' => 'btn btn-default']) ?>
    </div>

    <?php Form::end(); ?>

</div>
