<?php

use app\modules\admin\models\forms\MapItemForm;
use yii\helpers\Html;
use app\modules\admin\widgets\Form;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model MapItemForm */
?>

<div>

    <?php $form = Form::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'number')->textInput() ?>
    <?= $form->field($model, 'text')->textarea() ?>
    <?= $form->field($model, 'is_published')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Отмена',  Url::to(['map-item/index']), ['class' => 'btn btn-default']) ?>
    </div>

    <?php Form::end(); ?>

</div>
