<?php

use app\modules\admin\models\forms\MapItemForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model MapItemForm */

$this->title = 'Добавление элемента';
$this->params['breadcrumbs'][] = ['label' => 'Цифры', 'url' => Url::to(['map-item/index'])];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box">
    <div class="box-body">
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
    </div>
</div>
