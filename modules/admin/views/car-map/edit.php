<?php

use app\models\db\CarBrand;
use app\models\db\CarModel;
use app\modules\admin\models\forms\CarModelForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model CarModelForm */
/* @var $brand CarBrand */
/* @var $carModel CarModel */

$this->title = 'Редактирование файла ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Тех. карты', 'url' => Url::to(['maps/index'])];
$this->params['breadcrumbs'][] = ['label' => $brand->name, 'url' => Url::to(['maps/models', 'id' => $brand->id])];
$this->params['breadcrumbs'][] = ['label' => $carModel->name, 'url' => Url::to(['maps/files', 'id' => $carModel->id])];
$this->params['breadcrumbs'][] = $model->name;
?>
<div class="box">
    <div class="box-body">
        <?= $this->render('_form', [
            'model' => $model,
            'carModel' => $carModel
        ]) ?>
    </div>
</div>
