<?php

use app\models\db\Brand;
use app\modules\admin\models\forms\NewsForm;
use app\modules\admin\widgets\WisywygWidget;
use yii\helpers\Html;
use app\modules\admin\widgets\Form;
use yii\jui\DatePicker;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model NewsForm */

$brands = Brand::find()
    ->select('name')
    ->orderBy(['name' => SORT_ASC])
    ->indexBy('id')
    ->asArray()
    ->column();
?>
<div>

    <?php $form = Form::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'id_brand')->dropDownList($brands, ['prompt' => '------------'])?>
    <?= $form->field($model, 'name')->textInput() ?>
    <?= $form->field($model, 'annotation')->textarea() ?>
    <?= $form->field($model, 'content')->widget(WisywygWidget::class, ['options' => ['rows' => 20]]); ?>
    <?= $form->field($model, 'is_published')->checkbox() ?>
    <?= $form->field($model, 'news_date')->widget(DatePicker::class, [
        'language' => 'ru',
        'inline'   => true,
        //'dateFormat' => 'dd.MM.yyyy',
        'dateFormat' => 'yyyy-MM-dd',
        'value'    => null,
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Отмена',  Url::to(['news/index']), ['class' => 'btn btn-default']) ?>
    </div>

    <?php Form::end(); ?>

</div>
