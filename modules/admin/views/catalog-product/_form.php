<?php

use app\models\db\Catalog;
use app\modules\admin\models\forms\CatalogProductForm;
use yii\helpers\Html;
use app\modules\admin\widgets\Form;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model CatalogProductForm */
/* @var $catalog Catalog  */
?>

<div>

    <?php $form = Form::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'name')->textInput() ?>
    <?= $form->imageField($model, 'image')->fileInput() ?>
    <?= $form->field($model, 'is_published')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Отмена',  Url::to(['brand/products', 'id' => $catalog->id]), ['class' => 'btn btn-default']) ?>
    </div>

    <?php Form::end(); ?>

</div>
