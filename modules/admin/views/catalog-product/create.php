<?php

use app\models\db\Brand;
use app\models\db\Catalog;
use app\modules\admin\models\forms\CatalogProductForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model CatalogProductForm */
/** @var $brand Brand */
/** @var $catalog Catalog */

$this->title = 'Добавление продукта';
$this->params['breadcrumbs'][] = ['label' => 'Бренды', 'url' => Url::to(['brand/index'])];
$this->params['breadcrumbs'][] = ['label' => $brand->name, 'url' => Url::to(['brand/catalog', 'id' => $brand->id])];
$this->params['breadcrumbs'][] = $catalog->name;
?>
<div class="box">
    <div class="box-body">
        <?= $this->render('_form', [
            'model' => $model,
            'catalog' => $catalog
        ]) ?>
    </div>
</div>
