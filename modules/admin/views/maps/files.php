<?php

use app\models\db\CarBrand;
use app\models\db\CarMap;
use app\models\db\CarModel;
use app\modules\admin\grid\AdminGridView;
use app\modules\admin\grid\StatusColumn;
use app\modules\admin\widgets\Actions;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/** @var $brand CarBrand */
/** @var $model CarModel */

$this->title = 'Карты установок';
$this->params['breadcrumbs'][] = ['label' => 'Тех. карты', 'url' => Url::to(['maps/index'])];
$this->params['breadcrumbs'][] = ['label' => $brand->name, 'url' => Url::to(['maps/models', 'id' => $brand->id])];
$this->params['breadcrumbs'][] = $model->name;
?>
<div class="box">
    <div class="box-body">
        <?= Actions::widget(['controller' => 'car-map', 'item' => $model, 'buttons' => ['create']]);?>
        <table class="table table-bordered table-hover">
            <?=AdminGridView::widget([
                'dataProvider' => $dataProvider,
                'columns' => [
                    [
                        'attribute' => 'name',
                        'options' => ['width' => '80%'],
                        'format' => 'raw',
                        'value' => function(CarMap $model) {
                            return "<b>{$model->name}</b>";
                        }
                    ],
                    [
                        'class' => StatusColumn::class
                    ],
                    [
                        'label' => 'Действия',
                        'format' => 'raw',
                        'contentOptions' => [
                            'nowrap' => 'nowrap'
                        ],
                        'value' => function($item) {
                            return Actions::widget([
                                'controller' => 'car-map',
                                'item' => $item,
                                'buttons' => ['edit', 'delete']
                            ]);
                        }
                    ],
                ]
            ]);
            ?>
        </table>
    </div>
</div>
