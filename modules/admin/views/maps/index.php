<?php

use app\models\db\CarBrand;
use app\modules\admin\grid\AdminGridView;
use app\modules\admin\grid\StatusColumn;
use app\modules\admin\widgets\Actions;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Марки автомобилей';
$this->params['breadcrumbs'][] = 'Тех. карты';
?>
<div class="box">
    <div class="box-body">
        <?= Actions::widget(['controller' => 'car-brand', 'buttons' => ['create']]);?>
        <table class="table table-bordered table-hover">
            <?=AdminGridView::widget([
                'dataProvider' => $dataProvider,
                'columns' => [
                    [
                        'attribute' => 'img_ext',
                        'format' => 'raw',
                        'options' => ['width' => '10%'],
                        'value' => function(CarBrand $model) {
                            $img = $model->getImage('290x70');
                            if (!empty($img)) {
                                return Html::img($img);
                            } else {
                                return '';
                            }
                        }
                    ],
                    [
                        'attribute' => 'name',
                        'options' => ['width' => '80%'],
                        'format' => 'raw',
                        'value' => function(CarBrand $model) {
                            $link = Html::a('Модели', Url::to(['maps/models', 'id' => $model->id]));
                            return "<b>{$model->name}</b><br>[{$link}]";
                        }
                    ],
                    [
                        'class' => StatusColumn::class
                    ],
                    [
                        'label' => 'Действия',
                        'format' => 'raw',
                        'contentOptions' => [
                            'nowrap' => 'nowrap'
                        ],
                        'value' => function($item) {
                            return Actions::widget([
                                'controller' => 'car-brand',
                                'item' => $item,
                                'buttons' => ['edit', 'delete']
                            ]);
                        }
                    ],
                ]
            ]);
            ?>
        </table>
    </div>
</div>
