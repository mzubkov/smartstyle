<?php

use app\models\db\CarBrand;
use app\models\db\CarModel;
use app\modules\admin\grid\AdminGridView;
use app\modules\admin\grid\StatusColumn;
use app\modules\admin\widgets\Actions;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/** @var $brand CarBrand */

$this->title = $brand->name . '. Модели автомобилей';
$this->params['breadcrumbs'][] = ['label' => 'Тех. карты', 'url' => Url::to(['maps/index'])];
$this->params['breadcrumbs'][] = $brand->name;
?>
<div class="box">
    <div class="box-body">
        <?= Actions::widget(['controller' => 'car-model', 'item' => $brand, 'buttons' => ['create']]);?>
        <table class="table table-bordered table-hover">
            <?=AdminGridView::widget([
                'dataProvider' => $dataProvider,
                'columns' => [
                    [
                        'attribute' => 'img_ext',
                        'format' => 'raw',
                        'options' => ['width' => '10%'],
                        'value' => function(CarModel $model) {
                            $img = $model->getImage('290x165');
                            if (!empty($img)) {
                                return Html::img($img);
                            } else {
                                return '';
                            }
                        }
                    ],
                    [
                        'attribute' => 'name',
                        'options' => ['width' => '80%'],
                        'format' => 'raw',
                        'value' => function(CarModel $model) {
                            $link = Html::a('Карты установок', Url::to(['maps/files', 'id' => $model->id]));
                            return "<b>{$model->name}</b><br>[{$link}]";
                        }
                    ],
                    [
                        'class' => StatusColumn::class
                    ],
                    [
                        'label' => 'Действия',
                        'format' => 'raw',
                        'contentOptions' => [
                            'nowrap' => 'nowrap'
                        ],
                        'value' => function($item) {
                            return Actions::widget([
                                'controller' => 'car-model',
                                'item' => $item,
                                'buttons' => ['edit', 'delete']
                            ]);
                        }
                    ],
                ]
            ]);
            ?>
        </table>
    </div>
</div>
