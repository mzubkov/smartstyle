<?php

use app\models\db\InstructionCategory;
use app\modules\admin\models\forms\InstructionCategoryForm;
use yii\helpers\Html;
use app\modules\admin\widgets\Form;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model InstructionCategoryForm */
/* @var $category InstructionCategory  */
?>

<div>

    <?php $form = Form::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'name')->textInput() ?>
    <?= $form->field($model, 'is_published')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
        <?php if(!$category){ ?>
            <?= Html::a('Отмена',  Url::to(['instructions/index']), ['class' => 'btn btn-default']) ?>
        <?php } else {?>
            <?= Html::a('Отмена',  Url::to(['instructions/category', 'id' => $category->id]), ['class' => 'btn btn-default']) ?>
        <?php }?>
    </div>

    <?php Form::end(); ?>

</div>
