<?php

use app\models\db\InstructionCategory;
use app\modules\admin\models\forms\InstructionCategoryForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model InstructionCategoryForm */
/** @var $category InstructionCategory */

$this->title = 'Редактирование раздела: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Инструкции и документация', 'url' => Url::to(['instructions/index'])];
if ($category) {
    $this->params['breadcrumbs'][] = ['label' => $category->name, 'url' => Url::to(['instructions/category', 'id' => $category->id])];
}
$this->params['breadcrumbs'][] = $model->name;
?>
<div class="box">
    <div class="box-body">
        <?= $this->render('_form', [
            'model' => $model,
            'category' => $category
        ]) ?>
    </div>
</div>
