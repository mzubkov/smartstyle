<?php

use app\models\db\InstructionCategory;
use app\modules\admin\models\forms\InstructionCategoryForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model InstructionCategoryForm */
/** @var $category InstructionCategory */

$this->title = 'Добавление раздела';
$this->params['breadcrumbs'][] = ['label' => 'Инструкции и документация', 'url' => Url::to(['instructions/index'])];
if ($category && $category->parent) {
    $this->params['breadcrumbs'][] = ['label' => $category->parent->name, 'url' => Url::to(['instructions/category', 'id' => $category->id_parent])];
}
$this->params['breadcrumbs'][] = (!is_null($category)) ? $category->name : 'Добавление раздела';
?>
<div class="box">
    <div class="box-body">
        <?= $this->render('_form', [
            'model' => $model,
            'category' => $category
        ]) ?>
    </div>
</div>
