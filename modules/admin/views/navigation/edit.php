<?php

use app\modules\admin\models\forms\NavigationForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model NavigationForm */

$this->title = 'Редактирование элемента: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Навигация', 'url' => Url::to(['navigation/index'])];
$this->params['breadcrumbs'][] = 'Редактировать';
?>
<div class="box">
    <div class="box-body">
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
    </div>
</div>
