<?php

use app\modules\admin\models\forms\PartnerItemForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model PartnerItemForm */

$this->title = 'Добавление элемента';
$this->params['breadcrumbs'][] = ['label' => 'Быть партнером', 'url' => Url::to(['partner-item/index'])];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box">
    <div class="box-body">
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
    </div>
</div>
