<?php

use app\models\db\Department;
use app\models\db\Staff;
use app\modules\admin\grid\AdminGridView;
use app\modules\admin\grid\StatusColumn;
use app\modules\admin\widgets\Actions;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $department Department */

$this->title = 'Команда. ' . $department->name;
$this->params['breadcrumbs'][] = ['label' => 'Команда', 'url' => Url::to(['department/index'])];
$this->params['breadcrumbs'][] = $department->name;
?>
<div class="box">
    <div class="box-body">
        <?= Actions::widget(['controller' => 'staff', 'item' => $department, 'buttons' => ['create']]);?>
        <table class="table table-bordered table-hover">
            <?=AdminGridView::widget([
                'dataProvider' => $dataProvider,
                'columns' => [
                    [
                        'attribute' => 'img_ext',
                        'format' => 'raw',
                        'options' => ['width' => '10%'],
                        'value' => function(Staff $model) {
                            $img = $model->getImage('290x300');
                            if (!empty($img)) {
                                return Html::img($img, ['width' => 100]);
                            } else {
                                return '';
                            }
                        }
                    ],
                    [
                        'attribute' => 'name',
                        'format' => 'raw',
                        'options' => ['width' => '80%'],
                        'value' => function(Staff $model) {
                            return "<b>{$model->name}</b>({$model->post})<br>Тел.:{$model->phone}";
                        }
                    ],
                    [
                        'class' => StatusColumn::class
                    ],
                    [
                        'label' => 'Действия',
                        'format' => 'raw',
                        'contentOptions' => [
                            'nowrap' => 'nowrap'
                        ],
                        'value' => function($item) {
                            return Actions::widget([
                                'controller' => 'staff',
                                'item' => $item,
                                'buttons' => ['edit', 'delete']
                            ]);
                        }
                    ],
                ]
            ]);
            ?>
        </table>
    </div>
</div>
