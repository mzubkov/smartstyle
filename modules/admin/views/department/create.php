<?php

use app\modules\admin\models\forms\DepartmentForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model DepartmentForm */

$this->title = 'Добавление отдела';
$this->params['breadcrumbs'][] = ['label' => 'Команда', 'url' => Url::to(['department/index'])];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box">
    <div class="box-body">
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
    </div>
</div>
