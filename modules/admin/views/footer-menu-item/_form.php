<?php

use app\models\db\FooterMenuFolder;
use app\modules\admin\models\forms\FooterMenuItemForm;
use yii\helpers\Html;
use app\modules\admin\widgets\Form;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model FooterMenuItemForm */
/* @var $folder FooterMenuFolder  */
?>

<div>

    <?php $form = Form::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'name')->textInput() ?>
    <?= $form->field($model, 'url')->textInput() ?>
    <?= $form->field($model, 'is_published')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Отмена',  Url::to(['footer-menu/items', 'id' => $folder->id]), ['class' => 'btn btn-default']) ?>
    </div>

    <?php Form::end(); ?>

</div>
