<?php

use app\models\db\FooterMenuFolder;
use app\modules\admin\models\forms\FooterMenuItemForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model FooterMenuItemForm */
/** @var $folder FooterMenuFolder */

$this->title = 'Добавление ссылки';
$this->params['breadcrumbs'][] = ['label' => 'Навигация в футере', 'url' => Url::to(['footer-menu/index'])];
$this->params['breadcrumbs'][] = $folder->name;
?>
<div class="box">
    <div class="box-body">
        <?= $this->render('_form', [
            'model' => $model,
            'folder' => $folder
        ]) ?>
    </div>
</div>
