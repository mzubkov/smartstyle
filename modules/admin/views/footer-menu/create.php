<?php

use app\modules\admin\models\forms\FooterMenuFolderForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model FooterMenuFolderForm */

$this->title = 'Добавление раздела';
$this->params['breadcrumbs'][] = ['label' => 'Навигация в футере', 'url' => Url::to(['footer-menu/index'])];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box">
    <div class="box-body">
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
    </div>
</div>
