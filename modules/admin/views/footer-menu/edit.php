<?php

use app\modules\admin\models\forms\FooterMenuFolderForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model FooterMenuFolderForm */

$this->title = 'Редактирование раздела: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Навигация в футере', 'url' => Url::to(['footer-menu/index'])];
$this->params['breadcrumbs'][] = $model->name;
?>
<div class="box">
    <div class="box-body">
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
    </div>
</div>
