<?php

use app\modules\admin\models\forms\FooterMenuFolderForm;
use yii\helpers\Html;
use yii\helpers\Url;
use app\modules\admin\widgets\Form;

/* @var $this yii\web\View */
/* @var $model FooterMenuFolderForm */

?>

<div>

    <?php $form = Form::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'name')->textInput() ?>
    <?= $form->field($model, 'is_published')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Отмена',  Url::to(['footer-menu/index']), ['class' => 'btn btn-default']) ?>
    </div>

    <?php Form::end(); ?>

</div>
