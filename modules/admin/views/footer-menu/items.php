<?php

use app\models\db\FooterMenuFolder;
use app\models\db\FooterMenuItem;
use app\modules\admin\grid\AdminGridView;
use app\modules\admin\grid\StatusColumn;
use app\modules\admin\widgets\Actions;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $folder FooterMenuFolder */

$this->title = 'Навигация в футере. ' . $folder->name;
$this->params['breadcrumbs'][] = ['label' => 'Навигация в футере', 'url' => Url::to(['footer-menu/index'])];
$this->params['breadcrumbs'][] = $folder->name;
?>
<div class="box">
    <div class="box-body">
        <?= Actions::widget(['controller' => 'footer-menu-item', 'item' => $folder, 'buttons' => ['create']]);?>
        <table class="table table-bordered table-hover">
            <?=AdminGridView::widget([
                'dataProvider' => $dataProvider,
                'columns' => [
                    [
                        'attribute' => 'name',
                        'format' => 'raw',
                        'options' => ['width' => '80%'],
                        'value' => function(FooterMenuItem $model) {
                            $url = Html::a($model->url, $model->url, ['target' => '_blank']);
                            return "<b>{$model->name}</b><br>[{$url}]";
                        }
                    ],
                    [
                        'class' => StatusColumn::class
                    ],
                    [
                        'label' => 'Действия',
                        'format' => 'raw',
                        'contentOptions' => [
                            'nowrap' => 'nowrap'
                        ],
                        'value' => function($item) {
                            return Actions::widget([
                                'controller' => 'footer-menu-item',
                                'item' => $item,
                                'buttons' => ['edit', 'delete']
                            ]);
                        }
                    ],
                ]
            ]);
            ?>
        </table>
    </div>
</div>
