<?php

use app\modules\admin\models\forms\WorkItemForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model WorkItemForm */

$this->title = 'Редактирование элемента: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'С кем работаем', 'url' => Url::to(['work-item/index'])];
$this->params['breadcrumbs'][] = 'Редактировать';
?>
<div class="box">
    <div class="box-body">
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
    </div>
</div>
