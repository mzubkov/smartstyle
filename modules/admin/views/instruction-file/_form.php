<?php

use app\models\db\InstructionItem;
use app\modules\admin\models\forms\InstructionFileForm;
use yii\helpers\Html;
use yii\helpers\Url;
use app\modules\admin\widgets\Form;

/* @var $this yii\web\View */
/* @var $model InstructionFileForm */
/* @var $item InstructionItem */

?>

<div>

    <?php $form = Form::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'name')->textInput() ?>
    <?= $form->fileField($model, 'file')->fileInput() ?>
    <?= $form->field($model, 'is_published')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Отмена',  Url::to(['instructions/files', 'id' => $item->id]), ['class' => 'btn btn-default']) ?>
    </div>

    <?php Form::end(); ?>

</div>
