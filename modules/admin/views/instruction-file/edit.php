<?php

use app\models\db\InstructionCategory;
use app\models\db\InstructionItem;
use app\modules\admin\models\forms\InstructionFileForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model InstructionFileForm */
/* @var $category InstructionCategory */
/* @var $item InstructionItem */

$this->title = 'Редактирование файла ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Инструкции и документация', 'url' => Url::to(['instructions/index'])];
$this->params['breadcrumbs'][] = ['label' => $category->parent->name, 'url' => Url::to(['instructions/category', 'id' => $category->parent->id])];
$this->params['breadcrumbs'][] = ['label' => $category->name, 'url' => Url::to(['instructions/items', 'id' => $category->id])];
$this->params['breadcrumbs'][] = $model->name;
?>
<div class="box">
    <div class="box-body">
        <?= $this->render('_form', [
            'model' => $model,
            'item' => $item
        ]) ?>
    </div>
</div>
