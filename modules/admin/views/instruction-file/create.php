<?php

use app\models\db\InstructionCategory;
use app\models\db\InstructionItem;
use app\modules\admin\models\forms\InstructionFileForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model InstructionFileForm */
/* @var $category InstructionCategory */
/* @var $item InstructionItem */

$this->title = 'Добавление файла';
$this->params['breadcrumbs'][] = ['label' => 'Инструкции и документация', 'url' => Url::to(['instructions/index'])];
$this->params['breadcrumbs'][] = ['label' => $category->parent->name, 'url' => Url::to(['instructions/category', 'id' => $category->parent->id])];
$this->params['breadcrumbs'][] = ['label' => $category->name, 'url' => Url::to(['instructions/category', 'id' => $category->id])];
$this->params['breadcrumbs'][] = ['label' => $item->name, 'url' => Url::to(['instructions/files', 'id' => $item->id])];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box">
    <div class="box-body">
        <?= $this->render('_form', [
            'model' => $model,
            'item' => $item
        ]) ?>
    </div>
</div>
