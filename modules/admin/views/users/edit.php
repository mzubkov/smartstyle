<?php

use app\modules\admin\models\forms\UsersForm;
use yii\helpers\Url;

/** @var \yii\web\View $this */
/** @var $model UsersForm */

$this->title = 'Редактирование пользователя ' . $model->login;
$this->params['breadcrumbs'][] = ['label' => 'Пользователи', 'url' => Url::to(['users/index'])];
$this->params['breadcrumbs'][] = $model->login;

?>

<div class="box">
    <div class="box-body">
        <?=$this->render('form', ['model' => $model]) ?>
    </div>
</div>
