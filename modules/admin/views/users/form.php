<?php

use yii\helpers\Html;
use app\modules\admin\widgets\Form;
use yii\helpers\Url;

?>

<div class="user-form">
    <?php $form = Form::begin(); ?>
    <?= $form->field($model, 'login')->textInput() ?>
    <?= $form->field($model, 'password')->passwordInput() ?>
    <?= $form->field($model, 'passwordRepeat')->passwordInput() ?>

    <div class="form-group">
        <?= Html::submitButton( 'Сохранить',  ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Отмена',  Url::to(['users/index']), ['class' => 'btn btn-default']) ?>
    </div>
    <?php Form::end(); ?>
</div>
