<?php

use yii\helpers\Url;

/** @var \yii\web\View $this */

$this->title = 'Добавление пользователя';
$this->params['breadcrumbs'][] = ['label' => 'Пользователи', 'url' => Url::to(['users/index'])];
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="box">
    <div class="box-body">
        <?=$this->render('form', ['model' => $model]) ?>
    </div>
</div>
