<?php

use app\modules\admin\grid\AdminGridView;
use app\modules\admin\widgets\Actions;

/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Админ. пользователи';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="box">
    <div class="box-body">
        <?= Actions::widget(['controller' => 'users', 'buttons' => ['create']]);?>
        <table class="table table-bordered table-hover">
            <?= AdminGridView::widget([
                'dataProvider' => $dataProvider,
                'columns' => [
                    'id',
                    [
                        'label' => 'Логин',
                        'attribute' => 'login',
                        'options' => ['width' => '70%']

                    ],
                    [
                        'label' => 'Время последнего входа',
                        'attribute' => 'last_login',
                        'value' => 'last_login',
                    ],
                    [
                        'label' => 'Действия',
                        'format' => 'raw',
                        'value' => function($item) {
                            return Actions::widget([
                                'controller' => 'users',
                                'item' => $item,
                                'buttons' => ['edit', 'delete']
                            ]);
                        }
                    ],
                ]
            ]);
            ?>
        </table>
    </div>
</div>


