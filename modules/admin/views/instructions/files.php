<?php

use app\models\db\InstructionCategory;
use app\models\db\InstructionFile;
use app\models\db\InstructionItem;
use app\modules\admin\grid\AdminGridView;
use app\modules\admin\grid\StatusColumn;
use app\modules\admin\widgets\Actions;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/** @var $category InstructionCategory */
/** @var $item InstructionItem */

$this->title = 'Файлы';
$this->params['breadcrumbs'][] = ['label' => 'Инструкции и документация', 'url' => Url::to(['instructions/index'])];
$this->params['breadcrumbs'][] = ['label' => $category->parent->name, 'url' => Url::to(['instructions/category', 'id' => $category->parent->id])];
$this->params['breadcrumbs'][] = ['label' => $category->name, 'url' => Url::to(['instructions/items', 'id' => $category->id])];
$this->params['breadcrumbs'][] = $item->name;
?>
<div class="box">
    <div class="box-body">
        <?= Actions::widget(['controller' => 'instruction-file', 'item' => $item, 'buttons' => ['create']]);?>
        <table class="table table-bordered table-hover">
            <?=AdminGridView::widget([
                'dataProvider' => $dataProvider,
                'columns' => [
                    [
                        'attribute' => 'name',
                        'options' => ['width' => '80%'],
                        'format' => 'raw',
                        'value' => function(InstructionFile $model) {
                            $link = Html::a('Скачать', $model->getFile(), ['target' => '_blank']);
                            return "<b>{$model->name}</b><br>[{$link}]";
                        }
                    ],
                    [
                        'class' => StatusColumn::class
                    ],
                    [
                        'label' => 'Действия',
                        'format' => 'raw',
                        'contentOptions' => [
                            'nowrap' => 'nowrap'
                        ],
                        'value' => function($item) {
                            return Actions::widget([
                                'controller' => 'instruction-file',
                                'item' => $item,
                                'buttons' => ['edit', 'delete']
                            ]);
                        }
                    ],
                ]
            ]);
            ?>
        </table>
    </div>
</div>
