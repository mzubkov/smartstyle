<?php

use app\models\db\InstructionCategory;
use app\modules\admin\grid\AdminGridView;
use app\modules\admin\grid\StatusColumn;
use app\modules\admin\widgets\Actions;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/** @var $category InstructionCategory */

$this->title = 'Список разделов';
if (isset($category)) {
    $this->params['breadcrumbs'][] = ['label' => 'Инструкции и документация', 'url' => Url::to(['instructions/index'])];
    $this->params['breadcrumbs'][] = $category->name;
} else {
    $this->params['breadcrumbs'][] = 'Инструкции и документация';
}
?>
<div class="box">
    <div class="box-body">
        <?= Actions::widget(['controller' => 'instruction-category', 'item' => $category, 'buttons' => ['create']]);?>
        <table class="table table-bordered table-hover">
            <?=AdminGridView::widget([
                'dataProvider' => $dataProvider,
                'columns' => [
                    [
                        'attribute' => 'name',
                        'options' => ['width' => '80%'],
                        'format' => 'raw',
                        'value' => function(InstructionCategory $model) {
                            if (!$model->id_parent) {
                                $link = Html::a('Категории', Url::to(['instructions/category', 'id' => $model->id]));
                                return "<b>{$model->name}</b><br>[{$link}]";
                            } else {
                                $link = Html::a('Модели', Url::to(['instructions/items', 'id' => $model->id]));
                                return "<b>{$model->name}</b><br>[{$link}]";
                            }
                        }
                    ],
                    [
                        'class' => StatusColumn::class
                    ],
                    [
                        'label' => 'Действия',
                        'format' => 'raw',
                        'contentOptions' => [
                            'nowrap' => 'nowrap'
                        ],
                        'value' => function($item) {
                            return Actions::widget([
                                'controller' => 'instruction-category',
                                'item' => $item,
                                'buttons' => ['edit', 'delete']
                            ]);
                        }
                    ],
                ]
            ]);
            ?>
        </table>
    </div>
</div>
