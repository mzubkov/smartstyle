<?php

use app\models\db\Brand;
use app\models\db\BrandVideo;
use app\modules\admin\grid\AdminGridView;
use app\modules\admin\grid\StatusColumn;
use app\modules\admin\widgets\Actions;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $brand Brand */

$this->title = $brand->name . '. Видео';
$this->params['breadcrumbs'][] = ['label' => 'Бренды', 'url' => Url::to(['brand/index'])];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box">
    <div class="box-body">
        <?= Actions::widget(['controller' => 'brand-video', 'item' => $brand, 'buttons' => ['create']]);?>
        <table class="table table-bordered table-hover">
            <?=AdminGridView::widget([
                'dataProvider' => $dataProvider,
                'columns' => [
                    [
                        'attribute' => 'name',
                        'format' => 'raw',
                        'options' => ['width' => '80%'],
                        'value' => function(BrandVideo $model){
                            $videoLink = Html::a('Посмотреть видео', $model->getUrl(), ['target' => '_blank']);
                            return "<b>{$model->name}</b><br>{$videoLink}";
                        }
                    ],
                    [
                        'class' => StatusColumn::class
                    ],
                    [
                        'label' => 'Действия',
                        'format' => 'raw',
                        'contentOptions' => [
                            'nowrap' => 'nowrap'
                        ],
                        'value' => function($item) {
                            return Actions::widget([
                                'controller' => 'brand-video',
                                'item' => $item,
                                'buttons' => ['edit', 'delete']
                            ]);
                        }
                    ],
                ]
            ]);
            ?>
        </table>
    </div>
</div>
