<?php

use app\modules\admin\models\forms\BrandForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model BrandForm */

$this->title = 'Редактирование бренда ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Бренды', 'url' => Url::to(['brand/index'])];
$this->params['breadcrumbs'][] = 'Редактировать';
?>
<div class="box">
    <div class="box-body">
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
    </div>
</div>
