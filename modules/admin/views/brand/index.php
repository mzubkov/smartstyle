<?php

use app\models\db\Brand;
use app\modules\admin\grid\AdminGridView;
use app\modules\admin\grid\StatusColumn;
use app\modules\admin\widgets\Actions;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Бренды';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box">
    <div class="box-body">
        <?= Actions::widget(['controller' => 'brand', 'buttons' => ['create']]);?>
        <table class="table table-bordered table-hover">
            <?=AdminGridView::widget([
                'dataProvider' => $dataProvider,
                'columns' => [
                    [
                        'attribute' => 'img_ext',
                        'format' => 'raw',
                        'options' => ['width' => '10%'],
                        'value' => function(Brand $model) {
                            $img = $model->getImage('610x400');
                            if (!empty($img)) {
                                return Html::img($img, ['width' => 100]);
                            } else {
                                return '';
                            }
                        }
                    ],
                    [
                        'attribute' => 'name',
                        'options' => ['width' => '80%'],
                        'format' => 'raw',
                        'value' => function(Brand $model) {
                            $catalogLink = Html::a('Каталог', Url::to(['brand/catalog', 'id' => $model->id]));
                            $advantagesLink = Html::a('Преимущества', Url::to(['brand/advantages', 'id' => $model->id]));
                            $itemsLink = Html::a('Особенности', Url::to(['brand/items', 'id' => $model->id]));
                            $videoLink = Html::a('Видео', Url::to(['brand/video', 'id' => $model->id]));
                            $qouteLink = Html::a('Цитата', Url::to(['brand/quote', 'id' => $model->id]));
                            return "<b>{$model->name}</b><br>
                                [{$catalogLink}]<br>
                                [{$advantagesLink}]<br>
                                [{$itemsLink}]<br>
                                [{$videoLink}]<br>
                                [{$qouteLink}]
                            ";
                        }
                    ],
                    [
                        'class' => StatusColumn::class
                    ],
                    [
                        'label' => 'Действия',
                        'format' => 'raw',
                        'contentOptions' => [
                            'nowrap' => 'nowrap'
                        ],
                        'value' => function($item) {
                            return Actions::widget([
                                'controller' => 'brand',
                                'item' => $item,
                                'buttons' => ['edit', 'delete']
                            ]);
                        }
                    ],
                ]
            ]);
            ?>
        </table>
    </div>
</div>
