<?php

use app\modules\admin\models\forms\BrandForm;
use app\modules\admin\widgets\WisywygWidget;
use yii\helpers\Html;
use app\modules\admin\widgets\Form;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model BrandForm */
?>

<div>

    <?php $form = Form::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'name')->textInput() ?>
    <?= $form->field($model, 'description')->textarea() ?>
    <?= $form->imageField($model, 'image')->fileInput() ?>
    <?= $form->imageField($model, 'logo')->fileInput() ?>
    <?= $form->imageField($model, 'page_image')->fileInput() ?>
    <?= $form->field($model, 'content')->widget(WisywygWidget::class, ['options' => ['rows' => 20]])?>
    <?= $form->field($model, 'is_published')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Отмена',  Url::to(['brand/index']), ['class' => 'btn btn-default']) ?>
    </div>

    <?php Form::end(); ?>

</div>
