<?php

use app\models\db\Brand;
use app\models\db\Catalog;
use app\models\db\CatalogProduct;
use app\modules\admin\grid\AdminGridView;
use app\modules\admin\widgets\Actions;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $brand Brand */
/* @var $catalog Catalog */

$this->title = $brand->name . '. ' . $catalog->name;
$this->params['breadcrumbs'][] = ['label' => 'Бренды', 'url' => Url::to(['brand/index'])];
$this->params['breadcrumbs'][] = ['label' => $brand->name, 'url' => Url::to(['brand/catalog', 'id' => $brand->id])];
$this->params['breadcrumbs'][] = $catalog->name;
?>
<div class="box">
    <div class="box-body">
        <?= Actions::widget(['controller' => 'catalog-product', 'item' => $catalog, 'buttons' => ['create']]);?>
        <table class="table table-bordered table-hover">
            <?=AdminGridView::widget([
                'dataProvider' => $dataProvider,
                'columns' => [
                    [
                        'attribute' => 'img_ext',
                        'format' => 'raw',
                        'options' => ['width' => '10%'],
                        'value' => function(CatalogProduct $model) {
                            $img = $model->getImage('290x290');
                            if (!empty($img)) {
                                return Html::img($img, ['width' => 100]);
                            } else {
                                return '';
                            }
                        }
                    ],
                    [
                        'attribute' => 'name',
                        'format' => 'raw',
                        'options' => ['width' => '80%'],
                    ],
                    [
                        'attribute' => 'is_published',
                        'format' => 'raw',
                        'options' => ['width' => '5%'],
                        'label' => 'Статус',
                        'value' => function(CatalogProduct $model){
                            return Html::tag('i', '&nbsp', [
                                'class' => 'fa fa-fw fa-lightbulb-o',
                                'style' => 'font-size: 2rem;color: ' . ($model->is_published ? 'green' : 'red')
                            ]);
                        }
                    ],
                    [
                        'label' => 'Действия',
                        'format' => 'raw',
                        'contentOptions' => [
                            'nowrap' => 'nowrap'
                        ],
                        'value' => function($item) {
                            return Actions::widget([
                                'controller' => 'catalog-product',
                                'item' => $item,
                                'buttons' => ['edit', 'delete']
                            ]);
                        }
                    ],
                ]
            ]);
            ?>
        </table>
    </div>
</div>
