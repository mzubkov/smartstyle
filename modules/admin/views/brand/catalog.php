<?php

use app\models\db\Brand;
use app\models\db\Catalog;
use app\modules\admin\grid\AdminGridView;
use app\modules\admin\grid\StatusColumn;
use app\modules\admin\widgets\Actions;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $brand Brand */

$this->title = 'Каталог ' . $brand->name;
$this->params['breadcrumbs'][] = ['label' => 'Бренды', 'url' => Url::to(['brand/index'])];
$this->params['breadcrumbs'][] = $brand->name;
?>
<div class="box">
    <div class="box-body">
        <?= Actions::widget(['controller' => 'catalog', 'item' => $brand, 'buttons' => ['create']]);?>
        <table class="table table-bordered table-hover">
            <?=AdminGridView::widget([
                'dataProvider' => $dataProvider,
                'columns' => [
                    [
                        'attribute' => 'img_ext',
                        'format' => 'raw',
                        'options' => ['width' => '10%'],
                        'value' => function(Catalog $model) {
                            $img = $model->getImage('610x400');
                            if (!empty($img)) {
                                return Html::img($img, ['width' => 100]);
                            } else {
                                return '';
                            }
                        }
                    ],
                    [
                        'attribute' => 'name',
                        'format' => 'raw',
                        'options' => ['width' => '80%'],
                        'value' => function(Catalog $model) {
                            $link = Html::a('Продукты', Url::to(['brand/products', 'id' => $model->id]));
                            return "<b>{$model->name}</b><br>[{$link}]";
                        }
                    ],
                    [
                        'class' => StatusColumn::class
                    ],
                    [
                        'label' => 'Действия',
                        'format' => 'raw',
                        'contentOptions' => [
                            'nowrap' => 'nowrap'
                        ],
                        'value' => function($item) {
                            return Actions::widget([
                                'controller' => 'catalog',
                                'item' => $item,
                                'buttons' => ['edit', 'delete']
                            ]);
                        }
                    ],
                ]
            ]);
            ?>
        </table>
    </div>
</div>
