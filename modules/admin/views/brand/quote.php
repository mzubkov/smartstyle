<?php

use app\models\db\Brand;
use app\models\db\BrandQuote;
use app\modules\admin\grid\AdminGridView;
use app\modules\admin\grid\StatusColumn;
use app\modules\admin\widgets\Actions;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $brand Brand */

$this->title = $brand->name . '. Цитата';
$this->params['breadcrumbs'][] = ['label' => 'Бренды', 'url' => Url::to(['brand/index'])];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box">
    <div class="box-body">
        <?php if(!BrandQuote::find()->count()) { ?>
            <?= Actions::widget(['controller' => 'brand-quote', 'item' => $brand, 'buttons' => ['create']]);?>
        <?php }?>
        <table class="table table-bordered table-hover">
            <?=AdminGridView::widget([
                'dataProvider' => $dataProvider,
                'columns' => [
                    [
                        'attribute' => 'img_ext',
                        'format' => 'raw',
                        'options' => ['width' => '10%'],
                        'value' => function(BrandQuote $model) {
                            $img = $model->getImage('1872x450');
                            if (!empty($img)) {
                                return Html::img($img, ['width' => 100]);
                            } else {
                                return '';
                            }
                        }
                    ],
                    [
                        'attribute' => 'text',
                        'format' => 'raw',
                    ],
                    [
                        'class' => StatusColumn::class
                    ],
                    [
                        'label' => 'Действия',
                        'format' => 'raw',
                        'contentOptions' => [
                            'nowrap' => 'nowrap'
                        ],
                        'value' => function($item) {
                            return Actions::widget([
                                'controller' => 'brand-quote',
                                'item' => $item,
                                'buttons' => ['edit', 'delete']
                            ]);
                        }
                    ],
                ]
            ]);
            ?>
        </table>
    </div>
</div>
