<?php

use app\models\db\Department;
use app\modules\admin\models\forms\StaffForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model StaffForm */
/** @var $department Department */

$this->title = 'Добавление сотрудника';
$this->params['breadcrumbs'][] = ['label' => 'Команда', 'url' => Url::to(['department/index'])];
$this->params['breadcrumbs'][] = $department->name;
?>
<div class="box">
    <div class="box-body">
        <?= $this->render('_form', [
            'model' => $model,
            'department' => $department
        ]) ?>
    </div>
</div>
