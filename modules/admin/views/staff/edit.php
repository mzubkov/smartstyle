<?php

use app\models\db\Department;
use app\modules\admin\models\forms\StaffForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model StaffForm */
/** @var $department Department */

$this->title = 'Редактирование сотрудника: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Команда', 'url' => Url::to(['department/index'])];
$this->params['breadcrumbs'][] = ['label' => $department->name, 'url' => Url::to(['department/staff', 'id' => $department->id])];
$this->params['breadcrumbs'][] = $model->name;
?>
<div class="box">
    <div class="box-body">
        <?= $this->render('_form', [
            'model' => $model,
            'department' => $department
        ]) ?>
    </div>
</div>
