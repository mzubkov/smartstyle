<?php

use app\models\db\Department;
use app\modules\admin\models\forms\StaffForm;
use yii\helpers\Html;
use app\modules\admin\widgets\Form;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model StaffForm */
/* @var $department Department  */
?>

<div>

    <?php $form = Form::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'name')->textInput() ?>
    <?= $form->field($model, 'post')->textInput() ?>
    <?= $form->field($model, 'phone')->textInput() ?>
    <?= $form->imageField($model, 'image')->fileInput() ?>
    <?= $form->field($model, 'is_published')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Отмена',  Url::to(['department/staff', 'id' => $department->id]), ['class' => 'btn btn-default']) ?>
    </div>

    <?php Form::end(); ?>

</div>
