<?php

use app\modules\admin\models\forms\PartnerForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model PartnerForm */

$this->title = 'Добавление партнера';
$this->params['breadcrumbs'][] = ['label' => 'Партнеры', 'url' => Url::to(['partner/index'])];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box">
    <div class="box-body">
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
    </div>
</div>
