<?php

use app\modules\admin\models\forms\PartnerForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model PartnerForm */

$this->title = 'Редактирование партнера: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Партнеры', 'url' => Url::to(['partner/index'])];
$this->params['breadcrumbs'][] = 'Редактировать';
?>
<div class="box">
    <div class="box-body">
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
    </div>
</div>
