<?php

use app\models\db\Brand;
use app\modules\admin\models\forms\BrandVideoForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model BrandVideoForm */
/** @var $brand Brand */

$this->title = 'Добавление элемента';
$this->params['breadcrumbs'][] = ['label' => 'Бренды', 'url' => Url::to(['brand/index'])];
$this->params['breadcrumbs'][] = $brand->name;
?>
<div class="box">
    <div class="box-body">
        <?= $this->render('_form', [
            'model' => $model,
            'brand' => $brand
        ]) ?>
    </div>
</div>
