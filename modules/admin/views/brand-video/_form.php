<?php

use app\models\db\Brand;
use app\modules\admin\models\forms\BrandVideoForm;
use yii\helpers\Html;
use app\modules\admin\widgets\Form;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model BrandVideoForm */
/* @var $brand Brand  */
?>

<div>

    <?php $form = Form::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'name')->textInput()?>
    <?= $form->field($model, 'video_code')->textInput()->hint(
        'Пример ссылки <a href="https://www.youtube.com/watch?v=6SCJGwmrRfo" target="_blank"><b>https://www.youtube.com/watch?v=6SCJGwmrRfo</b></a>'
    )?>
    <?= $form->field($model, 'is_published')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Отмена',  Url::to(['brand/video', 'id' => $brand->id]), ['class' => 'btn btn-default']) ?>
    </div>

    <?php Form::end(); ?>

</div>
