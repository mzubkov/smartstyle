<?php

use app\models\db\News;
use app\modules\admin\models\forms\NewsImageForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model NewsImageForm */
/** @var $news News */

$this->title = 'Редактирование изображения';
$this->params['breadcrumbs'][] = ['label' => 'Новости', 'url' => Url::to(['department/index'])];
$this->params['breadcrumbs'][] = ['label' => $news->name, 'url' => Url::to(['department/staff', 'id' => $news->id])];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box">
    <div class="box-body">
        <?= $this->render('_form', [
            'model' => $model,
            'news' => $news
        ]) ?>
    </div>
</div>
