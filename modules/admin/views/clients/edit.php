<?php

use app\modules\admin\models\forms\ClientForm;
use yii\helpers\Url;

/** @var \yii\web\View $this */
/** @var $model ClientForm */

$this->title = 'Редактирование клиента ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Пользователи', 'url' => Url::to(['clients/index'])];
$this->params['breadcrumbs'][] = $model->login;

?>

<div class="box">
    <div class="box-body">
        <?=$this->render('form', ['model' => $model]) ?>
    </div>
</div>
