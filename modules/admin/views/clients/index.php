<?php

use app\modules\admin\grid\AdminGridView;
use app\modules\admin\widgets\Actions;

/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Клиенты';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="box">
    <div class="box-body">
        <?= Actions::widget(['controller' => 'clients', 'buttons' => ['create']]);?>
        <table class="table table-bordered table-hover">
            <?= AdminGridView::widget([
                'dataProvider' => $dataProvider,
                'columns' => [
                    'id',
                    [
                        'attribute' => 'name',
                        'options' => ['width' => '20%']

                    ],
                    [
                        'attribute' => 'login',
                        'options' => ['width' => '50%']

                    ],
                    [
                        'label' => 'Время последнего входа',
                        'attribute' => 'last_login',
                        'value' => 'last_login',
                    ],
                    [
                        'label' => 'Действия',
                        'format' => 'raw',
                        'value' => function($item) {
                            return Actions::widget([
                                'controller' => 'clients',
                                'item' => $item,
                                'buttons' => ['edit', 'delete']
                            ]);
                        }
                    ],
                ]
            ]);
            ?>
        </table>
    </div>
</div>


