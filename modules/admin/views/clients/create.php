<?php

use yii\helpers\Url;

/** @var \yii\web\View $this */

$this->title = 'Добавление клиента';
$this->params['breadcrumbs'][] = ['label' => 'Клиенты', 'url' => Url::to(['clients/index'])];
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="box">
    <div class="box-body">
        <?=$this->render('form', ['model' => $model]) ?>
    </div>
</div>
