<?php

use app\models\db\SellItem;
use app\modules\admin\grid\AdminGridView;
use app\modules\admin\grid\StatusColumn;
use app\modules\admin\widgets\Actions;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Как продавать';
$this->blocks['content-header'] = $this->title;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box">
    <div class="box-body">
        <?= Actions::widget(['controller' => 'sell-item', 'buttons' => ['create']]);?>
        <table class="table table-bordered table-hover">
            <?=AdminGridView::widget([
                'dataProvider' => $dataProvider,
                'columns' => [
                    [
                        'attribute' => 'img_ext',
                        'format' => 'raw',
                        'options' => ['width' => '10%'],
                        'value' => function(SellItem $model) {
                            $img = $model->getImage('100x100');
                            if (!empty($img)) {
                                return Html::img($img, ['width' => 100]);
                            } else {
                                return '';
                            }
                        }
                    ],
                    [
                        'attribute' => 'text',
                        'format' => 'raw',
                        'options' => ['width' => '80%'],
                    ],
                    [
                        'class' => StatusColumn::class
                    ],
                    [
                        'label' => 'Действия',
                        'format' => 'raw',
                        'contentOptions' => [
                            'nowrap' => 'nowrap'
                        ],
                        'value' => function($item) {
                            return Actions::widget([
                                'controller' => 'sell-item',
                                'item' => $item,
                                'buttons' => ['edit', 'delete']
                            ]);
                        }
                    ],
                ]
            ]);
            ?>
        </table>
    </div>
</div>
