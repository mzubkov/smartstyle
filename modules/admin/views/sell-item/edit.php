<?php

use app\modules\admin\models\forms\SellItemForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model SellItemForm */

$this->title = 'Редактирование элемента';
$this->params['breadcrumbs'][] = ['label' => 'Как продавать', 'url' => Url::to(['sell-item/index'])];
$this->params['breadcrumbs'][] = 'Редактировать';
?>
<div class="box">
    <div class="box-body">
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
    </div>
</div>
