<?php

use app\modules\admin\models\forms\TextBlockForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model TextBlockForm */

$this->title = 'Редактирование элемента: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Текстовые блоки', 'url' => Url::to(['text-block/index'])];
$this->params['breadcrumbs'][] = 'Редактировать';
?>
<div class="box">
    <div class="box-body">
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
    </div>
</div>
