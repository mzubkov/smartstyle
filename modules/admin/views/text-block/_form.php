<?php

use app\models\db\TextBlock;
use app\modules\admin\models\forms\TextBlockForm;
use app\modules\admin\widgets\WisywygWidget;
use yii\helpers\Html;
use app\modules\admin\widgets\Form;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model TextBlockForm */
?>

<div>

    <?php $form = Form::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'name')->textInput(['disabled' => true]) ?>
    <?php
        var_dump($model->column_type);
        switch ($model->column_type) {
            case TextBlock::TYPE_WYSIWYG:
                echo $form->field($model, 'content')->widget(WisywygWidget::class, ['options' => ['rows' => 20]]);
                break;
            case TextBlock::TYPE_INPUT:
                echo $form->field($model, 'content')->textInput();
                break;

            case TextBlock::TYPE_TEXTAREA:
                echo $form->field($model, 'content')->textarea();
                break;

            default:
                throw new \Exception('Неизвестный тип');
        }
    ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Отмена',  Url::to(['text-block/index']), ['class' => 'btn btn-default']) ?>
    </div>

    <?php Form::end(); ?>

</div>
