<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\db\News */

$this->title = 'Редактирование профиля:';
$this->params['breadcrumbs'][] = ['label' => 'Профиль', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Редактировать';
?>
<div>

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
