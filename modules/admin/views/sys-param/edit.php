<?php

use app\modules\admin\models\forms\SysParamForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model SysParamForm */

$this->title = 'Редактирование элемента: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Настройки', 'url' => Url::to(['sys-param/index'])];
$this->params['breadcrumbs'][] = 'Редактировать';
?>
<div class="box">
    <div class="box-body">
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
    </div>
</div>
