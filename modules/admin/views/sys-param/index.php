<?php

use app\modules\admin\grid\AdminGridView;
use app\modules\admin\widgets\Actions;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Текстовые блоки';
$this->blocks['content-header'] = $this->title;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box">
    <div class="box-body">
        <table class="table table-bordered table-hover">
            <?=AdminGridView::widget([
                'dataProvider' => $dataProvider,
                'columns' => [
                    [
                        'attribute' => 'identifier',
                        'options' => ['width' => '10%']
                    ],
                    [
                        'attribute' => 'name',
                        'options' => ['width' => '10%']
                    ],
                    [
                        'attribute' => 'value',
                        'options' => ['width' => '80%']
                    ],
                    [
                        'label' => 'Действия',
                        'format' => 'raw',
                        'contentOptions' => [
                            'nowrap' => 'nowrap'
                        ],
                        'value' => function($item) {
                            return Actions::widget([
                                'controller' => 'sys-param',
                                'item' => $item,
                                'itemKey' => 'identifier',
                                'buttons' => ['edit']
                            ]);
                        }
                    ],
                ]
            ]);
            ?>
        </table>
    </div>
</div>
