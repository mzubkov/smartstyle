<?php

use app\modules\admin\models\forms\TextPageForm;
use yii\helpers\Url;


/* @var $this yii\web\View */
/* @var $model TextPageForm */

$this->title = 'Создание элемента';
$this->params['breadcrumbs'][] = ['label' => 'Текстовые страницы', 'url' => Url::to(['text-page/index'])];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box">
    <div class="box-body">
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
    </div>
</div>
