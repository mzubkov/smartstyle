<?php

use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\db\Navigation */

$this->title = 'Редактирование страницы: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Текстовые страницы', 'url' => Url::to(['text-page/index'])];
$this->params['breadcrumbs'][] = $model->name;
?>
<div class="box">
    <div class="box-body">
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
    </div>
</div>
