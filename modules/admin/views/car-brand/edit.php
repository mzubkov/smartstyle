<?php

use app\modules\admin\models\forms\CarBrandForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model CarBrandForm */

$this->title = 'Редактирование марки ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Тех. карты', 'url' => Url::to(['maps/index'])];
$this->params['breadcrumbs'][] = 'Редактировать';
?>
<div class="box">
    <div class="box-body">
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
    </div>
</div>
