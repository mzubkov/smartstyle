<?php

use app\modules\admin\models\forms\CarBrandForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model CarBrandForm */

$this->title = 'Добавление марки';
$this->params['breadcrumbs'][] = ['label' => 'Тех. карты', 'url' => Url::to(['maps/index'])];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box">
    <div class="box-body">
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
    </div>
</div>
