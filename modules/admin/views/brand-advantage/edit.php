<?php

use app\models\db\Brand;
use app\modules\admin\models\forms\BrandAdvantageForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model BrandAdvantageForm */
/** @var $brand Brand */

$this->title = 'Редактирование элемента';
$this->params['breadcrumbs'][] = ['label' => 'Бренды', 'url' => Url::to(['brand/index'])];
$this->params['breadcrumbs'][] = ['label' => $brand->name, 'url' => Url::to(['brand/catalog', 'id' => $brand->id])];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="box">
    <div class="box-body">
        <?= $this->render('_form', [
            'model' => $model,
            'brand' => $brand
        ]) ?>
    </div>
</div>
