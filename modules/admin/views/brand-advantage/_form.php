<?php

use app\models\db\Brand;
use app\modules\admin\models\forms\BrandAdvantageForm;
use app\modules\admin\widgets\WisywygWidget;
use yii\helpers\Html;
use app\modules\admin\widgets\Form;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model BrandAdvantageForm */
/* @var $brand Brand  */
?>

<div>

    <?php $form = Form::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'text')->widget(WisywygWidget::class, ['options' => ['rows' => 10]])?>
    <?= $form->field($model, 'is_published')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Отмена',  Url::to(['brand/advantages', 'id' => $brand->id]), ['class' => 'btn btn-default']) ?>
    </div>

    <?php Form::end(); ?>

</div>
