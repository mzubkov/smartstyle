<?php
use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $content string */

$user = Yii::$app->getModule('admin')->admin;
?>

<header class="main-header">

    <?= Html::a('<span class="logo-mini"></span><span class="logo-lg">' . Yii::$app->name . '</span>', Yii::$app->homeUrl, ['class' => 'logo']) ?>

    <nav class="navbar navbar-static-top" role="navigation">

        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Свернуть меню</span>
        </a>

        <div class="navbar-custom-menu">

            <ul class="nav navbar-nav">

                <li class="dropdown user user-menu">
                    <a href="<?=\yii\helpers\Url::toRoute(['profile/edit'])?>">
                        <span class="hidden-xs"><?=$user->identity->login?></span>
                    </a>
                </li>

                <li>
                    <?= Html::a(
                        'Выйти ' . Html::tag('i', '', ['class' => 'fa fa-sign-out']),
                        ['/admin/logout'],
                        ['data-method' => 'post', 'class' => '']
                    ) ?>
                </li>
            </ul>
        </div>
    </nav>
</header>