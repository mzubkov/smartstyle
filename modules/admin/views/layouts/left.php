<?php
use yii\helpers\Url;
?>
<aside class="main-sidebar">
    <section class="sidebar">
        <?= \app\modules\admin\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                'items' => [
                    //['label' => 'Текст. страницы', 'icon' => 'file-word-o', 'url' => [Url::toRoute(['text-page/index'])]],
                    ['label' => 'Админ. пользователи', 'icon' => 'user-secret', 'url' => [Url::toRoute(['users/index'])]],
                    ['label' => 'Бренды', 'icon' => 'database', 'url' => [Url::toRoute(['brand/index'])]],
                    ['label' => 'Инструкции и документация', 'icon' => 'file-archive-o', 'url' => [Url::toRoute(['instructions/index'])]],
                    ['label' => 'Новости', 'icon' => 'newspaper-o', 'url' => [Url::toRoute(['news/index'])]],
                    ['label' => 'Клиенты', 'icon' => 'users', 'icon_color' => 'yellow', 'url' => [Url::toRoute(['clients/index'])]],
                    ['label' => 'С кем работаем', 'icon' => 'star', 'url' => [Url::toRoute(['work-item/index'])]],
                    ['label' => 'Быть партнером', 'icon' => 'briefcase', 'url' => [Url::toRoute(['partner-item/index'])]],
                    ['label' => 'Партнеры', 'icon' => 'users', 'icon_color' => 'green', 'url' => [Url::toRoute(['partner/index'])]],
                    ['label' => 'Как продавать', 'icon' => 'money', 'url' => [Url::toRoute(['sell-item/index'])]],
                    ['label' => 'Команда', 'icon' => 'users', 'icon_color' => 'purple', 'url' => [Url::toRoute(['department/index'])]],
                    ['label' => 'Тех. карты', 'icon' => 'map-o', 'url' => [Url::toRoute(['maps/index'])]],
                    ['label' => 'Статистика', 'icon' => 'sort-numeric-asc', 'url' => [Url::toRoute(['map-item/index'])]],
                    ['label' => 'Текстовые блоки', 'icon' => 'cube', 'url' => [Url::toRoute(['text-block/index'])]],
                    ['label' => 'Слайдшоу', 'icon' => 'picture-o', 'url' => [Url::toRoute(['slideshow/index'])]],
                    ['label' => 'Настройки', 'icon' => 'cog', 'url' => [Url::toRoute(['sys-param/index'])]],
                    ['label' => 'Навигация', 'icon' => 'mouse-pointer', 'url' => [Url::toRoute(['navigation/index'])]],
                    ['label' => 'Навигация футер', 'icon' => 'mouse-pointer', 'url' => [Url::toRoute(['footer-menu/index'])]],
                    /*
                    [
                        'label' => 'Some tools',
                        'icon' => 'share',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Gii', 'icon' => 'file-code-o', 'url' => ['/gii'],],
                            ['label' => 'Debug', 'icon' => 'dashboard', 'url' => ['/debug'],],
                            [
                                'label' => 'Level One',
                                'icon' => 'circle-o',
                                'url' => '#',
                                'items' => [
                                    ['label' => 'Level Two', 'icon' => 'circle-o', 'url' => '#',],
                                    [
                                        'label' => 'Level Two',
                                        'icon' => 'circle-o',
                                        'url' => '#',
                                        'items' => [
                                            ['label' => 'Level Three', 'icon' => 'circle-o', 'url' => '#',],
                                            ['label' => 'Level Three', 'icon' => 'circle-o', 'url' => '#',],
                                        ],
                                    ],
                                ],
                            ],
                        ],
                    ],
                    */
                ],
            ]
        ) ?>

    </section>
</aside>