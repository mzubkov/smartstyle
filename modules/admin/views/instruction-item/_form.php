<?php

use app\models\db\InstructionCategory;
use app\modules\admin\models\forms\InstructionItemForm;
use yii\helpers\Html;
use yii\helpers\Url;
use app\modules\admin\widgets\Form;

/* @var $this yii\web\View */
/* @var $model InstructionItemForm */
/* @var $category InstructionCategory */

?>

<div>

    <?php $form = Form::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'name')->textInput() ?>
    <?= $form->field($model, 'is_published')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Отмена',  Url::to(['instructions/items', 'id' => $category->id]), ['class' => 'btn btn-default']) ?>
    </div>

    <?php Form::end(); ?>

</div>
