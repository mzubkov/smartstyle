<?php

use app\modules\admin\models\forms\SlideshowForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model SlideshowForm */

$this->title = 'Добавление изображения';
$this->params['breadcrumbs'][] = ['label' => 'Слайдшоу', 'url' => Url::to(['slideshow/index'])];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box">
    <div class="box-body">
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
    </div>
</div>
