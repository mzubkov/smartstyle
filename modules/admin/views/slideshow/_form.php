<?php

use app\models\db\Slideshow;
use app\modules\admin\models\forms\SlideshowForm;
use yii\helpers\Html;
use yii\helpers\Url;
use app\modules\admin\widgets\Form;

/* @var $this yii\web\View */
/* @var $model SlideshowForm */

$positions = [
    Slideshow::POSITION_HEADER => 'Верх',
    Slideshow::POSITION_CENTER => 'Центр',
    Slideshow::POSITION_BOTTOM => 'Низ'
];
?>
<div>

    <?php $form = Form::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'position')->dropDownList($positions) ?>
    <?= $form->field($model, 'name')->textInput() ?>
    <?= $form->imageField($model, 'image')->fileInput() ?>
    <?= $form->field($model, 'is_published')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Отмена',  Url::to(['slideshow/index']), ['class' => 'btn btn-default']) ?>
    </div>

    <?php Form::end(); ?>

</div>
