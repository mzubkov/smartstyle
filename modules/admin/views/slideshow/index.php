<?php

use app\models\db\Slideshow;
use app\modules\admin\grid\AdminGridView;
use app\modules\admin\grid\StatusColumn;
use app\modules\admin\widgets\Actions;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Слайдшоу';
$this->blocks['content-header'] = $this->title;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box">
    <div class="box-body">
        <?= Actions::widget(['controller' => 'slideshow', 'buttons' => ['create']]);?>
        <table class="table table-bordered table-hover">
            <?=AdminGridView::widget([
                'dataProvider' => $dataProvider,
                'columns' => [
                    [
                        'attribute' => 'img_ext',
                        'format' => 'raw',
                        'options' => ['width' => '30%'],
                        'value' => function(Slideshow $model) {
                            $img = $model->getImage('300x75');
                            if (!empty($img)) {
                                return Html::img($img, ['width' => 300]);
                            } else {
                                return '';
                            }
                        }
                    ],
                    [
                        'attribute' => 'name',
                        'options' => ['width' => '50%'],
                    ],
                    [
                        'attribute' => 'position',
                        'options' => ['width' => '10%'],
                        'value' => function(Slideshow $model){
                            return "{$model->getPositionName()}";
                        }
                    ],
                    [
                        'class' => StatusColumn::class
                    ],
                    [
                        'label' => 'Действия',
                        'format' => 'raw',
                        'contentOptions' => [
                            'nowrap' => 'nowrap'
                        ],
                        'value' => function($item) {
                            return Actions::widget([
                                'controller' => 'slideshow',
                                'item' => $item,
                                'buttons' => ['edit', 'delete']
                            ]);
                        }
                    ],
                ]
            ]);
            ?>
        </table>
    </div>
</div>
