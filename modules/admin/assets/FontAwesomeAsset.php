<?php

namespace app\modules\admin\assets;

use yii\web\AssetBundle;

/**
 * FontAwesome AssetBundle
 */
class FontAwesomeAsset extends AssetBundle
{
    public $sourcePath = '@vendor/almasaeed2010/adminlte/bower_components/font-awesome';
    public $css = [
        'css/font-awesome.css',
    ];
}