<?php

namespace app\modules\admin;

use yii\base\BootstrapInterface;
use yii\base\Module;
use yii\web\ErrorHandler;
use yii\web\User;

/**
 * Class AdminModule
 * @package app\modules\admin
 *
 * @property User $admin
 */
class AdminModule extends Module implements BootstrapInterface
{
    public $controllerNamespace = 'app\modules\admin\controllers';

    /** @var string  */
    public $layout = '@app/modules/admin/views/layouts/main.php';

    /**
     * @throws \yii\base\InvalidConfigException
     */
    public function init()
    {
        parent::init();

        // Инициализируем компоненты
        if (\Yii::$app instanceof \yii\web\Application) {
            \Yii::configure($this, require(__DIR__ . '/config/web.php'));
        }

        $this->defaultRoute = 'admin/index';
        \Yii::$app->homeUrl = '/admin';

        /** @var ErrorHandler $handler */
        $handler = $this->get('errorHandler');
        \Yii::$app->set('errorHandler', $handler);
        $handler->register();

        $this->setAliases([
            '@views' => '@app/modules/admin/views/'
        ]);
    }

    /**
     * @param \yii\base\Application $app
     */
    public function bootstrap($app)
    {
        $app->getUrlManager()->addRules([
            'admin' => 'admin/admin',
            'admin/error' => 'admin/admin/error',
            'admin/login' => 'admin/admin/login',
            'admin/logout' => 'admin/admin/logout',
            'admin/<controller:[\w-]+>' => 'admin/<controller>',
            'admin/<controller:[\w-]+>/<action:[\w-]+>' => 'admin/<controller>/<action>',
        ], false);
    }
}