<?php

namespace app\modules\admin\models\search;

use app\models\db\AdminQueryInterface;
use app\models\db\BrandAdvantage;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * Class BrandAdvantageSearch
 * @package app\modules\admin\models
 */
class BrandAdvantageSearch extends Model
{
    /** @var integer */
    public $id_brand;

    /**
     * @return array
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * @param $params
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = BrandAdvantage::find()
            ->where(['id_brand' => $this->id_brand])
        ;

        if ($query instanceof AdminQueryInterface) {
            $query->forAdmin();
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);
        return $dataProvider;
    }

}
