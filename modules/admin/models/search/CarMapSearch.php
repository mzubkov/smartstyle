<?php

namespace app\modules\admin\models\search;

use app\models\db\AdminQueryInterface;
use app\models\db\CarMap;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * Class CarMapSearch
 * @package app\modules\admin\models
 */
class CarMapSearch extends Model
{
    /** @var integer */
    public $id_model;

    /**
     * @return array
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * @param $params
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CarMap::find()
            ->where(['id_model' => $this->id_model])
        ;

        if ($query instanceof AdminQueryInterface) {
            $query->forAdmin();
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);
        return $dataProvider;
    }

}
