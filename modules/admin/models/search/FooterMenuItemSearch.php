<?php

namespace app\modules\admin\models\search;

use app\models\db\AdminQueryInterface;
use app\models\db\FooterMenuItem;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * Class FooterMenuItemSearch
 * @package app\modules\admin\models
 */
class FooterMenuItemSearch extends Model
{
    /** @var integer */
    public $id_folder;

    /**
     * @return array
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * @param $params
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = FooterMenuItem::find()
            ->where(['id_folder' => $this->id_folder])
        ;

        if ($query instanceof AdminQueryInterface) {
            $query->forAdmin();
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);
        return $dataProvider;
    }

}
