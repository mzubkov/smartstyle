<?php

namespace app\modules\admin\models\search;

use app\models\db\AdminQueryInterface;
use app\models\db\Staff;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * Class StaffSearch
 * @package app\modules\admin\models
 */
class StaffSearch extends Model
{
    /** @var integer */
    public $id_department;

    /**
     * @return array
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * @param $params
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Staff::find()
            ->where(['id_department' => $this->id_department])
        ;

        if ($query instanceof AdminQueryInterface) {
            $query->forAdmin();
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);
        return $dataProvider;
    }

}
