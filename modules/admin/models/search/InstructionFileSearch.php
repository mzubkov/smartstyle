<?php

namespace app\modules\admin\models\search;

use app\models\db\AdminQueryInterface;
use app\models\db\InstructionFile;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * Class InstructionFileSearch
 * @package app\modules\admin\models
 */
class InstructionFileSearch extends Model
{
    /** @var integer */
    public $id_item;

    /**
     * @return array
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * @param $params
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = InstructionFile::find()
            ->where(['id_item' => $this->id_item])
        ;

        if ($query instanceof AdminQueryInterface) {
            $query->forAdmin();
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);
        return $dataProvider;
    }

}
