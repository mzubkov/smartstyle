<?php

namespace app\modules\admin\models\search;

use app\modules\admin\models\AdminUser;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\Expression;

/**
 * Class UserSearch
 * @package app\modules\admin\models
 */
class UsersSearch extends AdminUser
{
    public $fullname;
    public $short_name;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['fullname', 'short_name', 'last_login', 'login'], 'safe'],
            [['last_login'], 'date'],
        ];
    }

    /**
     * @return array
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * @param $params
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AdminUser::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);

        $dataProvider->setSort([
            'attributes' => ['id', 'login', 'last_login'],
            'defaultOrder' => [
                'login' => SORT_ASC
            ]
        ]);

        if (!($this->load($params) && $this->validate())) {

            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['ilike', 'login', $this->login]);

        if (!empty($this->last_login)) {
            $query->andWhere(new Expression("last_login::date = :last_login", [
                'last_login' => $this->last_login
            ]));
        }
        return $dataProvider;
    }

}
