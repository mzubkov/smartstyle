<?php

namespace app\modules\admin\models\search;

use app\models\db\AdminQueryInterface;
use app\models\db\InstructionCategory;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * Class InstructionCategorySearch
 * @package app\modules\admin\models
 */
class InstructionCategorySearch extends Model
{
    /** @var integer */
    public $id_parent;

    /**
     * @return array
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * @param $params
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = InstructionCategory::find()
            ->where(['id_parent' => $this->id_parent])
        ;

        if ($query instanceof AdminQueryInterface) {
            $query->forAdmin();
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);
        return $dataProvider;
    }

}
