<?php

namespace app\modules\admin\models\search;

use app\models\db\TextBlock;
use yii\base\Model;
use yii\data\ArrayDataProvider;

/**
 * Class TextBlockSearch
 * @package app\modules\admin\models
 */
class TextBlockSearch extends Model
{
    /**
     * @return array
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * @param $params
     * @return ArrayDataProvider
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function search($params)
    {
        $dataProvider = new ArrayDataProvider([
            'key' => 'identifier',
            'allModels' => \Yii::$container->get('textBlockFinder')->getAll(),
            'modelClass' => TextBlock::class
        ]);

        return $dataProvider;
    }

}
