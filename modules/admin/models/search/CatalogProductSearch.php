<?php

namespace app\modules\admin\models\search;

use app\models\db\AdminQueryInterface;
use app\models\db\CatalogProduct;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * Class CatalogProductSearch
 * @package app\modules\admin\models
 */
class CatalogProductSearch extends Model
{
    /** @var integer */
    public $id_catalog;

    /**
     * @return array
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * @param $params
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CatalogProduct::find()
            ->where(['id_catalog' => $this->id_catalog])
        ;

        if ($query instanceof AdminQueryInterface) {
            $query->forAdmin();
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);
        return $dataProvider;
    }

}
