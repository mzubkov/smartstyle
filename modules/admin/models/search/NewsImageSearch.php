<?php

namespace app\modules\admin\models\search;

use app\models\db\AdminQueryInterface;
use app\models\db\NewsImage;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * Class NewsImageSearch
 * @package app\modules\admin\models
 */
class NewsImageSearch extends Model
{
    /** @var integer */
    public $id_news;

    /**
     * @return array
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * @param $params
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = NewsImage::find()
            ->where(['id_news' => $this->id_news])
        ;

        if ($query instanceof AdminQueryInterface) {
            $query->forAdmin();
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);
        return $dataProvider;
    }

}
