<?php

namespace app\modules\admin\models\search;

use app\models\db\AdminQueryInterface;
use yii\base\Model;
use yii\data\ActiveDataProvider;

class BaseSearch extends Model
{
    /** @var string */
    public $modelClass;

    /**
     * @param $params
     * @return ActiveDataProvider
     */
    public function search()
    {
        $query = $this->modelClass::find();

        if ($query instanceof AdminQueryInterface) {
            $query->forAdmin();
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);

        return $dataProvider;
    }
}