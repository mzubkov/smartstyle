<?php

namespace app\modules\admin\models\search;

use app\models\db\AdminQueryInterface;
use app\models\db\InstructionItem;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * Class InstructionItemSearch
 * @package app\modules\admin\models
 */
class InstructionItemSearch extends Model
{
    /** @var integer */
    public $id_category;

    /**
     * @return array
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * @param $params
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = InstructionItem::find()
            ->where(['id_category' => $this->id_category])
        ;

        if ($query instanceof AdminQueryInterface) {
            $query->forAdmin();
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);
        return $dataProvider;
    }

}
