<?php

namespace app\modules\admin\models\search;

use app\models\db\SysParam;
use yii\base\Model;
use yii\data\ArrayDataProvider;

/**
 * Class SysParamSearch
 * @package app\modules\admin\models
 */
class SysParamSearch extends Model
{
    /**
     * @return array
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * @param $params
     * @return ArrayDataProvider
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function search($params)
    {
        $dataProvider = new ArrayDataProvider([
            'key' => 'identifier',
            'allModels' => \Yii::$container->get('sysParamFinder')->getAll(),
            'modelClass' => SysParam::class
        ]);

        return $dataProvider;
    }

}
