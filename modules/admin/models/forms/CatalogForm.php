<?php

namespace app\modules\admin\models\forms;

use app\models\db\Catalog;
use yii\web\UploadedFile;

/**
 * Class CatalogForm
 * @package app\modules\admin\models
 */
class CatalogForm extends BaseForm
{
    use ImageFormTrait;

    /** @var integer */
    public $id;

    /** @var integer */
    public $id_brand;

    /** @var string*/
    public $name;


    /** @var boolean */
    public $is_published;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            ['is_published', 'boolean'],
            [['image'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, jpeg'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'name' => 'Название',
            'image' => 'Изображение',
            'is_published' => 'Опубликовано'
        ];
    }

    /**
     * @param Catalog $model
     */
    public function import($model)
    {
        parent::import($model);
        $this->image_src = $model->getImage('610x400');
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function create()
    {
        $this->image = UploadedFile::getInstance($this, 'image');
        return parent::create();
    }

    /**
     * @param Catalog $model
     * @return bool
     * @throws \Exception
     */
    public function edit($model)
    {
        $this->image = UploadedFile::getInstance($this, 'image');
        return parent::edit($model);
    }

    /**
     * @param Catalog $model
     * @return Catalog
     * @throws \Exception
     */
    protected function save($model)
    {
        parent::save($model);

        if ($this->image) {
            $this->saveImage($model);
        }

        return $model;
    }

}