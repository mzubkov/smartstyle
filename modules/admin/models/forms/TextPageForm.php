<?php

namespace app\modules\admin\models\forms;

/**
 * Class TextPageForm
 * @package app\modules\admin\models
 */
class TextPageForm extends BaseForm
{
    /** @var integer */
    public $id;

    /** @var string*/
    public $identifier;

    /** @var string*/
    public $name;

    /** @var string*/
    public $content;

    /** @var boolean */
    public $is_published;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['identifier', 'content', 'name'], 'required'],
            ['is_published', 'boolean']
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'identifier' => 'Идентификатор',
            'name' => 'Название',
            'content' => 'Текст',
            'is_published' => 'Опубликовано',
        ];
    }
}