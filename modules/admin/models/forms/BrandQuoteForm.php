<?php

namespace app\modules\admin\models\forms;

use app\models\db\BrandQuote;
use yii\web\UploadedFile;

/**
 * Class BrandQuoteForm
 * @package app\modules\admin\models
 */
class BrandQuoteForm extends BaseForm
{
    use ImageFormTrait;

    /** @var integer */
    public $id;

    /** @var integer */
    public $id_brand;

    /** @var string*/
    public $text;

    /** @var boolean */
    public $is_published;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['text'], 'required'],
            ['is_published', 'boolean'],
            [['image'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, jpeg'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'text' => 'Текст',
            'image' => 'Изображение',
            'is_published' => 'Опубликовано'
        ];
    }

    /**
     * @param BrandQuote $model
     */
    public function import($model)
    {
        parent::import($model);
        $this->image_src = $model->getImage('1872x450');
    }
}