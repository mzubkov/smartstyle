<?php

namespace app\modules\admin\models\forms;

use app\models\db\SysParam;
use yii\base\Model;

/**
 * Class SysParamForm
 * @package app\modules\admin\models
 */
class SysParamForm extends Model
{
    /** @var integer */
    public $id;

    /** @var string*/
    public $identifier;

    /** @var string*/
    public $name;

    /** @var string*/
    public $value;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['identifier', 'value', 'name'], 'required'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'identifier' => 'Идентификатор',
            'name' => 'Название',
            'value' => 'Значение'
        ];
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function create()
    {
        if ($this->validate()) {
            $this->save(new SysParam());
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param SysParam $model
     * @return bool
     * @throws \Exception
     */
    public function edit(SysParam $model)
    {
        if ($this->validate()) {
            $this->save($model);
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param SysParam $model
     * @return SysParam
     * @throws \Exception
     */
    protected function save(SysParam $model)
    {
        $model->setAttributes($this->getAttributes());

        if (!$model->save()) {
            throw new \Exception('Ошибка сохранения записи');
        }

        return $model;
    }

}