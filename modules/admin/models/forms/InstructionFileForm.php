<?php

namespace app\modules\admin\models\forms;

use app\models\db\InstructionFile;

/**
 * Class InstructionFileForm
 * @package app\modules\admin\models
 */
class InstructionFileForm extends BaseForm
{
    use FileFormTrait;

    /** @var integer */
    public $id;

    /** @var integer */
    public $id_item;

    /** @var string*/
    public $name;

    /** @var boolean */
    public $is_published;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['name'], 'safe'],
            ['is_published', 'boolean'],
            [['file'], 'file', 'skipOnEmpty' => true, 'extensions' => 'pdf, doc, docx'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'name' => 'Название',
            'is_published' => 'Опубликовано',
            'file' => 'Файл'
        ];
    }

    /**
     * @param InstructionFile $model
     */
    public function import($model)
    {
        parent::import($model);
        $this->file_url = $model->getFile();
    }

}