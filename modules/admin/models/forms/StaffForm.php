<?php

namespace app\modules\admin\models\forms;

use app\models\db\Staff;
use yii\web\UploadedFile;

/**
 * Class StaffForm
 * @package app\modules\admin\models
 */
class StaffForm extends BaseForm
{
    use ImageFormTrait;

    /** @var integer */
    public $id;

    /** @var integer */
    public $id_department;

    /** @var string*/
    public $name;

    /** @var string*/
    public $post;

    /** @var string*/
    public $phone;

    /** @var boolean */
    public $is_published;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['name', 'post', 'phone'], 'required'],
            ['is_published', 'boolean'],
            [['image'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, jpeg'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'name' => 'Имя',
            'post' => 'Должность',
            'phone' => 'Телефон',
            'is_published' => 'Опубликовано',
            'image' => 'Изображение'
        ];
    }

    /**
     * @param Staff $model
     */
    public function import($model)
    {
        parent::import($model);
        $this->image_src = $model->getImage('290x300');
    }
}