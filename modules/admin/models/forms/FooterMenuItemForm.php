<?php

namespace app\modules\admin\models\forms;

use app\models\db\FooterMenuItem;

/**
 * Class FooterMenuItemForm
 * @package app\modules\admin\models
 */
class FooterMenuItemForm extends BaseForm
{
    /** @var integer */
    public $id;

    /** @var integer */
    public $id_folder;

    /** @var string*/
    public $name;

    /** @var string*/
    public $url;

    /** @var boolean */
    public $is_published;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['name', 'url'], 'required'],
            ['is_published', 'boolean'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'name' => 'Название',
            'url' => 'URL',
            'is_published' => 'Опубликовано',
        ];
    }
}