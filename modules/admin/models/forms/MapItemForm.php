<?php

namespace app\modules\admin\models\forms;

/**
 * Class MapItemForm
 * @package app\modules\admin\models
 */
class MapItemForm extends BaseForm
{
    /** @var integer */
    public $id;

    /** @var integer */
    public $number;

    /** @var string*/
    public $text;

    /** @var boolean */
    public $is_published;

    public function rules()
    {
        return [
            [['text', 'number'], 'required'],
            ['is_published', 'boolean']
        ];
    }

    public function attributeLabels()
    {
        return [
            'number' => 'Цифра',
            'name' => 'Название',
            'is_published' => 'Опубликовано'
        ];
    }
}