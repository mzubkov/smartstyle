<?php

namespace app\modules\admin\models\forms;

use app\models\db\BrandVideo;
use yii\base\Model;

/**
 * Class BrandAdvantageForm
 * @package app\modules\admin\models
 */
class BrandVideoForm extends BaseForm
{
    /** @var integer */
    public $id;

    /** @var integer */
    public $id_brand;

    /** @var string*/
    public $name;

    /** @var string*/
    public $video_code;

    /** @var boolean */
    public $is_published;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['name', 'video_code'], 'required'],
            ['is_published', 'boolean']
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'name' => 'Заголовок',
            'video_code' => 'Ссылка на видео (youtube)',
            'is_published' => 'Опубликовано'
        ];
    }

    /**
     * @param BrandVideo $model
     * @return Model
     * @throws \Exception
     */
    protected function save($model)
    {
        $model->setAttributes($this->getAttributes());
        $model->video_code = $this->getCodeFromUrl($this->video_code);

        if (!$model->save()) {
            throw new \Exception('Ошибка сохранения записи');
        }

        return $model;
    }

    /**
     * @param string $value
     * @return |null
     */
    protected function getCodeFromUrl(string $value)
    {
        $video_code = null;
        if (strpos($value, "https://www.youtube.com/watch?v=") == 0) {
            $params = parse_url($value);
            $params = explode('&', $params['query']);
            foreach ($params as $param) {
                list($key, $value) = explode('=', $param);
                if ($key == 'v') {
                    $video_code = $value;
                    break;
                }
            }
        }

        return $video_code;
    }
}