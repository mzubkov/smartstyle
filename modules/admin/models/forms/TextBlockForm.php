<?php

namespace app\modules\admin\models\forms;

/**
 * Class TextBlockForm
 * @package app\modules\admin\models
 */
class TextBlockForm extends BaseForm
{
    /** @var integer */
    public $id;

    /** @var string*/
    public $identifier;

    /** @var string*/
    public $name;

    /** @var string*/
    public $content;

    /** @var string */
    public $column_type;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['identifier', 'content', 'name'], 'required'],
            ['column_type', 'string']
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'identifier' => 'Идентификатор',
            'name' => 'Название',
            'content' => 'Текст',
            'column_type' => 'Тип',
        ];
    }
}