<?php

namespace app\modules\admin\models\forms;

/**
 * Class DepartmentForm
 * @package app\modules\admin\models
 */
class DepartmentForm extends BaseForm
{
    /** @var integer */
    public $id;

    /** @var string*/
    public $name;

    /** @var boolean */
    public $is_published;

    public function rules()
    {
        return [
            [['name'], 'required'],
            ['is_published', 'boolean']
        ];
    }

    public function attributeLabels()
    {
        return [
            'name' => 'Название',
            'is_published' => 'Опубликовано'
        ];
    }
}