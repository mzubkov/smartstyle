<?php

namespace app\modules\admin\models\forms;

use app\models\db\Slideshow;

/**
 * Class SlideshowForm
 * @package app\modules\admin\models
 */
class SlideshowForm extends BaseForm
{
    use ImageFormTrait;

    /** @var integer */
    public $id;

    /** @var integer */
    public $position;

    /** @var string*/
    public $name;

    /** @var boolean */
    public $is_published;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['name'], 'safe'],
            ['is_published', 'boolean'],
            ['position', 'in', 'range' => [Slideshow::POSITION_HEADER, Slideshow::POSITION_CENTER, Slideshow::POSITION_BOTTOM]],
            [['image'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, jpeg'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'position' => 'Позиция',
            'name' => 'Название',
            'is_published' => 'Опубликовано',
            'image' => 'Изображение'
        ];
    }

    /**
     * @param Slideshow $model
     */
    public function import($model)
    {
        parent::import($model);
        $this->image_src = $model->getImage('300x75');
    }

}