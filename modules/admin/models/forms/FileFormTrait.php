<?php

namespace app\modules\admin\models\forms;

use app\components\FileService;
use app\models\db\BrandItem;
use app\models\FileModelInterface;
use yii\db\ActiveRecord;
use yii\web\UploadedFile;

/**
 * Trait FileFormTrait
 * @package app\modules\admin\models\forms
 */
trait FileFormTrait
{
    /** @var UploadedFile */
    public $file;

    /** @var string */
    public $file_url;

    /**
     * @return FileService
     */
    protected function getFileService()
    {
        return \Yii::$app->fileService;
    }

    /**
     * @param FileModelInterface|ActiveRecord $model
     * @throws \Exception
     */
    protected function saveFile($model)
    {
        $this->getFileService()->save($this->file, $model->getFileRepository());
        $model->file_ext = $this->file->extension;
        $model->save(false);
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function create()
    {
        $this->file = UploadedFile::getInstance($this, 'file');
        return parent::create();
    }

    /**
     * @param BrandItem $model
     * @return bool
     * @throws \Exception
     */
    public function edit($model)
    {
        $this->file = UploadedFile::getInstance($this, 'file');
        return parent::edit($model);
    }

    /**
     * @param BrandItem $model
     * @return BrandItem
     * @throws \Exception
     */
    protected function save($model)
    {
        parent::save($model);

        if ($this->file) {
            $this->saveFile($model);
        }

        return $model;
    }
}