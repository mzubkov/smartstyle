<?php

namespace app\modules\admin\models\forms;

use app\components\ImageService;
use app\models\db\BrandItem;
use app\models\ImageModelInterface;
use yii\db\ActiveRecord;
use yii\web\UploadedFile;

/**
 * Trait ImageFormTrait
 * @package app\modules\admin\models\forms
 */
trait ImageFormTrait
{
    /** @var UploadedFile */
    public $image;

    /** @var string */
    public $image_src;

    /**
     * @return ImageService
     */
    protected function getImageService()
    {
        return \Yii::$app->imageService;
    }

    /**
     * @param ImageModelInterface|ActiveRecord $model
     * @throws \Exception
     */
    protected function saveImage($model)
    {
        $this->getImageService()->save($this->image, $model->getImageRepository());
        $model->img_ext = $this->image->extension;
        $model->save(false);
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function create()
    {
        $this->image = UploadedFile::getInstance($this, 'image');
        return parent::create();
    }

    /**
     * @param BrandItem $model
     * @return bool
     * @throws \Exception
     */
    public function edit($model)
    {
        $this->image = UploadedFile::getInstance($this, 'image');
        return parent::edit($model);
    }

    /**
     * @param BrandItem $model
     * @return BrandItem
     * @throws \Exception
     */
    protected function save($model)
    {
        parent::save($model);

        if ($this->image) {
            $this->saveImage($model);
        }

        return $model;
    }
}