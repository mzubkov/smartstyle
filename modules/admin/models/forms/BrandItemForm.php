<?php

namespace app\modules\admin\models\forms;

use app\models\db\BrandItem;
use yii\web\UploadedFile;

/**
 * Class BrandItemForm
 * @package app\modules\admin\models
 */
class BrandItemForm extends BaseForm
{
    use ImageFormTrait;

    /** @var integer */
    public $id;

    /** @var integer */
    public $id_brand;

    /** @var string*/
    public $name;

    /** @var string*/
    public $text;

    /** @var boolean */
    public $is_published;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['name', 'text'], 'required'],
            ['is_published', 'boolean'],
            [['image'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, jpeg'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'name' => 'Название',
            'text' => 'Текст',
            'image' => 'Изображение',
            'is_published' => 'Опубликовано'
        ];
    }

    /**
     * @param BrandItem $model
     */
    public function import($model)
    {
        parent::import($model);
        $this->image_src = $model->getImage('100x100');
    }
}