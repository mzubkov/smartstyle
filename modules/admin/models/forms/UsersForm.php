<?php

namespace app\modules\admin\models\forms;

use app\modules\admin\models\AdminUser;
use yii\base\Model;

/**
 * Class UsersForm
 * @package app\modules\admin\models
 */
class UsersForm extends BaseForm
{
    /** @var string */
    public $login;

    /** @var string */
    public $password;

    /** @var string */
    public $passwordRepeat;

    public function rules()
    {
        return [
            [['login','password', 'passwordRepeat'], 'required'],
            ['login', 'unique', 'targetClass' => AdminUser::class, 'targetAttribute' => 'login'],
            ['password', 'string'],
            ['passwordRepeat', 'compare', 'compareAttribute' => 'password'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'login' => 'Логин',
            'password' => 'Пароль',
            'passwordRepeat' => 'Повторите пароль'
        ];
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function create()
    {
        if ($this->validate()) {
            $user = $this->save(new $this->objectClass());
            $user->setPassword($this->password);
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param AdminUser $user
     * @return bool
     * @throws \yii\base\Exception
     */
    public function edit($user)
    {
        if ($this->validate()) {
            $user->login = $this->login;
            $user->setPassword($this->password);
            return $user->save();
        } else {
            return false;
        }
    }
}