<?php

namespace app\modules\admin\models\forms;

use app\models\db\CatalogProduct;
use yii\web\UploadedFile;

/**
 * Class CatalogProductForm
 * @package app\modules\admin\models
 */
class CatalogProductForm extends BaseForm
{
    use ImageFormTrait;

    /** @var integer */
    public $id;

    /** @var integer */
    public $id_catalog;

    /** @var string*/
    public $name;


    /** @var boolean */
    public $is_published;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            ['is_published', 'boolean'],
            [['image'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, jpeg'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'name' => 'Название',
            'image' => 'Изображение',
            'is_published' => 'Опубликовано'
        ];
    }

    /**
     * @param CatalogProduct $model
     */
    public function import($model)
    {
        parent::import($model);
        $this->image_src = $model->getImage('290x290');
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function create()
    {
        $this->image = UploadedFile::getInstance($this, 'image');
        return parent::create();
    }

    /**
     * @param CatalogProduct $model
     * @return bool
     * @throws \Exception
     */
    public function edit($model)
    {
        $this->image = UploadedFile::getInstance($this, 'image');
        return parent::edit($model);
    }

    /**
     * @param CatalogProduct $model
     * @return CatalogProduct
     * @throws \Exception
     */
    protected function save($model)
    {
        parent::save($model);

        if ($this->image) {
            $this->saveImage($model);
        }

        return $model;
    }

}