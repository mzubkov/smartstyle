<?php

namespace app\modules\admin\models\forms;

use app\models\db\SellItem;
use yii\web\UploadedFile;

/**
 * Class SellItemForm
 * @package app\modules\admin\models
 */
class SellItemForm extends BaseForm
{
    use ImageFormTrait;

    /** @var integer */
    public $id;

    /** @var string*/
    public $text;

    /** @var boolean */
    public $is_published;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['text'], 'safe'],
            ['is_published', 'boolean'],
            [['image'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, jpeg, svg'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'text' => 'Текст',
            'is_published' => 'Опубликовано',
            'image' => 'Изображение'
        ];
    }

    /**
     * @param SellItem $model
     */
    public function import($model)
    {
        parent::import($model);
        $this->image_src = $model->getImage('100x100');
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function create()
    {
        $this->image = UploadedFile::getInstance($this, 'image');
        return parent::create();
    }

    /**
     * @param SellItem $model
     * @return bool
     * @throws \Exception
     */
    public function edit($model)
    {
        $this->image = UploadedFile::getInstance($this, 'image');
        return parent::edit($model);
    }

    /**
     * @param SellItem $model
     * @return SellItem
     * @throws \Exception
     */
    protected function save($model)
    {
        parent::save($model);

        if ($this->image) {
            $this->saveImage($model);
        }

        return $model;
    }

}