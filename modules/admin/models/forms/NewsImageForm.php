<?php

namespace app\modules\admin\models\forms;

use app\models\db\NewsImage;

/**
 * Class NewsImageForm
 * @package app\modules\admin\models
 */
class NewsImageForm extends BaseForm
{
    use ImageFormTrait;

    /** @var integer */
    public $id;

    /** @var integer */
    public $id_news;

    /** @var string*/
    public $name;

    /** @var boolean */
    public $is_published;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['name'], 'string'],
            ['is_published', 'boolean'],
            [['image'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, jpeg'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'name' => 'Название',
            'is_published' => 'Опубликовано',
            'image' => 'Изображение'
        ];
    }

    /**
     * @param NewsImage $model
     */
    public function import($model)
    {
        parent::import($model);
        $this->image_src = $model->getImage('610x408');
    }
}