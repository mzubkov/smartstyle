<?php

namespace app\modules\admin\models\forms;

use app\models\db\Admin;
use yii\base\Model;

/**
 * Class AddNewUserForm
 * @package app\modules\admin\models
 */
class EditProfileForm extends Model
{
    /** @var string */
    public $first_name;
    /** @var string */
    public $last_name;

    /** @var string */
    public $fileLink;

    public function rules()
    {
        return [
            [['first_name', 'last_name'], 'string'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'first_name' => 'Имя',
            'last_name'  => 'Фамилия',
        ];
    }

    /**
     * @return bool
     * @throws \yii\base\Exception
     */
    public function save(Admin $profile)
    {
        if ($this->validate()) {
            $profile->first_name = $this->first_name;
            $profile->last_name  = $this->last_name;
            return $profile->save();
        } else {
            return false;
        }
    }

    public function load($data, $formName = null, $file = null)
    {
        $this->setAttributes($data['EditProfileForm'], true);
        if ($file) {
            $this->file = $file;
        }
        return true;
    }

    public function fillFromDB($values)
    {
        parent::setAttributes($values, true);
    }
}