<?php

namespace app\modules\admin\models\forms;

/**
 * Class InstructionCategoryForm
 * @package app\modules\admin\models
 */
class InstructionCategoryForm extends BaseForm
{
    /** @var integer */
    public $id;

    /** @var string*/
    public $name;

    /** @var boolean */
    public $is_published;

    /** @var integer */
    public $id_parent;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['name'], 'safe'],
            ['is_published', 'boolean'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'name' => 'Название',
            'is_published' => 'Опубликовано',
        ];
    }


}