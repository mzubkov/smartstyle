<?php

namespace app\modules\admin\models\forms;

/**
 * Class InstructionItemForm
 * @package app\modules\admin\models
 */
class InstructionItemForm extends BaseForm
{
    /** @var integer */
    public $id;

    /** @var integer */
    public $id_category;

    /** @var string*/
    public $name;

    /** @var boolean */
    public $is_published;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['name'], 'safe'],
            ['is_published', 'boolean'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'name' => 'Название',
            'is_published' => 'Опубликовано',
        ];
    }

}