<?php

namespace app\modules\admin\models\forms;

use app\models\ClientUser;

/**
 * Class ClientForm
 * @package app\modules\admin\models
 */
class ClientForm extends BaseForm
{
    /** @var string */
    public $name;

    /** @var string */
    public $login;

    /** @var string */
    public $password;

    /** @var string */
    public $passwordRepeat;

    public function rules()
    {
        return [
            ['name', 'string'],
            [['login', 'password', 'passwordRepeat'], 'required'],
            ['password', 'string'],
            ['passwordRepeat', 'compare', 'compareAttribute' => 'password'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'name' => 'Имя',
            'login' => 'Логин',
            'password' => 'Пароль',
            'passwordRepeat' => 'Повторите пароль'
        ];
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function create()
    {
        if ($this->validate()) {
            $user = $this->save(new $this->objectClass());
            $user->setPassword($this->password);
            return $user->save();
        } else {
            return false;
        }
    }

    /**
     * @param ClientUser $user
     * @return bool
     * @throws \Exception
     */
    public function edit($user)
    {
        if ($this->validate()) {
            $user = $this->save($user);
            $user->setPassword($this->password);
            return $user->save();
        } else {
            return false;
        }
    }
}