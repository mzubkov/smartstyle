<?php

namespace app\modules\admin\models\forms;

/**
 * Class BrandAdvantageForm
 * @package app\modules\admin\models
 */
class BrandAdvantageForm extends BaseForm
{
    /** @var integer */
    public $id;

    /** @var integer */
    public $id_brand;

    /** @var string*/
    public $text;


    /** @var boolean */
    public $is_published;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['text'], 'required'],
            ['is_published', 'boolean']
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'text' => 'Текст',
            'is_published' => 'Опубликовано'
        ];
    }

}