<?php

namespace app\modules\admin\models\forms;

use app\models\db\CarBrand;

/**
 * Class CarBrandForm
 * @package app\modules\admin\models
 */
class CarBrandForm extends BaseForm
{
    use ImageFormTrait;

    /** @var integer */
    public $id;

    /** @var string*/
    public $name;

    /** @var boolean */
    public $is_published;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['name'], 'safe'],
            ['is_published', 'boolean'],
            [['image'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, jpeg'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'name' => 'Название',
            'is_published' => 'Опубликовано',
            'image' => 'Изображение'
        ];
    }

    /**
     * @param CarBrand $model
     */
    public function import($model)
    {
        parent::import($model);
        $this->image_src = $model->getImage('290x70');
    }

}