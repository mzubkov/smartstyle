<?php

namespace app\modules\admin\models\forms;

/**
 * Class NavigationForm
 * @package app\modules\admin\models
 */
class NavigationForm extends BaseForm
{
    /** @var integer */
    public $id;

    /** @var string*/
    public $name;

    /** @var string */
    public $url;

    /** @var boolean */
    public $is_published;

    public function rules()
    {
        return [
            [['name', 'url'], 'required'],
            ['is_published', 'boolean']
        ];
    }

    public function attributeLabels()
    {
        return [
            'name' => 'Название',
            'url' => 'URL',
            'is_published' => 'Опубликовано'
        ];
    }
}