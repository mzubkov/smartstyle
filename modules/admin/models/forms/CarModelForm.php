<?php

namespace app\modules\admin\models\forms;

use app\models\db\CarModel;

/**
 * Class CarModelForm
 * @package app\modules\admin\models
 */
class CarModelForm extends BaseForm
{
    use ImageFormTrait;

    /** @var integer */
    public $id;

    /** @var integer */
    public $id_brand;

    /** @var string*/
    public $name;

    /** @var boolean */
    public $is_published;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['name'], 'safe'],
            ['is_published', 'boolean'],
            [['image'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, jpeg'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'name' => 'Название',
            'is_published' => 'Опубликовано',
            'image' => 'Изображение'
        ];
    }

    /**
     * @param CarModel $model
     */
    public function import($model)
    {
        parent::import($model);
        $this->image_src = $model->getImage('290x165');
    }

}