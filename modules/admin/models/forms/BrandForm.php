<?php

namespace app\modules\admin\models\forms;

use app\models\db\Brand;
use yii\web\UploadedFile;

/**
 * Class BrandForm
 * @package app\modules\admin\models
 */
class BrandForm extends BaseForm
{
    use ImageFormTrait;

    /** @var integer */
    public $id;

    /** @var string*/
    public $name;

    /** @var string*/
    public $description;

    /** @var string*/
    public $content;

    /** @var UploadedFile */
    public $logo;

    /** @var string */
    public $logo_src;

    /** @var UploadedFile */
    public $page_image;

    /** @var string */
    public $page_image_src;

    /** @var boolean */
    public $is_published;

    /** @var Brand */
    private $model;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            ['description', 'string'],
            [['content'], 'safe'],
            ['is_published', 'boolean'],
            [['image', 'logo', 'page_image'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, jpeg, svg'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'name' => 'Название',
            'description' => 'Описание',
            'content' => 'Текст',
            'is_published' => 'Опубликовано',
            'image' => 'Изображение',
            'logo' => 'Логотип',
            'page_image' => 'Изображение в шапке'
        ];
    }

    /**
     * @param Brand $model
     */
    public function import($model)
    {
        parent::import($model);
        $this->image_src = $model->getImage('610x400');
        $this->logo_src = $model->getLogo();
        $this->page_image_src = $model->getPageImage();
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function create()
    {
        $this->image = UploadedFile::getInstance($this, 'image');
        $this->logo = UploadedFile::getInstance($this, 'logo');
        $this->page_image = UploadedFile::getInstance($this, 'page_image');
        return parent::create();
    }

    /**
     * @param Brand $model
     * @return bool
     * @throws \Exception
     */
    public function edit($model)
    {
        $this->image = UploadedFile::getInstance($this, 'image');
        $this->logo = UploadedFile::getInstance($this, 'logo');
        $this->page_image = UploadedFile::getInstance($this, 'page_image');
        return parent::edit($model);
    }

    /**
     * @param Brand $model
     * @return Brand
     * @throws \Exception
     */
    protected function save($model)
    {
        parent::save($model);

        if ($this->image) {
            $this->saveImage($model);
        }

        if ($this->logo) {
            $this->saveLogo($model);
        }

        if ($this->page_image) {
            $this->savePageImage($model);
        }

        return $model;
    }

    /**
     * @param Brand $model
     * @throws \Exception
     */
    protected function saveLogo(Brand $model)
    {
        $this->getImageService()->save($this->logo, $model->getLogoRepository());
        $model->logo_ext = $this->logo->extension;
        $model->save(false);
    }

    /**
     * @param Brand $model
     * @throws \Exception
     */
    protected function savePageImage(Brand $model)
    {
        $this->getImageService()->save($this->page_image, $model->getPageImageRepository());
        $model->page_img_ext = $this->page_image->extension;
        $model->save(false);
    }
}