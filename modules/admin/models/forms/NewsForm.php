<?php

namespace app\modules\admin\models\forms;

/**
 * Class NewsForm
 * @package app\modules\admin\models
 */
class NewsForm extends BaseForm
{
    /** @var integer */
    public $id_brand;

    /** @var string */
    public $name;

    /** @var string */
    public $annotation;

    /** @var string */
    public $content;

    /** @var string */
    public $news_date;

    /** @var boolean */
    public $is_published;

    public function rules()
    {
        return [
            ['id_brand', 'integer'],
            [['name'], 'required'],
            ['annotation', 'string'],
            ['news_date', 'date', 'format' => 'php:Y-m-d'],
            [['content'], 'safe'],
            ['is_published', 'boolean'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'name' => 'Заголовок',
            'annotation' => 'Краткое описание',
            'content' => 'Текст новости',
            'news_date' => 'Дата',
            'is_published' => 'Опубликовано',
            'id_brand' => 'Бренд'
        ];
    }
}