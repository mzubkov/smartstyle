<?php

namespace app\modules\admin\models\forms;

use yii\base\Model;

/**
 * Class BaseForm
 * @package app\modules\admin\models\forms
 */
abstract class BaseForm extends Model
{
    public $objectClass;

    /**
     * @return bool
     * @throws \Exception
     */
    public function create()
    {
        if ($this->validate()) {
            $this->save(new $this->objectClass());
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param Model $model
     * @return bool
     * @throws \Exception
     */
    public function edit($model)
    {
        if ($this->validate()) {
            $this->save($model);
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param Model $model
     * @return Model
     * @throws \Exception
     */
    protected function save($model)
    {
        $model->setAttributes($this->getAttributes());

        if (!$model->save()) {
            throw new \Exception('Ошибка сохранения записи');
        }

        return $model;
    }

    /**
     * @param Model $model
     */
    public function import($model)
    {
        $this->setAttributes($model->getAttributes());
    }
}