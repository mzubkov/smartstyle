<?php

namespace app\modules\admin\grid;

use yii\grid\Column;
use yii\helpers\Html;

class StatusColumn extends Column
{
    /**
     * @return string
     */
    protected function renderHeaderCellContent()
    {
        return 'Статус';
    }

    /**
     * @param mixed $model
     * @param mixed $key
     * @param int $index
     * @return string
     */
    protected function renderDataCellContent($model, $key, $index)
    {
        return Html::tag('i', '&nbsp', [
            'class' => 'fa fa-fw fa-lightbulb-o',
            'style' => 'font-size: 2rem;color: ' . ($model->is_published ? 'green' : 'red')
        ]);
    }
}