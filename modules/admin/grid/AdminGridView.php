<?php

namespace app\modules\admin\grid;

use yii\grid\GridView;

class AdminGridView extends GridView
{
    public $layout = "{items}\n{pager}\n{summary}";
}