<?php

namespace app\modules\admin\widgets;

use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

/**
 * Class Form
 * @package app\modules\admin\widgets
 */
class Form extends ActiveForm
{
    /**
     * @param $model
     * @param $attribute
     * @param array $options
     * @return object
     * @throws \yii\base\InvalidConfigException
     */
    public function imageField($model, $attribute, $options = [])
    {
        $config = $this->fieldConfig;
        if ($config instanceof \Closure) {
            $config = call_user_func($config, $model, $attribute);
        }
        $config['class'] = ImageField::class;

        return \Yii::createObject(ArrayHelper::merge($config, $options, [
            'model' => $model,
            'attribute' => $attribute,
            'form' => $this,
        ]));
    }

    /**
     * @param $model
     * @param $attribute
     * @param array $options
     * @return object
     * @throws \yii\base\InvalidConfigException
     */
    public function fileField($model, $attribute, $options = [])
    {
        $config = $this->fieldConfig;
        if ($config instanceof \Closure) {
            $config = call_user_func($config, $model, $attribute);
        }
        $config['class'] = FileField::class;

        return \Yii::createObject(ArrayHelper::merge($config, $options, [
            'model' => $model,
            'attribute' => $attribute,
            'form' => $this,
        ]));
    }
}