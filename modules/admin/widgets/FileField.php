<?php

namespace app\modules\admin\widgets;

use yii\widgets\ActiveField;

/**
 * Class FileField
 * @package app\modules\admin\widgets
 */
class FileField extends ActiveField
{
    /**
     * @param array $options
     * @return $this|ActiveField
     */
    public function fileInput($options = [])
    {
        parent::fileInput($options);
        $field = $this->attribute . '_url';
        $this->parts['{hint}'] = "<a href='{$this->model->$field}'>{$this->model->$field}</a>";

        return $this;
    }
}