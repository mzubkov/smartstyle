<?php

namespace app\modules\admin\widgets;

class Breadcrumbs extends \yii\widgets\Breadcrumbs
{
    public $homeLink = [
        'label' => 'Главная',
        'url' => '/admin'
    ];
}