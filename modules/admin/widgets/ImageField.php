<?php

namespace app\modules\admin\widgets;

use yii\widgets\ActiveField;

/**
 * Class ImageField
 * @package app\modules\admin\widgets
 */
class ImageField extends ActiveField
{
    /**
     * @param array $options
     * @return $this|ActiveField
     */
    public function fileInput($options = [])
    {
        parent::fileInput($options);
        $field = $this->attribute . '_src';
        $this->parts['{hint}'] = "<img src='{$this->model->$field}' width='100'>";

        return $this;
    }
}