<?php

namespace app\modules\admin\widgets;

use yii\bootstrap\Widget;
use yii\helpers\Html;
use yii\helpers\Url;

class Actions extends Widget
{
    protected $_availableBtn = [
        'view'   => ['type' => 'view', 'name' => 'Просмотр', 'class' => 'btn-info', 'icon' => 'fa-eye'],
        'create' => ['type' => 'create', 'name' => 'Добавить', 'class' => 'btn-success', 'icon' => 'fa-plus'],
        'edit'   => ['type' => 'edit', 'name' => 'Редактировать', 'class' => 'btn-warning', 'icon' => 'fa-edit'],
        'delete' => ['type' => 'delete', 'name' => 'Удалить', 'class' => 'btn-danger delete-object', 'icon' => 'fa-trash']
    ];

    public $buttons = [];
    public $controller;
    public $item;
    public $itemKey = 'id';
    public $showTitle = true;
    public $reloadPath = false;

    /**
     * @return array
     * @throws \Exception
     */
    protected function getButtons()
    {
        $buttons = [];
        foreach ($this->buttons as $button) {
            if (isset($this->_availableBtn[$button])) {
                $buttons[$button] = $this->_availableBtn[$button];
            }
        }

        return $buttons;
    }

    /**
     * @param $button
     * @return string
     * @throws \Exception
     */
    protected function getUrl($button)
    {
        $id = $this->itemKey;
        switch ($button['type']) {
            case 'view':
                return Url::toRoute(["{$this->controller}/view", 'id' => $this->item->$id]);

            case 'create':
                return ($this->item)
                    ? Url::toRoute(["{$this->controller}/create", 'id' => $this->item->$id])
                    : Url::toRoute(["{$this->controller}/create"]);

            case 'edit':
                return Url::toRoute(["{$this->controller}/edit", 'id' => $this->item->$id]);

            case 'delete':
                return Url::toRoute(["{$this->controller}/delete", 'id' => $this->item->$id]);

            default:
                throw new \Exception('Тип действия на найден');
                break;
        }
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function run()
    {
        $html = [];

        foreach ($this->getButtons() as $button) {
            $options = [
                'class' => 'btn btn-xs ' . $button['class']
            ];

            $html[] = Html::a(
                $button['name'] . '&nbsp;' . Html::tag('i', '', ['class' => 'fa ' . $button['icon']]),
                $this->getUrl($button),
                $options
            );
        }

        return implode(' ', $html);
    }
}