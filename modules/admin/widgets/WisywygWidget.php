<?php

namespace app\modules\admin\widgets;

use dosamigos\tinymce\TinyMce;

class WisywygWidget extends TinyMce
{
    public $language = 'ru';

    public $clientOptions = [
        'plugins' => [
            'advlist autolink lists link image charmap anchor',
            'searchreplace visualblocks code fullscreen',
            'insertdatetime media table paste code help wordcount'
        ],
        'toolbar' => [
            'undo redo | bold italic backcolor | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat',
            'fontselect fontsizeselect formatselect | link image'
        ],
        'valid_elements' => 'strong/b,p,q,div,a,br,h1,h2,h3,h4,h5',
        'entity_encoding' => "raw",
        'menubar' => false,
        'branding' => false
    ];
}