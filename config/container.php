<?php

use app\models\SysParamFinder;
use app\models\TextBlockFinder;

return [
    'definitions' => [
    ],
    'singletons' => [
        'textBlockFinder' => function () {
            return new TextBlockFinder();
        },
        'sysParamFinder' => function () {
            return new SysParamFinder();
        },
    ]
];
