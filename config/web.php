<?php

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';
$mailer = require __DIR__ . '/mailer.php';
$container = require __DIR__ . '/container.php';

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'language' => 'ru_RU',
    'name' => 'Смартстиль',
    'bootstrap' => ['log', 'admin'],
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'modules' => [
        'admin' => [
            'class' => \app\modules\admin\AdminModule::class
        ],
    ],
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'ALssJP1h0OqefMbaALZL-LUUZmp8JQKG',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'class' => 'app\models\WebUser',
            'identityClass' => 'app\models\ClientUser',
            'enableAutoLogin' => false,
            'returnUrl' => '/'
        ],
        'errorHandler' => [
            'class' => yii\web\ErrorHandler::class,
            'errorAction' => 'site/error',
        ],
        'mailer' => $mailer,
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,
        'urlManager' => [
            'enablePrettyUrl' => true,
            'enableStrictParsing' => true,
            'showScriptName' => false,
            'rules' => [
                [
                    'class' => \app\common\BrandUrlRule::class
                ],

                /*
                [
                    'class' => \app\common\DocumentUrlRule::class
                ],
                */

                '' => 'site/index',
                'login' => 'site/login',
                'logout' => 'site/logout',

                'news/<year:\d{4}>' => 'news/year',
                'news/<year:\d{4}>/<identifier:[\w-]+>' => 'news/detail',
                '<controller:[\w-]+>' => '<controller>',
                '<controller:[\w-]+>/<id:\d+>' => '<controller>/view',
                '<controller:[\w-]+>/<action:[\w-]+>/<id:\d+>' => '<controller>/<action>',
                '<controller:[\w-]+>/<action:[\w-]+>' => '<controller>/<action>',
            ],
        ],
        'imageService' => [
            'class' => 'app\components\ImageService',
            'storePath' => '@app/web'
        ],
        'fileService' => [
            'class' => 'app\components\FileService',
            'storePath' => '@app/web'
        ]
    ],
    'params' => $params,
    'container' => $container,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        'allowedIPs' => ['*'],
    ];
}

return $config;
