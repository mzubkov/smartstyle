<?php

return [
    'site_title' => [
        'name' => 'Название сайта',
        'value' => 'Смартстиль',
    ],
    'from_mail' => [
        'name' => 'Почта отправилетя',
        'value' => 'noreply@gmail.com',
    ],
    'contacts_email' => [
        'name' => 'Почта для контактов',
        'value' => 'zubkov.michael@gmail.com',
    ]
];