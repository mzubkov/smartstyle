<?php

use app\models\db\TextBlock;

return [
    'main_page_head' => [
        'name' => 'Заголовок на главной страницы',
        'content' => 'Официальный дистрибьютор автомобильной электроники <span>с 2013 года</span>',
        'column_type' => TextBlock::TYPE_INPUT
    ],
    'main_page_text' => [
        'name' => 'Текст для главной страницы',
        'content' => '<p>Мы - команда профессионалов в сфере продаж охранных систем Pandora, Pandect, Parkmaster, iDatalink, Mio. <br>
            В работе сочетаем огромный опыт и смарт-подход к ведению бизнеса. На первом месте качество. На втором - выполнение обязательств. <br>
            На третьем - уважение к партнерам и хорошие цены. На четвертом - максимальная доступность, обучение, техподдержка.</p>',
        'column_type' => TextBlock::TYPE_WYSIWYG
    ],
    'news_text' => [
        'name' => 'Текст для страницы новостей',
        'content' => '<h1>Новости и события</h1>
            <p>Быть в курсе последних новостей наших производителей.</p>',
        'column_type' => TextBlock::TYPE_WYSIWYG
    ],
    'maps_text' => [
        'name' => 'Текст для страницы технологических карт',
        'content' => '<h1>Технический отдел Смарт Стиль</h1>
            <p>Опытные установщики. Используем smart-решения в работе. Мы умеем делать то, что не могут другие!</p>',
        'column_type' => TextBlock::TYPE_WYSIWYG
    ],
    'team_head' => [
        'name' => 'Наша команда',
        'content' => 'Наша команда',
        'column_type' => TextBlock::TYPE_INPUT
    ],
    'team_text' => [
        'name' => 'Текст для нашей команды',
        'content' => '<p>Текст для нашей команды</p>',
        'column_type' => TextBlock::TYPE_WYSIWYG
    ],
    'partner_head' => [
        'name' => 'Партнеры - заголовок',
        'content' => 'Крупнейшие автосалоны Москвы - наши клиенты',
        'column_type' => TextBlock::TYPE_INPUT
    ],
    'partner_text' => [
        'name' => 'Текст для партнеров',
        'content' => '<p>Нашими партнерами уже много лет являются салоны: Фаворит, Рольф, Major, У-Сервис, Ирбис и многие другие.</p>',
        'column_type' => TextBlock::TYPE_WYSIWYG
    ],
    'partner_items_head' => [
        'name' => 'Быть партнером - заголовок',
        'content' => 'Быть партнером - просто, <span>как 1-2-3</span>',
        'column_type' => TextBlock::TYPE_INPUT
    ],
    'partner_items_text' => [
        'name' => 'Текст для блока "Быть партнером"',
        'content' => '<p>Простота и прозрачность операций между компанией и ее партнерами - важнейший аспект для нас. <br>
                Так же, как порядочность, честность и уважение.</p>',
        'column_type' => TextBlock::TYPE_WYSIWYG
    ],
    'sell_head' => [
        'name' => 'Как продавать - заголовок',
        'content' => 'Расскажем, как продавать и устанавливать',
        'column_type' => TextBlock::TYPE_INPUT
    ],
    'sell_text' => [
        'name' => 'Текст для блока "Быть партнером"',
        'content' => '<p>Благодаря высочайшему качеству наши охранные системы продают сами себя. Тем не менее, мы всегда оказываем помощь в продвижении продукции.
                <br>
                Каждый новый партнер получает индивидуального менеджера из отдела сопровождения Смарт-Стиль:</p>',
        'column_type' => TextBlock::TYPE_WYSIWYG
    ],
    'tech_head1' => [
        'name' => 'Ззаголовок1 для формы техподдержки',
        'content' => 'Техподдержка 24/7',
        'column_type' => TextBlock::TYPE_INPUT
    ],
    'tech_head2' => [
        'name' => 'Заголовок2 для формы техподдержки',
        'content' => 'мы обучаем и консультируем',
        'column_type' => TextBlock::TYPE_INPUT
    ],
    'tech_text' => [
        'name' => 'Текст для формы техподдержки',
        'content' => '
<p>Работая с нами, Вы получаете мощную техподдержку по продуктам.
                В нашем штате 5 установщиков под руководством опытного техдиректора.</p>
            <ul class="team-group_support-list">
                <li>Подробные консультации по телефону.</li>
                <li>Выездной шеф-монтаж, обучение.</li>
                <li>Авторская установка, решение задач любой сложности.</li>
            </ul>        ',
        'column_type' => TextBlock::TYPE_WYSIWYG
    ],
    'feedback_head' => [
        'name' => 'Заголовок для формы обратной связи',
        'content' => 'Заполните анкету и узнайте индивидуальные цены',
        'column_type' => TextBlock::TYPE_INPUT
    ],
    'feedback_text' => [
        'name' => 'Текст для формы обратной связи',
        'content' => '<p>Заполните анкету и получите прайс-листы и стартовые условия. Скидка - динамическая. <br>
            В зависимости от объема закупок в течение месяца она будет увеличиваться.</p>',
        'column_type' => TextBlock::TYPE_WYSIWYG
    ],
    'feedback_form_options1' => [
        'name' => 'Область деятельности (каждое значение в новой строке)',
        'content' => "Область деятельности\nОбласть деятельности1\nОбласть деятельности2\nОбласть деятельности3",
        'column_type' => TextBlock::TYPE_TEXTAREA
    ],
    'feedback_form_options2' => [
        'name' => 'Объем закупок в месяц (каждое значение в новой строке)',
        'content' => "Объем закупок в месяц\nОбъем закупок в месяц1\nОбъем закупок в месяц2\nОбъем закупок в месяц3",
        'column_type' => TextBlock::TYPE_TEXTAREA
    ],
    'header_phone' => [
        'name' => 'Телефон в шапке',
        'content' => '+7 (495) 777-44-44',
        'column_type' => TextBlock::TYPE_INPUT
    ],
    'footer_phone' => [
        'name' => 'Телефон в футере',
        'content' => '+7 (495) 777-44-44',
        'column_type' => TextBlock::TYPE_INPUT
    ],
    'footer_address' => [
        'name' => 'Адрес в футере',
        'content' => 'Бизнес-Парк "Румянцево".\nКиевское шоссе, строение 1, корпус Б.\nТелефон: +7 (495) 777-44-44',
        'column_type' => TextBlock::TYPE_TEXTAREA
    ],
    'footer_sell_head' => [
        'name' => 'Футер, отдел продаж - заголовок',
        'content' => 'Отдел продаж:',
        'column_type' => TextBlock::TYPE_INPUT
    ],
    'footer_sell_text' => [
        'name' => 'Футер, отдел продаж - текст',
        'content' => '<p>9 этаж, офис 907Б <br>
                    Время работы: <br>
                    пн-чт: 9.00-18.00 <br>
                    пт: 9.00-17.00</p>',
        'column_type' => TextBlock::TYPE_WYSIWYG
    ],
    'footer_centr_head' => [
        'name' => 'Футер, установочный центр - заголовок',
        'content' => 'Установочный Центр:',
        'column_type' => TextBlock::TYPE_INPUT
    ],
    'footer_centr_text' => [
        'name' => 'Футер, установочный центр - текст',
        'content' => '<p>8 этаж <br>
                    Время работы: <br>
                    ежедневно 9.00-20.00 <br>
                    (по выходным предварительная запись обязательна)</p>',
        'column_type' => TextBlock::TYPE_WYSIWYG
    ],
    'copyright' => [
        'name' => 'Копирайт',
        'content' => '<p>Copyright © 2017 Смарт-Стиль. Все права защищены.</p>',
        'column_type' => TextBlock::TYPE_WYSIWYG
    ],
];