<?php

return [
    'path' => '/media/slideshow',
    'variants' => [
        '1872x450' => [
            'width' => 1872,
            'height' => 450,
            'method' => 'resize_and_crop'
        ],
        '300x75' => [
            'width' => 300,
            'height' => 75,
            'method' => 'resize_and_crop'
        ]
    ]
];