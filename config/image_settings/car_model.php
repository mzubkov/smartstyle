<?php

return [
    'path' => '/media/car_model',
    'variants' => [
        '290x165' => [
            'width' => 290,
            'height' => 165,
            'method' => 'resize_and_crop'
        ]
    ]
];