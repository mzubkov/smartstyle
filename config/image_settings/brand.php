<?php

return [
    'path' => '/media/brand',
    'variants' => [
        '610x400' => [
            'width' => 610,
            'height' => 400,
            'method' => 'resize_and_crop'
        ],
        '290x400' => [
            'width' => 290,
            'height' => 400,
            'method' => 'resize_and_crop'
        ],
    ]
];