<?php

return [
    'path' => '/media/brand/page_image',
    'variants' => [
        'page_image' => [
            'width' => 1872,
            'height' => 450,
            'method' => 'resize_and_crop'
        ],
    ]
];