<?php

return [
    'path' => '/media/partner_item',
    'variants' => [
        '100x100' => [
            'width' => 100,
            'height' => 100,
            'method' => 'resize_and_crop'
        ]
    ]
];