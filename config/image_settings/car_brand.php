<?php

return [
    'path' => '/media/car_brand',
    'variants' => [
        '290x70' => [
            'width' => 290,
            'height' => 70,
            'method' => 'resize_and_crop'
        ]
    ]
];