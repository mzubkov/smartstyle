<?php

return [
    'path' => '/media/catalog_product',
    'variants' => [
        '290x290' => [
            'width' => 290,
            'height' => 290,
            'method' => 'resize_and_crop'
        ]
    ]
];