<?php

return [
    'path' => '/media/main_page_slideshow',
    'variants' => [
        '1872x450' => [
            'width' => 1872,
            'height' => 300,
            'method' => 'resize_and_crop'
        ],
        '1872x300' => [
            'width' => 1872,
            'height' => 300,
            'method' => 'resize_and_crop'
        ],
        '300x75' => [
            'width' => 300,
            'height' => 75,
            'method' => 'resize_and_crop'
        ]
    ]
];