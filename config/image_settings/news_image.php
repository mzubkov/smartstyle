<?php

return [
    'path' => '/media/catalog',
    'variants' => [
        '1920x1280' => [
            'width' => 1920,
            'height' => 1280,
            'method' => 'resize_and_crop'
        ],
        '610x408' => [
            'width' => 610,
            'height' => 408,
            'method' => 'resize_and_crop'
        ]
    ]
];