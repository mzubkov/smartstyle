<?php

return [
    'path' => '/media/catalog',
    'variants' => [
        '610x400' => [
            'width' => 610,
            'height' => 400,
            'method' => 'resize_and_crop'
        ]
    ]
];