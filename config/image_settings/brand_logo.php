<?php

return [
    'path' => '/media/brand/logo',
    'variants' => [
        'logo' => [
            'height' => 55,
            'method' => 'resize'
        ],
    ]
];