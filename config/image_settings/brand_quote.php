<?php

return [
    'path' => '/media/brand/brand_quote',
    'variants' => [
        '1872x450' => [
            'width' => 1872,
            'height' => 450,
            'method' => 'resize_and_crop'
        ],
    ]
];