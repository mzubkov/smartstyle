<?php

return [
    'path' => '/media/partner',
    'variants' => [
        '290x120' => [
            'width' => 290,
            'height' => 120,
            'method' => 'resize_and_crop'
        ]
    ]
];