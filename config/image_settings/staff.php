<?php

return [
    'path' => '/media/staff',
    'variants' => [
        '290x300' => [
            'width' => 290,
            'height' => 300,
            'method' => 'resize_and_crop'
        ]
    ]
];