<?php

namespace app\widgets;

use app\models\db\WorkItem;
use yii\base\Widget;

class WorkItems extends Widget
{
    /** @var WorkItem[] */
    private $items;

    /**
     * @throws \yii\base\InvalidConfigException
     */
    public function init()
    {
        parent::init();
        $this->items = WorkItem::find()
            ->forSite()
            ->all();
    }

    public function run()
    {
        return $this->render('work-items', [
            'items' => $this->items
        ]);
    }
}