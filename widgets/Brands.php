<?php

namespace app\widgets;

use app\models\db\Brand;
use yii\base\Widget;

class Brands extends Widget
{
    /** @var Brand[] */
    private $items;

    /**
     * @throws \yii\base\InvalidConfigException
     */
    public function init()
    {
        parent::init();
        $this->items = Brand::find()
            ->forSite()
            ->all();
    }

    public function run()
    {
        return $this->render('brands', [
            'brands' => $this->items
        ]);
    }
}