<?php

namespace app\widgets;

use app\models\db\SellItem;
use yii\base\Widget;

class SellItems extends Widget
{
    /** @var SellItem[] */
    private $items;

    /**
     * @throws \yii\base\InvalidConfigException
     */
    public function init()
    {
        parent::init();
        $this->items = SellItem::find()
            ->forSite()
            ->all();
    }

    public function run()
    {
        return $this->render('sell-items', [
            'items' => $this->items
        ]);
    }
}