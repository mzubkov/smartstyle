<?php

namespace app\widgets;

use yii\base\Widget;

/**
 * Class CallbackForm
 * @package app\modules\site\widgets
 */
class CallbackForm extends Widget
{
    /**
     * @return string
     */
    public function run()
    {
        return $this->render('callback-form');
    }
}