<?php

namespace app\widgets;

use app\models\db\CarBrand;
use yii\base\Widget;

/**
 * Class TechMaps
 * @package app\modules\site\widgets
 */
class TechMaps extends Widget
{
    /**
     * @return string
     */
    public function run()
    {
        if (\Yii::$app->user->isGuest) {
            return '';
        }

        $brands = CarBrand::find()
            ->forSite()
            ->all();

        if (!count($brands)) {
            return '';
        }

        return $this->render('tech-maps', [
            'brands' => $brands
        ]);
    }
}