<?php

namespace app\widgets;

use yii\base\Widget;

class Slideshow extends Widget
{
    /** @var integer */
    public $position = 0;

    /** @var bool  */
    public $showPresentation = false;

    /** @var string  */
    public $blockClass;

    /** @var string  */
    public $sliderClass;

    /** @var string  */
    public $elementClass;

    /**
     * @var
     */
    private $items;

    /**
     * @throws \yii\base\InvalidConfigException
     */
    public function init()
    {
        parent::init();
        $this->items = \app\models\db\Slideshow::find()
            ->where(['position' => $this->position])
            ->forSite()
            ->all();
    }

    /**
     * @return string
     */
    public function run()
    {
        if (!$this->items) {
            return '';
        }

        return $this->render('slideshow', [
            'slides' => $this->items,
            'blockClass' => $this->blockClass,
            'sliderClass' => $this->sliderClass,
            'elementClass' => $this->elementClass,
            'showPresentation' => $this->showPresentation
        ]);
    }

}