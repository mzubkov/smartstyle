<?php

namespace app\widgets;

use app\models\db\InstructionCategory;
use yii\base\Widget;
use yii\db\ActiveQuery;

/**
 * Class Documentation
 * @package app\modules\site\widgets
 */
class Documentation extends Widget
{
    /**
     * @return string
     */
    public function run()
    {
        if (\Yii::$app->user->isGuest) {
            return '';
        }

        $categories = InstructionCategory::find()
            ->rootCategories()
            ->forSite()
            ->with([
                'categories' => function($query){
                    /** @var $query ActiveQuery */
                    $query->andWhere(['is_published' => true]);
                    $query->with([
                        'items' => function($itemsQuery){
                            /** @var $itemsQuery ActiveQuery */
                            $itemsQuery->andWhere(['is_published' => true]);
                            $itemsQuery->with([
                                'files' => function($filesQuery){
                                    /** @var $filesQuery ActiveQuery */
                                    $filesQuery->andWhere(['is_published' => true]);
                                }
                            ]);
                        }
                    ]);
                }
            ])
            ->all();

        return $this->render('documentation', [
            'categories' => $categories
        ]);
    }
}