<?php

namespace app\widgets;

use yii\base\Widget;

/**
 * Class Map
 * @package app\modules\site\widgets
 */
class Map extends Widget
{
    /**
     * @return string
     */
    public function run()
    {
        return $this->render('map');
    }
}