<?php

namespace app\widgets;

use app\models\db\Partner;
use yii\base\Widget;

class Partners extends Widget
{
    /** @var Partner[] */
    private $items;

    /**
     * @throws \yii\base\InvalidConfigException
     */
    public function init()
    {
        parent::init();
        $this->items = Partner::find()
            ->forSite()
            ->all();
    }

    public function run()
    {
        return $this->render('partners', [
            'partners' => $this->items
        ]);
    }
}