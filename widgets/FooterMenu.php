<?php

namespace app\widgets;

use app\models\db\FooterMenuFolder;
use yii\base\Widget;

/**
 * Class Navigation
 * @package app\modules\site\widgets
 */
class FooterMenu extends Widget
{
    /**
     * @var
     */
    private $items;

    /**
     * @throws \yii\base\InvalidConfigException
     */
    public function init()
    {
        parent::init();
        $this->items = FooterMenuFolder::find()
            ->forSite()
            ->all();
    }

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('footer-menu', [
            'folders' => $this->items
        ]);
    }
}