<?php

namespace app\widgets;

use app\models\db\Department;
use yii\base\Widget;

class Team extends Widget
{
    /** @var Department[] */
    private $items;

    /**
     * @throws \yii\base\InvalidConfigException
     */
    public function init()
    {
        parent::init();
        $this->items = Department::find()
            ->forSite()
            ->all();
    }

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('team', [
            'departments' => $this->items
        ]);
    }
}