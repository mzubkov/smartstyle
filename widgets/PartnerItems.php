<?php

namespace app\widgets;

use app\models\db\PartnerItem;
use yii\base\Widget;

class PartnerItems extends Widget
{
    /** @var PartnerItem[] */
    private $items;

    /**
     * @throws \yii\base\InvalidConfigException
     */
    public function init()
    {
        parent::init();
        $this->items = PartnerItem::find()
            ->forSite()
            ->all();
    }

    public function run()
    {
        return $this->render('partner-items', [
            'items' => $this->items
        ]);
    }
}