<?php

use app\models\db\WorkItem;

/** @var $items WorkItem[] */

?>
<div class="working">
    <h2>С кем мы работаем?</h2>
    <div class="working-category-wr">
        <div class="working-category slider-wrapper">
            <ul class="slider-wrapper-wr">
                <?php foreach ($items as $item) {?>
                    <li class="slider_slide"><span class="working-category-item"><?=$item->name?></span></li>
                <?php }?>
            </ul>
        </div>
        <div class="swiper-pagination pag"></div>
    </div>
    <div class="working-note">* Если Вы являетесь физическим лицом, вам <a href="/maps" title="">сюда</a></div>
</div>