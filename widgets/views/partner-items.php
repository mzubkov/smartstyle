<?php

use app\models\db\PartnerItem;

/** @var $items PartnerItem[] */
?>
<div class="partnership-step-wrap">
    <div class="partnership-step">
        <h2><?=Yii::$container->get('textBlockFinder')->get('partner_items_head');?></h2>
        <div class="partnership-step-tx"><?=Yii::$container->get('textBlockFinder')->get('partner_items_text');?></div>
        <div class="partnership-step-cont-wr">
            <div class="partnership-step-cont">
                <ul class="partnership-step-list">
                    <?php foreach ($items as $item) {?>
                    <li class="partnership-step-item">
                        <div class="partnership-step-item-mark" style="background-image: url(<?=$item->getImage('100x100')?>);"></div>
                        <div class="partnership-step-item-tx"><?=$item->text?></div>
                    </li>
                    <?php }?>
                </ul>
            </div>
            <div class="partnership-step-pagination"></div>
        </div>
    </div>
</div>