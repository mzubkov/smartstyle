<?php
use app\models\db\Department;
use app\widgets\SupportForm;

/** @var $departments Department[] */
?>
<div class="team" data-unifScrollFrom="#team">
    <h2><?=Yii::$container->get('textBlockFinder')->get('team_head')?></h2>
    <div class="team-tx">
        <?=Yii::$container->get('textBlockFinder')->get('team_text')?>
    </div>
    <div class="team-wrappergroup">
        <?php foreach ($departments as $department) {?>
            <div class="team-group">
                <div class="team-group-list">
                    <?php foreach ($department->staff as $staff) {?>
                        <div class="team-group-list__el">
                            <div class="team-group-list__el-photo" style="background-image: url(<?=$staff->getImage('290x300')?>);"></div>
                            <div class="team-group-list__el-name"><?=$staff->name?></div>
                            <div class="team-group-list__el-info">
                                <span><?=$staff->post?></span> <?=$staff->phone?>
                            </div>
                        </div>
                    <?php }?>
                </div>
                <div class="team-group-name"><?=$department->name?></div>
                <div class="team-group-pagination"></div>
            </div>
        <?php }?>
    </div>
    <div class="team-group_support">
        <?=SupportForm::widget()?>
        <div class="team-group-name">Технический отдел</div>
    </div>
</div>