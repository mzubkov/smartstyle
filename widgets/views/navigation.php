<?php
use app\models\db\Navigation;

/** @var $items Navigation[] */
?>
<nav class="head-nav">
    <ul class="head-nav-list">
        <?php foreach ($items as $item) {?>
            <li><a href="javascript:void(0)" data-unifScrollTo="<?=$item->url?>" title="<?=$item->name?>"><?=$item->name?></a></li>
        <?php }?>
    </ul>
    <div class="head-nav-btn"></div>
</nav>