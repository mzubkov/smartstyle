<?php
/** @var $videos BrandVideo[] */

use app\models\db\BrandVideo;

?>
<div class="video-wrap">
    <div class="video">
        <div class="video-list">
            <?php foreach ($videos as $video) {?>
                <div class="video-list__elem-img_v">
                    <div class="video-list__elem-img">
                        <?=$video->getVideoPreview()?>
                    </div>
                    <div class="video-list__elem-title"><?=$video->name?></div>
                </div>
            <?php } ?>
        </div>
    </div>
</div>