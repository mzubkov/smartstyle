<?php
use app\models\db\SellItem;

/** @var $items SellItem[] */
?>
<div class="market-wrap">
    <div class="market">
        <h2><?=Yii::$container->get('textBlockFinder')->get('sell_head')?></h2>
        <div class="market-tx"><?=Yii::$container->get('textBlockFinder')->get('sell_text')?></div>
        <div class="market-cont-wr">
            <div class="market-cont">
                <div class="market-cont-list">
                    <?php foreach ($items as $item) {?>
                    <div class="market-cont-item">
                        <div class="market-cont-item-mark" style="background-image: url(<?=$item->getImage('100x100')?>);"></div>
                        <div class="market-cont-item-tx"><?=$item->text?></div>
                    </div>
                    <?php }?>
                </div>
            </div>
            <div class="market-pagination"></div>
        </div>
    </div>
</div>