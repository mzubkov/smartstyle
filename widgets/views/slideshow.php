<?php

use app\models\db\Slideshow;

/** @var $slides Slideshow[] */
/** @var $blockClass string */
/** @var $sliderClass string */
/** @var $elementClass string */
/** @var $showPresentation boolean */

?>

<div class="<?=$blockClass?>">
    <div class="<?=$sliderClass?>">
        <?php foreach ($slides as $slide){?>
            <div class="<?=$elementClass?>" style="background-image: url(<?=$slide->getImage('1872x450')?>);"></div>
        <?php }?>
    </div>

    <?php if ($showPresentation) {?>
        <a href="#" title="Презентация Смарт-Стиль" class="presentation-btn">Презентация Смарт-Стиль</a>
    <?php }?>
</div>