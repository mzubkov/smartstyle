<?php
use app\models\TextBlockFinder;
use yii\web\View;

/** @var $tbFinder TextBlockFinder */
$tbFinder = Yii::$container->get('textBlockFinder');

$js = <<<JS
    $('form[name="callback_form"]').submit(function(){
        var _form = $(this);
        $('input:not([type=submit])', _form).removeClass('error');
        $('.head-tel-drop-message').removeClass('error').removeClass('success').html('');
        $.post(_form.attr('action'), _form.serialize(), function(data){
            $('.head-tel-drop-message').addClass('error').html('<p>' + data.message + '</p>');
            if (data.status == 'ok') {
                $('.head-tel-drop-message').addClass('success');
                $('input:not([type=submit])', _form).val('');
            }
            
            if (data.status == 'error') {
                $('.head-tel-drop-message').addClass('error');
                for (var i = 0; i < data.fields.length; i++) {
                    $('[name="callback[' + data.fields[i] + ']"]', _form).addClass('error');
                }
            }
        }, 'json');
        return false;
    });
JS;
$this->registerJs($js, View::POS_READY);
?>
<div class="head-box">
    <div class="head-tel">
        <div class="head-tel-number"><?=$tbFinder->get('header_phone')?></div>
        <div class="head-tel-tx desktop-only"><a href="javascript:void(0);" title="Позвоните мне">Позвоните мне</a>
            <div class="head-tel-drop">
                <div class="head-tel-drop-message"></div>
                <form action="/callback" name="callback_form">
                    <input type="hidden" name="<?=Yii::$app->request->csrfParam; ?>" value="<?=Yii::$app->request->getCsrfToken(); ?>" />
                    <div class="head-tel-drop-input"><input type="tel" name="callback[phone]" placeholder="Телефон"/></div>
                    <div class="head-tel-drop-input"><input type="text" name="callback[name]" placeholder="Ваше имя"/></div>
                    <div class="head-tel-drop-btn"><input type="submit" value="Позвоните мне"/></div>
                </form>
                <div class="head-tel-drop-close"></div>
            </div>
        </div>
    </div>
    <span class="head-lk"><span class="desktop-only">Личный кабинет</span> <span class="mobile-only">ЛК</span></span>
</div>