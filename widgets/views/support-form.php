<?php

use app\models\TextBlockFinder;
use yii\web\View;

/** @var $tbFinder TextBlockFinder */
$tbFinder = Yii::$container->get('textBlockFinder');

$js = <<<JS
    $('form[name="support_form"]').submit(function(){
        var _form = $(this);
        $('input:not([type=submit]), textarea', _form).removeClass('error');
        $('.team-group_support-form-message').removeClass('error').removeClass('success').html('');
        $.post(_form.attr('action'), _form.serialize(), function(data){
            $('.team-group_support-form-message').addClass('error').html('<p>' + data.message + '</p>');
            if (data.status == 'ok') {
                $('.team-group_support-form-message').addClass('success');
                $('input:not([type=submit]), textarea', _form).val('');
            }
            
            if (data.status == 'error') {
                $('.team-group_support-form-message').addClass('error');
                for (var i = 0; i < data.fields.length; i++) {
                    $('[name="support[' + data.fields[i] + ']"]', _form).addClass('error');
                }
            }
        }, 'json');
        return false;
    });
JS;
$this->registerJs($js, View::POS_READY);
?>
<div class="team-group_support-columns" data-unifScrollFrom="support">
    <div class="team-group_support-columns__item">
        <h2><?=$tbFinder->get('tech_head1')?> <br>
            <?=$tbFinder->get('tech_head2')?></h2>
        <div class="team-group_support-tx">
            <?=$tbFinder->get('tech_text')?>
        </div>
        <div class="team-group_support-note">
            <p>Есть технические вопросы? Воспользуйтесь формой справа.</p>
        </div>
    </div>
    <div class="team-group_support-columns__item">
        <div class="team-group_support-form">
            <div class="team-group_support-form-message"></div>
            <form action="/support" name="support_form">
                <input type="hidden" name="<?=Yii::$app->request->csrfParam; ?>" value="<?=Yii::$app->request->getCsrfToken(); ?>" />
                <div class="team-group_support-form-text">
                    <textarea name="support[text]" id="" cols="30" rows="10" placeholder="Кратко изложите суть проблемы"></textarea>
                </div>
                <div class="team-group_support-form-row">
                    <div class="team-group_support-form-input"><input type="text" name="support[contact]" placeholder="Ваше имя и номер телефона"/></div>
                    <div class="team-group_support-form-btn"><input type="submit" value="Отправить"/></div>
                </div>
            </form>
        </div>
    </div>
</div>