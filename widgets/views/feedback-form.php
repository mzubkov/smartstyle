<?php

use app\models\TextBlockFinder;
use yii\web\View;

/** @var $this View */
/** @var TextBlockFinder $tbFinder */
$tbFinder = Yii::$container->get('textBlockFinder');
$options1 = explode("\n", $tbFinder->get('feedback_form_options1'));
$options2 = explode("\n", $tbFinder->get('feedback_form_options2'));

$js = <<<JS
    $('form[name="feedback_form"]').submit(function(){
        var _form = $(this);
        $('input:not([type=submit]), textarea, select', _form).removeClass('error');
        $('.questionary-message').removeClass('error').removeClass('success').html('');
        $.post(_form.attr('action'), _form.serialize(), function(data){
            $('.questionary-message').addClass('error').html('<p>' + data.message + '</p>');
            if (data.status == 'ok') {
                $('.questionary-message').addClass('success');
                $('input:not([type=submit]), textarea, select', _form).val('');
            }
            
            if (data.status == 'error') {
                $('.questionary-message').addClass('error');
                for (var i = 0; i < data.fields.length; i++) {
                    $('[name="feedback[' + data.fields[i] + ']"]', _form).addClass('error');
                }
            }
        }, 'json');
        return false;
    });
JS;
$this->registerJs($js, View::POS_READY);
?>
<div class="questionary">
    <h2><?=$tbFinder->get('feedback_head')?></h2>
    <div class="questionary-txt"><?=$tbFinder->get('feedback_text')?></div>
    <div class="questionary-message"></div>
    <form action="/feedback" name="feedback_form">
        <input type="hidden" name="<?=Yii::$app->request->csrfParam; ?>" value="<?=Yii::$app->request->getCsrfToken(); ?>" />
        <div class="questionary-form">
            <div class="questionary-form-container">
                <div class="questionary-form-column">
                    <div class="questionary-form-line">
                        <input type="text" name="feedback[company_name]" placeholder="Ваше имя / Название компании"/>
                    </div>
                    <?php if (!empty($options1)) {?>
                    <div class="questionary-form-line questionary-form-line_var">
                        <div class="questionary-form-line-element">
                            <select name="feedback[activity]">
                                <?php foreach ($options1 as $option) { ?>
                                    <option value="<?=$option?>"><?=$option?></option>
                                <?php }?>
                            </select>
                        </div>
                        <div class="questionary-form-line-person"><a href="#" title="Частное лицо">Частное лицо</a></div>
                    </div>
                    <?php }?>
                    <?php if (!empty($options2)) {?>
                    <div class="questionary-form-line">
                        <select name="feedback[buy_volume]">
                            <?php foreach ($options2 as $option) { ?>
                                <option value="<?=$option?>"><?=$option?></option>
                            <?php }?>
                        </select>
                    </div>
                    <?php }?>
                    <div class="questionary-form-line">
                        <input type="text" name="feedback[address]" placeholder="Адрес"/>
                    </div>
                    <div class="questionary-form-line">
                        <input type="url" name="feedback[url]" placeholder="Веб-сайт"/>
                    </div>
                </div>
                <div class="questionary-form-column">
                    <div class="questionary-form-line">
                        <input type="text" name="feedback[name]" placeholder="Контактное лицо (имя)"/>
                    </div>
                    <div class="questionary-form-line">
                        <input type="tel" name="feedback[phone]" placeholder="Номер телефона"/>
                    </div>
                    <div class="questionary-form-line">
                        <input type="email" name="feedback[email]" placeholder="E-mail"/>
                    </div>
                    <div class="questionary-form-line">
                        <textarea cols="30" rows="10" name="feedback[info]" placeholder="Дополнительная информация"></textarea>
                    </div>
                </div>
            </div>
            <div class="questionary-form-btn"><input type="submit" value="Стать партнером Смарт-Стиль"/></div>
        </div>
    </form>
</div>