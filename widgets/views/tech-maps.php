<?php

use app\models\db\CarBrand;

/** @var $brands CarBrand[] */
?>
<div class="technmaps-brands active" data-unifScrollFrom="tech-maps">
    <div class="technmaps-brands-container">
        <h2>Выберите марку автомобиля:</h2>
        <div class="technmaps-brands-choice">
            <div class="technmaps-brands-choice-list">
                <?php foreach ($brands as $brand) {?>
                    <div data-brandId="<?=$brand->id?>" class="technmaps-brands-choice-list__element">
                        <div class="technmaps-brands-choice-list__element-title">
                            <div class="technmaps-brands-choice-list__element-title-logo">
                                <img src="<?=$brand->getImage('290x70')?>" alt="<?=$brand->name?>">
                            </div>
                            <div class="technmaps-brands-choice-list__element-title-name"><?=$brand->name?></div>
                        </div>
                        <div class="technmaps-brands-choice-list__element-cont"></div>
                    </div>
                <?php }?>
            </div>
        </div>
        <?php foreach ($brands as $brand) {?>
            <div data-brandModelId="<?=$brand->id?>" class="technmaps-brands-wrapper">
                <div class="technmaps-brands-wrapper-cont">
                    <div class="technmaps-brands-models">
                        <h2>Выберите модель автомобиля <span><?=$brand->name?></span>:</h2>
                        <div class="technmaps-brands-models-list">
                            <?php foreach ($brand->publishedModels as $model) {?>
                                <div data-modelSettingsCont="<?=$model->id?>" class="technmaps-brands-models-list__model">
                                    <div class="technmaps-brands-models-list__model-title">
                                        <div class="technmaps-brands-models-list__model-title-img">
                                            <img src="<?=$model->getImage('290x165')?>" alt="<?=$model->name?>">
                                        </div>
                                        <div class="technmaps-brands-models-list__model-title-name"><?=$model->name?></div>
                                    </div>
                                    <div class="technmaps-brands-models-list__model-cont">
                                        <div class="technmaps-brands-models-list__model-settings">
                                            <h2>Карты установок:</h2>
                                            <div class="technmaps-brands-models-list__model-settings-cards">
                                                <?php foreach ($model->publishedMaps as $map) {?>
                                                <div class="technmaps-brands-models-list__model-settings-cards__card">
                                                    <a href="<?=$map->getFile()?>" title="<?=$map->name?>"><?=$map->name?></a>
                                                </div>
                                                <?php }?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div data-modelSettings="<?=$model->id?>" class="technmaps-brands-models-list-box"></div>
                            <?php }?>
                        </div>
                    </div>
                    <div class="technmaps-brands-settings"></div>
                </div>
            </div>
        <?php }?>
    </div>
</div>