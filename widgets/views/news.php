<?php

/** @var $news News[] */

use app\models\db\News;


?>
<div class="news">
    <h2>Новости и события</h2>
    <div class="news-cont">
        <div class="news-list">
            <?php foreach ($news as $item) {?>
            <div class="news-list__item">
                <div class="news-list__item-date"><?=$item->date?></div>
                <div class="news-list__item-title">
                    <a href="<?=$item->url?>" title="<?=$item->name?>"><?=$item->name?></a>
                </div>
                <div class="news-list__item-brand"><?=(($item->id_brand) ? $item->brand->name : '')?></div>
            </div>
            <?php }?>
        </div>
        <div class="news-btn"><a href="/news" title="Все новости">Все новости</a></div>
    </div>
</div>