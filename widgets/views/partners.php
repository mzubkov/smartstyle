<?php

use app\models\db\Partner;

/** @var $partners Partner[] */

?>
<div class="partnership-clients">
    <h2><?=Yii::$container->get('textBlockFinder')->get('partner_head')?></h2>
    <div class="partnership-clients-tx">
        <?=Yii::$container->get('textBlockFinder')->get('partner_text')?>
    </div>
    <div class="partnership-clients-cont-wr">
        <div class="partnership-clients-cont">
            <ul class="partnership-clients-list">
                <?php foreach ($partners as $partner) {?>
                    <li class="partnership-clients-item"><img src="<?=$partner->getImage('290x120')?>" alt="<?=$partner->name; ?>"></li>
                <?php }?>
            </ul>
        </div>
        <div class="partnership-clients-pagination"></div>
    </div>
</div>