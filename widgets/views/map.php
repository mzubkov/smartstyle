<?php
/** @var TextBlockFinder $finder */

use app\models\TextBlockFinder;

$finder = Yii::$container->get('textBlockFinder');
$address_items = $finder->get('footer_address');
$address_items = explode("\n", $address_items);
?>
<div class="map" data-unifScrollFrom="#contacts">
    <div id="map_contacts" class="map" style="height: 300px;" data-map_zoom="16" data-mapContent='Смартстиль' data-map_position_lat="55.634149" data-map_position_long="37.443075"></div>
</div>
<div class="address">
    <div class="address-cont">
        <div class="address-column">
            <div class="address-main">
                <?php foreach ($address_items as $address) { ?>
                    <span class="address-main-el"><?=$address?></span>
                <?php }?>
            </div>
        </div>
        <div class="address-column">
            <div class="address-department">
                <h4><?=$finder->get('footer_sell_head')?></h4>
                <div class="address-department-info"><?=$finder->get('footer_sell_text')?></div>
            </div>
            <div class="address-department">
                <h4>Установочный Центр:</h4>
                <div class="address-department-info"><?=$finder->get('footer_sell_text')?></div>
            </div>
        </div>
    </div>
</div>
