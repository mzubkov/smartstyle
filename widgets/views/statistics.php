<?php
/** @var $items MapItem[] */

use app\models\db\MapItem;

?>
<div class="statistics">
    <div class="statistics-list">
        <?php foreach ($items as $item) {?>
        <div class="statistics-list__item">
            <div class="statistics-list__item-number"><span><?=$item->number?></span></div>
            <div class="statistics-list__item-tx"><?=Yii::$app->formatter->asNtext($item->text)?></div>
        </div>
        <?php } ?>
    </div>
    <div class="statistics-pagination"></div>
</div>