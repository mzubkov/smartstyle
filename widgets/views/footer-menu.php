<?php
/** @var $folders FooterMenuFolder[] */

use app\models\db\FooterMenuFolder;

?>
<?php foreach ($folders as $folder) {?>
<div class="foot-column">
    <h4><span class="foot-column-casing"><?=$folder->name?></span></h4>
    <div class="foot-column-casing">
        <ul class="foot-menu">
            <?php foreach ($folder->publishedItems as $item) {?>
                <li><a href="<?=$item->url?>" title="<?=$item->name?>"><?=$item->name?></a></li>
            <?php }?>
        </ul>
    </div>
</div>
<?php }?>