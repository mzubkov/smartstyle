<?php
/** @var $categories InstructionCategory[]*/

use app\models\db\InstructionCategory;

?>

<div class="documentation" data-unifScrollFrom="instructions">
    <div class="documentation-container">
        <h2>Инструкции и документация</h2>
        <div class="documentation-list">
            <div class="documentation-list__item">
                <?php foreach ($categories as $category){?>
                <div class="documentation-list__item-head"><?=$category->name?></div>
                <div class="documentation-list__item-serieslist">
                    <?php foreach ($category->categories as $subCategory){?>
                    <div class="documentation-list__item-serieslist__el">
                        <div class="documentation-list__item-serieslist__el-title"><?=$subCategory->name?></div>
                        <div class="documentation-list__item-serieslist__el-modellist">
                            <?php foreach ($subCategory->items as $item){?>
                            <div class="documentation-list__item-serieslist__el-modellist__elem">
                                <div class="documentation-list__item-serieslist__el-modellist__elem-name"><?=$item->name?></div>
                                <div class="documentation-list__item-serieslist__el-modellist__elem-doclist">
                                    <?php foreach ($item->files as $file){?>
                                    <div class="documentation-list__item-serieslist__el-modellist__elem-doclist__element">
                                        <a href="<?=$file->getFile()?>" title="<?=$file->name?>" target="_blank"><?=$file->name?></a></div>
                                    <?php }?>
                                </div>
                            </div>
                            <?php }?>
                        </div>
                    </div>
                    <?php }?>
                </div>
            </div>
            <?php }?>
        </div>
    </div>
</div>