<?php

use app\models\db\Brand;

/** @var $brands Brand[] */

?>
<div class="tile-wr show_button_up" data-unifScrollFrom="#catalog">
    <div class="tile_vary">
        <div class="tile_vary-list">
            <?php foreach ($brands as $brand) {?>
                <div class="tile_vary-list__elem">
                    <a href="<?=$brand->getUrl()?>" class="tile_vary-list__elem-box" style="background-image: url('<?=$brand->getImage('610x400')?>');">
                        <span class="tile_vary-list__elem-info">
                            <span class="tile_vary-list__elem-info-logo">
                                <img src="<?=$brand->getLogo()?>" alt="<?=$brand->name?>">
                            </span>
                            <span class="tile_vary-list__elem-info-name"><?=$brand->description?></span>
                        </span>
                    </a>
                </div>
            <?php }?>
        </div>
        <div class="tile_vary-pagination"></div>
    </div>
</div>