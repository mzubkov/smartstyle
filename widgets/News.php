<?php

namespace app\widgets;

use yii\base\Widget;

/**
 * Class News
 * @package app\modules\site\widgets
 */
class News extends Widget
{
    /**
     * @return string
     */
    public function run()
    {
        $news = \app\models\db\News::find()->forSite()->last()->all();

        if (!count($news)) {
            return '';
        }

        return $this->render('news', [
            'news' => $news
        ]);
    }
}