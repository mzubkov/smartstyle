<?php

namespace app\widgets;

use app\models\db\MapItem;
use yii\base\Widget;

class Statistics extends Widget
{
    /** @var MapItem[] */
    private $items;

    /**
     * @throws \yii\base\InvalidConfigException
     */
    public function init()
    {
        parent::init();
        $this->items = MapItem::find()
            ->forSite()
            ->all();
    }

    public function run()
    {
        if (!$this->items) {
            return '';
        }

        return $this->render('statistics', [
            'items' => $this->items
        ]);
    }
}