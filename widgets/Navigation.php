<?php

namespace app\widgets;

use yii\base\Widget;

/**
 * Class Navigation
 * @package app\modules\site\widgets
 */
class Navigation extends Widget
{
    /**
     * @var
     */
    private $items;

    /**
     * @throws \yii\base\InvalidConfigException
     */
    public function init()
    {
        parent::init();
        $this->items = \app\models\db\Navigation::find()
            ->forSite()
            ->all();
    }

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('navigation', [
            'items' => $this->items
        ]);
    }
}