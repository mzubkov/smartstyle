<?php

namespace app\widgets;

use yii\base\Widget;

/**
 * Class FeedbackForm
 * @package app\modules\site\widgets
 */
class FeedbackForm extends Widget
{
    /**
     * @return string
     */
    public function run()
    {
        return $this->render('feedback-form');
    }
}