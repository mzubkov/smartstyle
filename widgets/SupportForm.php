<?php

namespace app\widgets;

use yii\base\Widget;

/**
 * Class TeachForm
 * @package app\modules\site\widgets
 */
class SupportForm extends Widget
{
    /**
     * @return string
     */
    public function run()
    {
        return $this->render('support-form');
    }
}