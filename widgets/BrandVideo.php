<?php

namespace app\widgets;

use app\models\db\Brand;
use yii\base\Widget;

class BrandVideo extends Widget
{
    /** @var Brand[] */
    private $items;

    /**
     * @throws \yii\base\InvalidConfigException
     */
    public function init()
    {
        parent::init();
        $this->items = \app\models\db\BrandVideo::find()
            ->forSite()
            ->all();
    }

    /**
     * @return string
     */
    public function run()
    {
        if (!$this->items) {
            return '';
        }

        return $this->render('brand-video', [
            'videos' => $this->items
        ]);
    }
}