<?php

use app\models\db\News;
use app\models\db\NewsImage;
use app\widgets\Map;

/** @var $news News */
/** @var $images NewsImage[]|array */

$site_title = Yii::$container->get('sysParamFinder')->get('site_title');
?>
<div class="page">
    <div class="page-cont show_button_up">
        <div class="breadcrumbs">
            <span><a href="/" title="<?=$site_title?>"><?=$site_title?></a></span>
            <span><a href="/news" title="Новости">Новости</a></span>
            <span><?=$news->name?></span>
        </div>
        <h1><?=$news->name?></h1>
        <div class="titlenews"><?=$news->annotation?></div>
        <div class="infonews"><?=$news->news_date?> <?php if ($news->id_brand) {?>/ <span><?=$news->brand->name?></span><?php }?></div>
        <div class="contentnews">
            <?=$news->content?>
            <?php if (count($images)) {?>
                <div class="contentnews-images">
                    <?php $c=1; foreach ($images as $image) {?>
                        <img src="<?=$image->getImage('610x408')?>" alt="<?=$image->name?>" class="<?=(($c%2) ? 'alignleft' : 'alignright')?>">
                    <?php $c++;}?>
                </div>
            <?php }?>
        </div>
    </div>
    <?=Map::widget()?>
</div>