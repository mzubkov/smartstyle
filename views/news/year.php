<?php

use app\models\db\News;
use app\widgets\Map;

/** @var $currentYear string */
/** @var $years array */
/** @var $news News[] */

?>
<div class="page">
    <div class="page-cont">
        <div class="page-txt">
            <?=Yii::$container->get('textBlockFinder')->get('news_text');?>
        </div>
        <div class="newsandevents show_button_up">
            <div class="newsandevents-head">
                <div class="newsandevents-head-selected"><?=$currentYear?></div>
                <div class="newsandevents-head-list">
                    <?php foreach ($years as $year) {?>
                        <a href="/news/<?=$year?>" title="<?=$year?>"
                           class="newsandevents-head-list__item <?php if ($year == $currentYear) {?>active<?php }?>">
                            <?=$year?></a>
                    <?php }?>
                </div>
            </div>
            <div class="newsandevents-content">
                <div class="newsandevents-content-list">
                    <?php foreach ($news as $item) {?>
                        <div class="newsandevents-content-list__item">
                            <div class="newsandevents-content-list__item-date"><?=$item->date?></div>
                            <div class="newsandevents-content-list__item-title">
                                <a href="<?=$item->url?>" title="<?=$item->name?>"><?=$item->name?></a>
                            </div>
                            <div class="newsandevents-content-list__item-brand"><?=(($item->id_brand) ? $item->brand->name : '')?></div>
                        </div>
                    <?php }?>
                </div>
            </div>
        </div>
    </div>
    <?=Map::widget()?>
</div>