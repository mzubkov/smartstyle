<?php

use app\widgets\Map;
?>
<div class="page">
    <div class="page-cont show_button_up">
        <h1><?=$name;?></h1>
        <div class="contentnews">
            <?=$message?>
        </div>
    </div>
    <?=Map::widget()?>
</div>