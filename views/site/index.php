<?php

/* @var $this yii\web\View */
/** @var $textBlockFinder TextBlockFinder */
$textBlockFinder = Yii::$container->get('textBlockFinder');
$this->title = 'Main page';

use app\models\TextBlockFinder;
use app\widgets\Brands;
use app\widgets\FeedbackForm;
use app\widgets\Map;
use app\widgets\News;
use app\widgets\PartnerItems;
use app\widgets\Partners;
use app\widgets\SellItems;
use app\widgets\Slideshow;
use app\widgets\Team;
use app\widgets\WorkItems;

?>

<div class="page-cont" data-unifScrollFrom="#about">
    <div class="page-txt">
        <h1><?=$textBlockFinder->get('main_page_head')?></h1>
        <?=$textBlockFinder->get('main_page_text')?>
    </div>
    <?=WorkItems::widget()?>
    <?=Brands::widget()?>
</div>

<div class="partnership" data-unifScrollFrom="#partnership">
    <?=PartnerItems::widget()?>
    <?=Partners::widget()?>
</div>

<?=Slideshow::widget([
    'position' => \app\models\db\Slideshow::POSITION_CENTER,
    'showPresentation' => true,
    'blockClass' => 'space',
    'sliderClass' => 'space-slider',
    'elementClass' => 'space-slider-slide'
])?>

<?=SellItems::widget()?>

<?=FeedbackForm::widget()?>

<?=Slideshow::widget([
    'position' => \app\models\db\Slideshow::POSITION_BOTTOM,
    'blockClass' => 'space space_var',
    'sliderClass' => 'space_var-slider',
    'elementClass' => 'space_var-slider-slide'
])?>

<?=Team::widget()?>

<?=News::widget()?>

<?=Map::widget()?>