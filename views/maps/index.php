<?php

use app\widgets\Documentation;
use app\widgets\Map;
use app\widgets\Slideshow;
use app\widgets\Statistics;
use app\widgets\SupportForm;
use app\widgets\TechMaps;
use yii\web\View;

/** @var $this View */
$user = Yii::$app->user;

$js = <<<JS
    $('#login_form').submit(function(){
        var _form = $(this);
        $('.technmaps-head-form-info').html('');
        $.post('/login', _form.serialize(), function(data){
            if (data.status == 'error') {
                $('.technmaps-head-form-info').html('<p>Неверное имя пользователя или пароль</p>');
            }
            
            if (data.status == 'ok') {
                window.location.reload();
            }
        }, 'json');
        return false;
    });
JS;

$this->registerJs($js, View::POS_READY);
?>
<div class="page">
    <div class="page-cont">
        <div class="page-txt">
            <?=Yii::$container->get('textBlockFinder')->get('maps_text');?>
        </div>
        <?=Statistics::widget()?>
    </div>
    <div class="anchors show_button_up" style="background-image: url(/img/tunarov.jpg);">
        <div class="anchors-list">
            <a href="javascript:void(0);" title="Технологические карты" class="anchors-list__item" data-unifScrollTo="tech-maps">Технологические карты</a>
            <a href="javascript:void(0);" title="Техподдержка online" class="anchors-list__item" data-unifScrollTo="support">Техподдержка online</a>
            <a href="javascript:void(0);" title="Инструкции и документация" class="anchors-list__item" data-unifScrollTo="instructions">Инструкции и документация</a>
        </div>
    </div>
    <div class="technmaps">
        <?php if ($user->isGuest) {?>
            <div class="technmaps-head">
                <div class="technmaps-head-cont">
                    <h2>Технологические карты</h2>
                    <div class="technmaps-head-info">Это закрытый раздел. <br>
                        Для доступа к информации введите логин и парол</div>
                    <form action="/login" method="post" id="login_form">
                        <input type="hidden" name="<?=Yii::$app->request->csrfParam; ?>" value="<?=Yii::$app->request->getCsrfToken(); ?>" />
                        <div class="technmaps-head-form technmaps-head-form-info"></div>
                        <div class="technmaps-head-form">
                            <div class="technmaps-head-form-input"><input type="text" name="username" placeholder="Логин"/></div>
                            <div class="technmaps-head-form-input"><input type="password" name="password" placeholder="Пароль"/></div>
                            <input type="submit" style="display: none;">
                        </div>
                    </form>
                    <div class="technmaps-head-tx">Логин и пароль можно получить у персонального менеджера.</div>
                </div>
            </div>
        <?php }?>
        <?=TechMaps::widget()?>
    </div>
    <div class="team team_var">
        <div class="team-group_support">
            <?=SupportForm::widget()?>
        </div>
    </div>
    <?=Slideshow::widget([
        'position' => \app\models\db\Slideshow::POSITION_BOTTOM,
        'blockClass' => 'space space_var',
        'sliderClass' => 'space_var-slider',
        'elementClass' => 'space_var-slider-slide'
    ])?>
    <?=Documentation::widget()?>
    <?=Map::widget()?>
</div>