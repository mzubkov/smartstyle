<?php
use yii\helpers\Html;
?>

<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode(Yii::$container->get('sysParamFinder')->get('site_title')) ?></title>
    <?php $this->head() ?>
</head>