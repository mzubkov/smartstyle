<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\assets\SiteAsset;

SiteAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<?=$this->render('head')?>
<body>
<?php $this->beginBody() ?>

<?=$this->render('header')?>
<div class="page">
    <?= $content ?>
</div>
<?=$this->render('footer')?>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
