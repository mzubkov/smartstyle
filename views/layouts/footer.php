<?php

use app\widgets\FooterMenu;

?>
<footer class="foot">
    <div class="foot-container">
        <div class="foot-box">
            <div class="foot-column foot-column_cop">
                <div class="foot-column-casing">
                    <div class="foot-contact">
                        <h4>Контакты</h4>
                        <div class="foot-contact-tel"><?=Yii::$container->get('textBlockFinder')->get('footer_phone')?></div>
                        <div class="foot-contact-link"><a href="javascript:void(0)" title="Напишите нам">Напишите нам</a></div>
                    </div>
                    <div class="foot-cop">
                        <?=Yii::$container->get('textBlockFinder')->get('copyright')?>
                    </div>
                </div>
            </div>

            <?=FooterMenu::widget()?>
        </div>
    </div>
</footer>

<div class="btn-up" style=""></div>
<div class="popup-bg" style=""></div>
<div class="popup-wrap" style="">
    <div class="popup-container">
        <div class="popup-box">
            <div class="popup" style="">
                <h4>Ваша заявка принята!</h4>
                <div class="popup-txt">
                    <p>Максим, сделай возможность редактирования</p>
                </div>
                <div class="popup-close"></div>
            </div>
            <div class="popup-form" style="">
                <form action="">
                    <div class="head-tel-drop-input"><input class="error" type="tel" placeholder="Телефон"/></div>
                    <div class="head-tel-drop-input"><input type="text" placeholder="Ваше имя"/></div>
                    <div class="head-tel-drop-input"><textarea cols="30" rows="10" placeholder="Текст"></textarea></div>
                    <div class="head-tel-drop-btn"><input type="submit" value="Позвоните мне"/></div>
                </form>
                <div class="head-tel-drop-close"></div>
            </div>
        </div>
    </div>
</div>