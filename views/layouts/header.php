<?php

use app\widgets\CallbackForm;
use app\widgets\Navigation;
use app\widgets\Slideshow;
use yii\web\View;

/** @var $this View */

?>
<header class="head">
    <div class="head-panel">
        <div class="head-cont">
            <a href="/" title="<?=Yii::$container->get('sysParamFinder')->get('site_title')?>" class="head-logo"></a>
            <?=Navigation::widget()?>
            <?=CallbackForm::widget()?>
        </div>
    </div>
    <?php if (isset($this->blocks['head_image'])){ ?>
        <?= $this->blocks['head_image'] ?>
    <?php } else{ ?>
        <?=Slideshow::widget([
            'position' => \app\models\db\Slideshow::POSITION_HEADER,
            'blockClass' => 'head-img',
            'sliderClass' => 'head-img-slider',
            'elementClass' => 'head-img-slider-element'
        ])?>
    <?php } ?>

</header>