<?php

use app\models\db\Brand;
use app\models\db\Catalog;
use app\widgets\BrandVideo;
use app\widgets\FeedbackForm;
use app\widgets\Map;
use yii\web\View;

/** @var $this View */
/** @var $brand Brand */
/** @var $catalogs Catalog[] */

?>


<?php $this->beginBlock('head_image'); ?>
    <div class="head-img_brand" style="background-image: url('<?=$brand->getPageImage()?>')">
        <div class="head-img_brand-logo"><img src="<?=$brand->getLogo()?>" alt="<?=$brand->name?>"></div>
    </div>
<?php $this->endBlock(); ?>

<div class="page">
    <div class="page-cont">
        <div class="page-txt">
            <?=$brand->content?>
        </div>
    </div>
    <?php if (!empty($catalogs)) {?>
        <div class="catalog">
            <div class="page-cont">
                <h4>Каталог продукции <?=$brand->name?>:</h4>
            </div>
            <div class="catalog-head show_button_up">
                <div class="page-cont">
                    <div class="catalog-head-nav">
                        <div class="catalog-head-nav-list">
                            <?php foreach ($catalogs as $catalog) {?>
                                <div class="catalog-head-nav-list__item">
                                    <a href="javascript:void(0)" title="<?=$catalog->name?>" data-unifScrollTo="<?=$catalog->identifier?>">
                                        <span class="catalog-head-nav-list__item-img" style="background-image: url(<?=$catalog->getImage('610x400')?>);"></span>
                                        <span class="catalog-head-nav-list__item-name"><?=$catalog->name?> <span><?=$catalog->products_count?></span></span>
                                    </a>
                                </div>
                            <?php }?>
                        </div>
                    </div>
                </div>
            </div>

            <div class="page-cont">
                <?php foreach ($catalogs as $catalog) {?>
                    <div data-unifScrollFrom="<?=$catalog->identifier?>" class="catalog-category">
                        <div class="catalog-category-container">
                            <div class="catalog-category-list">
                                <?php foreach ($catalog->publishedProducts as $product){?>
                                    <div class="catalog-category-list__element">
                                        <span class="catalog-category-list__element-img"><img src="<?=$product->getImage('290x290')?>" alt="<?=$product->name?>"></span>
                                        <span class="catalog-category-list__element-name"><?=$product->name?></span>
                                    </div>
                                <?php }?>
                            </div>
                        </div>
                        <div class="catalog-category-control">
                            <div class="catalog-category-control__prev"></div>
                            <div class="catalog-category-control__next"></div>
                        </div>
                        <div class="catalog-category-name"><?=$catalog->name?></div>
                        <div class="catalog-category-pagination"></div>
                    </div>
                <?php }?>
            </div>
        </div>
    <?php }?>

    <?php if($brand->publishedAdvantages){?>
        <div class="page-cont">
            <div class="cooperation">
                <h2>Наши партнеры получают:</h2>
                <ul class="cooperation-list">
                    <?php foreach ($brand->publishedAdvantages as $advantage){?>
                        <li><?=$advantage->text?></li>
                    <?php }?>
                </ul>
                <div class="cooperation-btn"><a data-unifScrollTo="toAnketa" href="#" title="#">Начать сотрудничество</a></div>
            </div>
        </div>
    <?php }?>

    <?php if ($brand->publishedQuote) {?>
        <div class="slogan">
            <div class="slogan-img" style="background-image: url('<?=$brand->publishedQuote->getImage('1872x450')?>');"></div>
            <div class="slogan-cont">
                <div class="slogan-cont-txt"><?=$brand->publishedQuote->text?>  <span><?=$brand->name?></span></div>
            </div>
        </div>
    <?php }?>

    <?php if($brand->publishedItems){?>
        <div class="virtues">
            <div class="virtues-cont">
                <div class="virtues-list">
                    <?php foreach ($brand->publishedItems as $item) {?>
                    <div class="virtues-list__item">
                        <div class="virtues-list__item-img"><img src="<?=$item->getImage('100x100')?>" alt="<?=$item->name?>"></div>
                        <h4><?=$item->name?></h4>
                        <p><?=$item->text?></p>
                    </div>
                    <?php }?>
                </div>
                <div class="virtues-pagination"></div>
            </div>
        </div>
    <?php }?>

    <?=BrandVideo::widget()?>
    <?=FeedbackForm::widget()?>
    <?=Map::widget()?>
</div>
