<?php

namespace app\controllers;

use app\models\db\News;
use app\models\db\NewsImage;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class NewsController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $years = News::getYears();

        if (empty($years)) {
            return $this->redirect('/');
        } else {
            return $this->redirect('news/' . $years[0]);
        }

    }

    /**
     * @param $year
     * @return string
     */
    public function actionYear($year)
    {
        $news = News::find()->forSite()->forYear($year)->all();
        return $this->render('year', [
            'currentYear' => $year,
            'years' => News::getYears(),
            'news' => $news
        ]);
    }

    /**
     * @param $year
     * @param $identifier
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionDetail($year, $identifier)
    {
        $news = News::find()->forSite()->forYear($year)->andWhere(['identifier' => $identifier])->one();

        if (!$news) {
            throw new NotFoundHttpException('Новость не найденаы');
        }

        $images = NewsImage::find()->forSite()->forNews($news)->all();
        return $this->render('detail', [
            'news' => $news,
            'images' => $images
        ]);
    }
}