<?php

namespace app\controllers;

use app\models\db\SupportMessage;
use yii\filters\VerbFilter;
use yii\web\Controller;

class SupportController extends Controller
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'index'  => ['post'],
                ],
            ],
        ];
    }

    /**
     * @return \yii\web\Response
     */
    public function actionIndex()
    {
        $message = new SupportMessage();
        $message->load(\Yii::$app->request->post('support'), '');
        if (!$message->save()) {
            $errors = $message->getErrors();
            $result = [
                'status' => 'error',
                'message' => 'Ошибка отправки сообщения',
                'fields' => array_keys($errors),
                'errors' => $errors
            ];
        } else {
            $result = [
                'status' => 'ok',
                'message' => 'Сообщение успешно отправлено'
            ];
        }

        return $this->asJson($result);
    }
}