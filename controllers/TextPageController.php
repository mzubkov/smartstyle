<?php

namespace app\controllers;

use app\models\db\TextPage;
use yii\web\Controller;

class TextPageController extends Controller
{
    public function actionPage($identifier)
    {
        $page = TextPage::findOne(['identifier' => $identifier, 'is_published' => true]);

        return $this->render('page', [
            'page' => $page
        ]);
    }
}