<?php

namespace app\controllers;

use app\models\db\Brand;
use app\models\db\Catalog;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class BrandController extends Controller
{
    /**
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionDetail($id)
    {
        $brand = Brand::find()->forSite()->andWhere(['id' => $id])->one();

        if (!$brand) {
            throw new NotFoundHttpException('Не найден бренд');
        }

        $catalogs = Catalog::find()->forSite()
            ->withPublishedProductsCount($brand->id)
            ->all();

        return $this->render('detail', [
            'brand' => $brand,
            'catalogs' => $catalogs
        ]);
    }
}