<?php

namespace app\controllers;

use app\models\db\CallbackMessage;
use yii\filters\VerbFilter;
use yii\web\Controller;

class CallbackController extends Controller
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'index'  => ['post'],
                ],
            ],
        ];
    }

    /**
     * @return \yii\web\Response
     */
    public function actionIndex()
    {
        $message = new CallbackMessage();
        $message->load(\Yii::$app->request->post('callback'), '');
        if (!$message->save()) {
            $errors = $message->getErrors();
            $result = [
                'status' => 'error',
                'message' => 'Ошибка отправки сообщения',
                'fields' => array_keys($errors),
                'errors' => $errors
            ];
        } else {
            $result = [
                'status' => 'ok',
                'message' => 'Сообщение успешно отправлено'
            ];
        }

        return $this->asJson($result);
    }
}