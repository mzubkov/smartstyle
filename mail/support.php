<?php
use app\models\db\SupportMessage;

/** @var $message SupportMessage */
/** @var $site_title string */
?>
<h1>На сайте "<?=!empty($site_title) ? $site_title : 'не указано'?>" было оставлено сообщение для техподдержки</h1>
<p>Контактная информация: <?=!empty($message->contact) ? $message->contact : 'не указано'?></p>
<p>Сообщение: <?=!empty($message->text) ? $message->text : ''?></p>