<?php
use app\models\db\FeedbackMessage;

/** @var $message FeedbackMessage */
/** @var $site_title string */
?>
<h1>На сайте "<?=!empty($site_title) ? $site_title : 'не указано'?>" было оставлено сообщение для обратной связи</h1>
<p>Ваше имя / Название компании: <?=!empty($message->company_name) ? $message->company_name : 'не указано'?></p>
<p>Область деятельности: <?=!empty($message->activity) ? $message->activity : 'не указано'?></p>
<p>Объем закупок в месяц: <?=!empty($message->buy_volume) ? $message->buy_volume : 'не указано'?></p>
<p>Адрес: <?=!empty($message->address) ? $message->address : 'не указано'?></p>
<p>Веб-сайт: <?=!empty($message->url) ? $message->url : 'не указано'?></p>
<p>Контактное лицо (имя): <?=!empty($message->name) ? $message->name : 'не указано'?></p>
<p>Номер телефона: <?=!empty($message->phone) ? $message->phone : 'не указано'?></p>
<p>E-mail: <?=!empty($message->email) ? $message->email : 'не указано'?></p>
<p>Дополнительная информация: <?=!empty($message->info) ? $message->info : ''?></p>