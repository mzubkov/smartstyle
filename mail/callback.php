<?php

use app\models\db\CallbackMessage;

/** @var $message CallbackMessage */
/** @var $site_title string */
?>
<h1>На сайте "<?=!empty($site_title) ? $site_title : 'не указано'?>" было оставлено сообщение для обратного звонка</h1>
<p>Имя: <?=!empty($message->name) ? $message->name : 'не указано'?></p>
<p>Телефон: <?=!empty($message->phone) ? $message->phone : 'не указано'?></p>