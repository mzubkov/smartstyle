<?php

namespace app\components;

use app\models\ImageRepository;
use yii\base\Component;
use yii\helpers\BaseInflector;
use yii\helpers\FileHelper;
use yii\imagine\Image;
use yii\web\UploadedFile;

/**
 * Class ImageService
 * @package app\components
 */
class ImageService extends Component
{
    /** @var string */
    public $storePath;

    public function init()
    {
        parent::init();

        $this->storePath = \Yii::getAlias($this->storePath);
    }

    /**
     * @param UploadedFile $file
     * @param ImageRepository $repository
     * @throws \Exception
     */
    public function save(UploadedFile $file, ImageRepository $repository)
    {
        $path = $this->storePath . $repository->path;
        foreach ($repository->variants as $name => $variant) {
            $method = lcfirst(BaseInflector::camelize($variant['method']));
            if ($file->extension == 'svg') {
                $file->saveAs("{$path}/{$repository->getFileName($name, $file->extension)}", false);
            } else {
                $this->$method(
                    $file->tempName,
                    $path,
                    $repository->getFileName($name, $file->extension),
                    $variant['width'] ?? null,
                    $variant['height'] ?? null
                );
            }
        }

        $file->saveAs("{$path}/{$repository->getFileName('src', $file->extension)}");
    }


    /**
     * @param string $image
     * @param string $path
     * @param string $filename
     * @param int|null $width
     * @param int|null $height
     * @throws \Exception
     */
    protected function resize(string $image, string $path, string $filename, ?int $width, ?int $height)
    {
        try {
            FileHelper::createDirectory($path);
        } catch (\Exception $e) {
            throw new \Exception('Не удалось создать директорию ' . $path);
        }

        if (!is_writable($path)) {
            throw new \Exception('Невозможно записать изображение на диск в директорию ' . $path);
        }

        try {
            Image::resize($image, $width, $height, true, true)
                ->save($path . '/'. $filename);
        } catch (\Exception $e) {
            throw new \Exception('Внутренняя ошибка сервера: ' . $e->getMessage());
        }
    }

    /**
     * @param string $image
     * @param string $path
     * @param string $filename
     * @param int $width
     * @param int $height
     */
    function resizeAndCrop(string $image, string $path, string $filename, int $width, int $height) {
        $info = getimagesize($image);

        if ($info[0] > $width || $info[1] > $height) {
            if(((float)$info[0]/$info[1]) > ((float)$width/$height)) {
                $img = Image::resize($image, null, $height, true, true);
            } else {
                $img = Image::resize($image, $width, null, true, true);
            }

            $info = $img->getSize();
            $x = ($info->getWidth() - $width)/2;
            $y = ($info->getHeight() - $height)/2;
            if ($x < 0) {
                $x = 0;
            }
            if ($y < 0) {
                $y = 0;
            }
            Image::crop($img, $width, $height, [$x, $y])
                ->save($path . '/'. $filename);
        } else {
            Image::frame($image, 0, 'fff')->save($path . '/'. $filename);
        }
    }

    /**
     * @param string $image
     * @param string $path
     * @param string $filename
     * @param int $width
     * @param int $height
     * @throws \Exception(
     */
    protected function crop(string $image, string $path, string $filename, int $width, int $height)
    {
        if (!is_writable($path)) {
            throw new \Exception('Невозможно записать изображение на диск в директорию ' . $path);
        }

        $info = getimagesize($image);
        $x = ($info[0] - $width)/2;
        $y = ($info[1] - $height)/2;
        if ($x < 0) {
            $x = 0;
        }
        if ($y < 0) {
            $y = 0;
        }
        try {
            Image::crop($image, $width, $height, [$x, $y])
                ->save($path . '/'. $filename);
        } catch (\Exception $e) {
            throw new \Exception('Внутренняя ошибка сервера: ' . $e->getMessage());
        }
    }
}