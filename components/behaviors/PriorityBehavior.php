<?php

namespace app\components\behaviors;

use yii\base\Behavior;
use yii\db\ActiveRecord;

class PriorityBehavior extends Behavior
{
    const PRIORITY_STEP = 10;

    /** @var ActiveRecord */
    public $owner;

    public function events()
    {
        return [
            ActiveRecord::EVENT_BEFORE_INSERT => 'setPriority'
        ];
    }

    /**
     * @param $event
     * @throws \yii\db\Exception
     */
    public function setPriority($event)
    {
        $max_priority = (int)\Yii::$app->db->createCommand(
            'select coalesce(max(priority), 0) from ' . $this->owner::tableName()
        )->queryScalar();
        $this->owner->priority = $max_priority + self::PRIORITY_STEP;
    }
}