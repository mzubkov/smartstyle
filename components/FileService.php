<?php

namespace app\components;

use app\models\FileRepository;
use yii\base\Component;
use yii\web\UploadedFile;

class FileService extends Component
{
    /** @var string */
    public $storePath;

    public function init()
    {
        parent::init();

        $this->storePath = \Yii::getAlias($this->storePath);
    }

    /**
     * @param UploadedFile $file
     * @param FileRepository $repository
     * @throws \Exception
     */
    public function save(UploadedFile $file, FileRepository $repository)
    {
        $path = $this->storePath . $repository->path;
        $file->saveAs("{$path}/{$repository->getFileName($file->extension)}");
    }


}