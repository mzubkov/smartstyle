<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%text_block}}`.
 */
class m191028_115344_create_text_page_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%text_page}}', [
            'id' => $this->primaryKey(),
            'identifier' => $this->string()->notNull()->unique(),
            'name' => $this->string(),
            'content' => $this->text()->defaultValue(null),
            'is_published' => $this->boolean()->defaultValue(false)
        ], $tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%text_page}}');
    }
}
