<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%navigation}}`.
 */
class m191021_071525_create_navigation_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%navigation}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255),
            'url' => $this->string(255),
            'is_published' => $this->boolean()->defaultValue(false),
            'priority' => $this->integer()->defaultValue(0)
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%navigation}}');
    }
}
