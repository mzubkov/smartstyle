<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%client}}`.
 */
class m191031_142556_create_client_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%client}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'login' => $this->string()->unique(),
            'password' => $this->string(),
            'is_published' => $this->boolean(),
            'auth_key' => $this->string(32),
            'last_login' => $this->timestamp(),
            'ip' => $this->string()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%client}}');
    }
}
