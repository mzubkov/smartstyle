<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%slideshow}}`.
 */
class m191028_093646_create_slideshow_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%slideshow}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'img_ext' => $this->string(6),
            'is_published' => $this->boolean(),
            'priority' => $this->integer()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%slideshow}}');
    }
}
