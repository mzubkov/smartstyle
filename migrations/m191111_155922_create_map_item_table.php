<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%map_item}}`.
 */
class m191111_155922_create_map_item_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%map_item}}', [
            'id' => $this->primaryKey(),
            'number' => $this->integer()->notNull(),
            'text' => $this->string()->notNull(),
            'is_published' => $this->boolean()->defaultValue(false),
            'priority' => $this->integer()->defaultValue(0)
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%map_item}}');
    }
}
