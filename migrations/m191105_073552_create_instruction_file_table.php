<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%instruction_file}}`.
 */
class m191105_073552_create_instruction_file_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%instruction_file}}', [
            'id' => $this->primaryKey(),
            'id_item' => $this->integer()->defaultValue(null),
            'name' => $this->string()->notNull(),
            'file_ext' => $this->string(6),
            'is_published' => $this->boolean()->defaultValue(false),
            'priority' => $this->integer()->defaultValue(0)
        ]);

        $this->addForeignKey(
            'fk-instruction_file-id_item',
            'instruction_file',
            'id_item',
            'instruction_item',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-instruction_file-id_item', 'instruction_file');
        $this->dropTable('{{%instruction_file}}');
    }
}
