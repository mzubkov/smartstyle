<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%sys_param}}`.
 */
class m191028_082811_create_sys_param_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%sys_param}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'identifier' => $this->string(),
            'value' => $this->string()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%sys_param}}');
    }
}
