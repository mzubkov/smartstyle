<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%catalog_product}}`.
 */
class m191030_151544_create_catalog_product_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%catalog_product}}', [
            'id' => $this->primaryKey(),
            'id_catalog' => $this->integer()->notNull(),
            'name' => $this->string()->notNull(),
            'img_ext' => $this->string(6),
            'is_published' => $this->boolean(),
            'priority' => $this->integer()->defaultValue(0)
        ]);

        $this->addForeignKey(
            'fk-catalog_product-id_catalog',
            'catalog_product',
            'id_catalog',
            'catalog',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-catalog_product-id_catalog', '{{%catalog_product}}');
        $this->dropTable('{{%catalog_product}}');
    }
}
