<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%instruction_item}}`.
 */
class m191105_073543_create_instruction_item_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%instruction_item}}', [
            'id' => $this->primaryKey(),
            'id_category' => $this->integer()->defaultValue(null),
            'name' => $this->string()->notNull(),
            'is_published' => $this->boolean()->defaultValue(false),
            'priority' => $this->integer()->defaultValue(0)
        ]);

        $this->addForeignKey(
            'fk-instruction_item-id_category',
            'instruction_item',
            'id_category',
            'instruction_category',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-instruction_item-id_category', 'instruction_item');
        $this->dropTable('{{%instruction_item}}');
    }
}
