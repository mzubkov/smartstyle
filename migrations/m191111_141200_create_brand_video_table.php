<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%brand_video}}`.
 */
class m191111_141200_create_brand_video_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%brand_video}}', [
            'id' => $this->primaryKey(),
            'id_brand' => $this->integer()->notNull(),
            'name' => $this->string()->notNull(),
            'video_code' => $this->string()->notNull(),
            'is_published' => $this->boolean(),
            'priority' => $this->integer()
        ]);

        $this->addForeignKey(
            'fk-brand_video-id_brand',
            'brand_video',
            'id_brand',
            'brand',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-brand_video-id_brand', '{{%brand_video}}');
        $this->dropTable('{{%brand_video}}');
    }

}
