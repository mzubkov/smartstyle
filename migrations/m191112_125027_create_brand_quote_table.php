<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%brand_quote}}`.
 */
class m191112_125027_create_brand_quote_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%brand_quote}}', [
            'id' => $this->primaryKey(),
            'id_brand' => $this->integer()->notNull(),
            'text' => $this->text(),
            'img_ext' => $this->string(6),
            'is_published' => $this->boolean()
        ]);

        $this->addForeignKey(
            'fk-brand_quote-id_brand',
            'brand_quote',
            'id_brand',
            'brand',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-brand_quote-id_brand', '{{%brand_quote}}');
        $this->dropTable('{{%brand_quote}}');
    }
}
