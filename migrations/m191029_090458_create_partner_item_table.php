<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%partner_item}}`.
 */
class m191029_090458_create_partner_item_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%partner_item}}', [
            'id' => $this->primaryKey(),
            'text' => $this->string(),
            'img_ext' => $this->string(6),
            'is_published' => $this->boolean(),
            'priority' => $this->integer()->defaultValue(0)
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%partner_item}}');
    }
}
