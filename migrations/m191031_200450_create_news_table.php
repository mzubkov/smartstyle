<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%news}}`.
 */
class m191031_200450_create_news_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%news}}', [
            'id' => $this->primaryKey(),
            'id_brand' => $this->integer(),
            'identifier' => $this->string()->unique()->notNull(),
            'name' => $this->string()->notNull(),
            'annotation' => $this->string(1024),
            'content' => $this->text(),
            'news_date' => $this->date(),
            'is_published' => $this->boolean()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%news}}');
    }
}
