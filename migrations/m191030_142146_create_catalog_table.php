<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%catalog}}`.
 */
class m191030_142146_create_catalog_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%catalog}}', [
            'id' => $this->primaryKey(),
            'id_brand' => $this->integer()->notNull(),
            'identifier' => $this->string()->unique()->notNull(),
            'name' => $this->string()->unique()->notNull(),
            'img_ext' => $this->string(6),
            'is_published' => $this->boolean(),
            'priority' => $this->integer()->defaultValue(0)
        ]);

        $this->addForeignKey(
            'fk-catalog-id_brand',
            'catalog',
            'id_brand',
            'brand',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-catalog-id_brand', '{{%catalog}}');
        $this->dropTable('{{%catalog}}');
    }
}
