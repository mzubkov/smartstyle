<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%brand_advantage}}`.
 */
class m191030_201038_create_brand_advantage_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%brand_advantage}}', [
            'id' => $this->primaryKey(),
            'id_brand' => $this->integer()->notNull(),
            'text' => $this->string()->notNull(),
            'is_published' => $this->boolean(),
            'priority' => $this->integer()
        ]);

        $this->addForeignKey(
            'fk-brand_advantage-id_brand',
            'brand_advantage',
            'id_brand',
            'brand',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-brand_advantage-id_brand', '{{%brand_advantage}}');
        $this->dropTable('{{%brand_advantage}}');
    }
}
