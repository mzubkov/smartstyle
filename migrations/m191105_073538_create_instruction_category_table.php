<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%instruction_category}}`.
 */
class m191105_073538_create_instruction_category_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%instruction_category}}', [
            'id' => $this->primaryKey(),
            'id_parent' => $this->integer()->defaultValue(null),
            'name' => $this->string()->notNull(),
            'is_published' => $this->boolean()->defaultValue(false),
            'priority' => $this->integer()->defaultValue(0)
        ]);

        $this->addForeignKey(
            'fk-instruction_category-id_parent',
            'instruction_category',
            'id_parent',
            'instruction_category',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-instruction_category-id_parent', 'instruction_category');
        $this->dropTable('{{%instruction_category}}');
    }
}
