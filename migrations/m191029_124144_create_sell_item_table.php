<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%sell_item}}`.
 */
class m191029_124144_create_sell_item_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%sell_item}}', [
            'id' => $this->primaryKey(),
            'text' => $this->string(),
            'img_ext' => $this->string(6),
            'is_published' => $this->boolean(),
            'priority' => $this->integer()->defaultValue(0)
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%sell_item}}');
    }
}
