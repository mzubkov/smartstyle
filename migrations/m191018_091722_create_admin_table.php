<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%admin}}`.
 */
class m191018_091722_create_admin_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%admin}}', [
            'id' => $this->primaryKey(),
            'login' => $this->string(),
            'password' => $this->string(),
            'auth_key' => $this->string(32),
            'last_login' => $this->timestamp(),
            'ip' => $this->string()
        ]);

        $this->insert('{{%admin}}', array(
            'login' => 'admin',
            'password' => Yii::$app->security->generatePasswordHash('test'),
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%admin}}');
    }
}
