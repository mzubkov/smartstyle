<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%car_model}}`.
 */
class m191031_075038_create_car_model_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%car_model}}', [
            'id' => $this->primaryKey(),
            'id_brand' => $this->integer()->notNull(),
            'name' => $this->string()->notNull(),
            'img_ext' => $this->string(6),
            'is_published' => $this->boolean(),
            'priority' => $this->integer()
        ]);

        $this->addForeignKey(
            'fk-car_model-id_brand',
            'car_model',
            'id_brand',
            'car_brand',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-car_model-id_brand', '{{%car_model}}');
        $this->dropTable('{{%car_model}}');
    }
}
