<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%brand_item}}`.
 */
class m191030_205203_create_brand_item_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%brand_item}}', [
            'id' => $this->primaryKey(),
            'id_brand' => $this->integer()->notNull(),
            'name' => $this->string()->notNull(),
            'text' => $this->string()->notNull(),
            'img_ext' => $this->string(6),
            'is_published' => $this->boolean(),
            'priority' => $this->integer()->defaultValue(0)
        ]);

        $this->addForeignKey(
            'fk-brand_item-id_brand',
            'brand_item',
            'id_brand',
            'brand',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-brand_item-id_brand', '{{%brand_item}}');
        $this->dropTable('{{%brand_item}}');
    }
}
