<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%news_image}}`.
 */
class m191031_200721_create_news_image_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%news_image}}', [
            'id' => $this->primaryKey(),
            'id_news' => $this->integer()->notNull(),
            'name' => $this->string(),
            'img_ext' => $this->string(6),
            'is_published' => $this->boolean(),
            'priority' => $this->integer()
        ]);

        $this->addForeignKey(
            'fk-news_image-id_news',
            'news_image',
            'id_news',
            'news',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-news_image-id_news', 'news_image');
        $this->dropTable('{{%news_image}}');
    }
}
