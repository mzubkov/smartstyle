<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%work_item}}`.
 */
class m191029_132619_create_work_item_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%work_item}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'is_published' => $this->boolean(),
            'priority' => $this->integer()->defaultValue(0)
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%work_item}}');
    }
}
