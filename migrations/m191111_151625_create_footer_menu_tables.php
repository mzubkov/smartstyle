<?php

use yii\db\Migration;

/**
 * Class m191111_151625_create_footer_menu_tables
 */
class m191111_151625_create_footer_menu_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%footer_menu_folder}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'is_published' => $this->boolean(),
            'priority' => $this->integer()
        ]);

        $this->createTable('{{%footer_menu_item}}', [
            'id' => $this->primaryKey(),
            'id_folder' => $this->integer()->notNull(),
            'name' => $this->string()->notNull(),
            'url' => $this->string()->notNull(),
            'is_published' => $this->boolean(),
            'priority' => $this->integer()
        ]);

        $this->addForeignKey(
            'fk-footer_menu_item-id_folder',
            'footer_menu_item',
            'id_folder',
            'footer_menu_folder',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-footer_menu_item-id_folder', '{{%footer_menu_item}}');
        $this->dropTable('{{%footer_menu_item}}');
        $this->dropTable('{{%footer_menu_folder}}');
    }
}
