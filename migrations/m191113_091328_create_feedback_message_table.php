<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%feedback_message}}`.
 */
class m191113_091328_create_feedback_message_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%feedback_message}}', [
            'id' => $this->primaryKey(),
            'company_name' => $this->string(),
            'activity' => $this->string(),
            'buy_volume' => $this->string(),
            'address' => $this->string(),
            'url' => $this->string(),
            'name' => $this->string(),
            'phone' => $this->string(),
            'email' => $this->string(),
            'info' => $this->string(1024)
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%feedback_message}}');
    }
}
