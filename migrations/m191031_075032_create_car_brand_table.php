<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%car_brand}}`.
 */
class m191031_075032_create_car_brand_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%car_brand}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'img_ext' => $this->string(6),
            'is_published' => $this->boolean(),
            'priority' => $this->integer()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%car_brand}}');
    }
}
