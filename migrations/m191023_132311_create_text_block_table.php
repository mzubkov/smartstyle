<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%text_block}}`.
 */
class m191023_132311_create_text_block_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%text_block}}', [
            'id' => $this->primaryKey(),
            'identifier' => $this->string()->notNull(),
            'name' => $this->string()->notNull(),
            'content' => $this->text(),
            'is_wysiwyg' => $this->boolean()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%text_block}}');
    }
}
