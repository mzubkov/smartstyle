<?php

use yii\db\Migration;

/**
 * Class m191112_114304_add_position_field_to_slideshow_table
 */
class m191112_114304_add_position_field_to_slideshow_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%slideshow}}', 'position', $this->tinyInteger()->defaultValue(0));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%slideshow}}', 'position');
    }

}
