<?php

use yii\db\Migration;

/**
 * Class m191021_102011_create_team_tables
 */
class m191021_102011_create_team_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%department}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255),
            'is_published' => $this->boolean()->defaultValue(false)
        ]);

        $this->createTable('{{%staff}}', [
            'id' => $this->primaryKey(),
            'id_department' => $this->integer()->notNull(),
            'name' => $this->string(255),
            'post' => $this->string(255),
            'phone' => $this->string(255),
            'img_ext' => $this->string(6),
            'is_published' => $this->boolean()->defaultValue(false)
        ]);

        $this->addForeignKey(
            'fk-staff-id_department',
            'staff',
            'id_department',
            'department',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-staff-id_department', '{{%staff}}');
        $this->dropTable('{{%staff}}');
        $this->dropTable('{{%department}}');
    }
}
