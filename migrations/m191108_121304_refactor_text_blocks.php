<?php

use yii\db\Migration;

/**
 * Class m191108_121304_refactor_text_blocks
 */
class m191108_121304_refactor_text_blocks extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('text_block', 'column_type', $this->string());
        $rows = $this->db->createCommand('select * from text_block')->queryAll();
        foreach ($rows as $row) {
            $this->db->createCommand('update text_block set column_type = :type where id = :id', [
                    ':type' => $row['is_wysiwyg'] ? 'wysiwyg' : 'input',
                    ':id' => $row['id']
                ])
                ->execute();
        }
        $this->dropColumn('text_block', 'is_wysiwyg');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191108_121304_refactor_text_blocks cannot be reverted.\n";

        return false;
    }
}
