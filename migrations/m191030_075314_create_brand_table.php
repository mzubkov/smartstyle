<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%brand}}`.
 */
class m191030_075314_create_brand_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%brand}}', [
            'id' => $this->primaryKey(),
            'identifier' => $this->string()->notNull()->unique(),
            'name' => $this->string()->notNull(),
            'description' => $this->string()->notNull(),
            'content' => $this->text(),
            'img_ext' => $this->string(6),
            'logo_ext' => $this->string(6),
            'page_img_ext' => $this->string(6),
            'is_published' => $this->boolean(),
            'priority' => $this->integer()->defaultValue(0)
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%brand}}');
    }
}
