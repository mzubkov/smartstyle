<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%support_message}}`.
 */
class m191114_074940_create_support_message_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%support_message}}', [
            'id' => $this->primaryKey(),
            'text' => $this->text(),
            'contact' => $this->string(512),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer()
        ]);

        $this->addColumn('{{%feedback_message}}', 'created_at', $this->integer()->defaultValue(0));
        $this->addColumn('{{%feedback_message}}', 'updated_at', $this->integer()->defaultValue(0));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%feedback_message}}', 'created_at');
        $this->dropColumn('{{%feedback_message}}', 'updated_at');
        $this->dropTable('{{%support_message}}');
    }
}
