<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%car_map}}`.
 */
class m191031_075052_create_car_map_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%car_map}}', [
            'id' => $this->primaryKey(),
            'id_model' => $this->integer()->notNull(),
            'name' => $this->string()->notNull(),
            'file_ext' => $this->string(6),
            'is_published' => $this->boolean()
        ]);

        $this->addForeignKey(
            'fk-car_map-id_model',
            'car_map',
            'id_model',
            'car_model',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-car_map-id_model', '{{%car_map}}');
        $this->dropTable('{{%car_map}}');
    }
}
