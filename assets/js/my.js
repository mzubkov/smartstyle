var numbersSwiper,mySwiper,mySwiper1,mySwiper2,mySwiper3,mySwiper4,clientsSwiper,partnershipSwiper,marketSwiper,frontSwiper,frontTeam,catSwiper,virtuesSwiper;
var settings = {
	unifActiveClass: 'active'
};
var $body;
$(document).ready(function () {
	$body = $('body');
	swiper_init();
	$(window).resize(function(){
		swiper_init();
	});

	var headSwiper = new Swiper('.head-img', {
		direction: 'horizontal',
		wrapperClass: 'head-img-slider',
		slideClass: 'head-img-slider-element',
		slidesPerView: 1,
		effect: 'fade',
		autoplay: 3000,
		speed: 1000
	});

	var spaceSwiper = new Swiper('.space', {
		direction: 'horizontal',
		wrapperClass: 'space-slider',
		slideClass: 'space-slider-slide',
		slidesPerView: 1,
		effect: 'fade',
		autoplay: 3000,
		speed: 1000
	});

	var spaceSwiper1 = new Swiper('.space_var', {
		direction: 'horizontal',
		wrapperClass: 'space_var-slider',
		slideClass: 'space_var-slider-slide',
		slidesPerView: 1,
		effect: 'fade',
		autoplay: 3000,
		speed: 1000
	});

	clientsSwiper = new Swiper('.partnership-clients-cont', {
		// Optional parameters
		direction: 'horizontal',
		wrapperClass: 'partnership-clients-list',
		slideClass: 'partnership-clients-item',
		slidesPerView: 4,
		autoplay: 2000,
		speed: 1000,
		spaceBetween: 30,

		// If we need pagination
		pagination: '.partnership-clients-pagination',
		paginationClickable: true,
		grabCursor: true,
		breakpoints: {
			1279:{
				slidesPerView: 4,
				spaceBetween: 15
			},
			1023:{
				slidesPerView: 2,
				spaceBetween: 30

			},
			767: {
				slidesPerView: 1
			}
		},
		onInit:function(swiper){
			if($('.partnership-clients-pagination .swiper-pagination-bullet').length == 1){
				$('.partnership-clients-pagination').hide();
			}
		}
	});

	/*if(window.innerWidth < 1024 && typeof clientsSwiper == 'undefined'){
		clientsSwiper = new Swiper ('.partnership-clients-cont', {
			// Optional parameters
			direction: 'horizontal',
			wrapperClass: 'partnership-clients-list',
			slideClass: 'partnership-clients-item',
			loop: true,
			slidesPerView: 2,
			autoplay: 2000,
			speed: 1000,
			spaceBetween: 30,

			// If we need pagination
			pagination: '.partnership-clients-pagination',
			paginationClickable: true,
			grabCursor: true,
			breakpoints: {
				767: {
					slidesPerView: 1,
					spaceBetween: 15
				}
			}

		})
	}*/

	jQuery('.head-nav-btn').click(function(){
		jQuery('.head-nav-list').toggleClass(settings.unifActiveClass);
		return false;
	});

	jQuery('.head-tel-tx a,.head-box .head-tel-drop-close').click(function(){
		jQuery('.head-tel-drop').toggleClass(settings.unifActiveClass);
		return false;
	});

	$('.foot-column h4').click(function(){
		if(window.innerWidth < 1023){
			if(!$(this).parent().hasClass(settings.unifActiveClass)){
				$('.foot-column').removeClass(settings.unifActiveClass);
				$(this).parent().addClass(settings.unifActiveClass);
			}
			else{
				$('.foot-column').removeClass(settings.unifActiveClass);
			}
		}
	});

	scrollFunc();
	$(window).scroll(function(){
		scrollFunc();
	});

	var $catSwiperElms = $('.catalog-category');
	if($catSwiperElms.length){
		if(typeof catSwiper === 'undefined'){
			catSwiper = [];
		}
		$catSwiperElms.each(function(e){
			var $_this = $(this);

			if(!$_this.hasClass('swiperCatIndx_'+e)){
				$_this.addClass('swiperCatIndx_'+e);
			}
			if(typeof catSwiper !== 'undefined' && typeof catSwiper[e] === 'undefined'){
				catSwiper[e] = new Swiper ('.swiperCatIndx_'+e+' .catalog-category-container', {
					// Optional parameters
					direction: 'horizontal',
					wrapperClass: 'swiperCatIndx_'+e+' .catalog-category-list',
					slideClass: 'swiperCatIndx_'+e+' .catalog-category-list__element',
					loop: true,
					slidesPerView: 4,
					autoplay: false,
					speed: 1000,
					spaceBetween: 30,

					// If we need pagination
					pagination: '.swiperCatIndx_'+e+' .catalog-category-pagination',
					paginationClickable: true,
					grabCursor: true,
					nextButton: '.swiperCatIndx_'+e+' .catalog-category-control__next',
					prevButton: '.swiperCatIndx_'+e+' .catalog-category-control__prev',
					breakpoints: {
						1023: {
							slidesPerView: 4,
							spaceBetween: 10
						},
						767: {
							slidesPerView: 2,
							spaceBetween: 20
						},
						479: {
							slidesPerView: 2,
							spaceBetween: 10
						}
					}

				})
			}
		});
	}
	$body.on('click','[data-unifScrollTo]',function(){
		var $_this = $(this);
		var $parentsMain = $('.page');
		var $scrollElm = $('html,body');

		var $id = $_this.attr('data-unifScrollTo');
		var $frame = $parentsMain.find('[data-unifScrollFrom="'+$id+'"]');

		if($frame.length){
			var $topElm = $frame.offset().top;
			var topPadding = 10;
			if(typeof $frame.attr('data-unifScrollPadding') !== 'undefined'){
				topPadding = parseInt($frame.attr('data-unifScrollPadding'));
			}
			if($('body').hasClass('head-panel-fixed') || $topElm > $('.head-panel').outerHeight(true)){
				$topElm -= $('.head-panel').outerHeight(true);
			}
			$topElm -= topPadding;
			$scrollElm.animate({
				scrollTop: $topElm
			},{
				duration: 600,
				easing: 'swing',
				start: function(){

				}
			});
		}
		return false;
	});
	$body.on('click','[data-brandId] .technmaps-brands-choice-list__element-title',function(e){
		e.preventDefault();
		var $_this = $(this);
		var $parent = $_this.parents('[data-brandId]');
		if(!$parent.hasClass(settings.unifActiveClass)){
			if(window.innerWidth >= 768){
				$parent = $('.technmaps-brands-wrapper[data-brandModelId="'+$parent.attr('data-brandId')+'"]');
				$parent.find('.technmaps-brands-settings').html('');
				$parent.find('.technmaps-brands-models-list__model-title.'+settings.unifActiveClass).removeClass(settings.unifActiveClass);
				$('[data-brandModelId]').removeClass(settings.unifActiveClass).removeAttr('style');
				$('[data-brandId]').removeClass(settings.unifActiveClass);
			}
			else{
				$parent.find('.technmaps-brands-models-list__model-title').removeClass(settings.unifActiveClass);
				$parent.find('[data-modelSettings]').html('');
			}

			$parent.show();

			$_this.parents(['data-brandId']).addClass(settings.unifActiveClass);
		}
		else{
			if(window.innerWidth < 768){
				$('[data-brandModelId]').removeClass(settings.unifActiveClass).removeAttr('style');
				$parent.removeClass(settings.unifActiveClass);
				$parent.find('.technmaps-brands-models-list__model-cont').removeAttr('style');
			}
		}
	});
	$body.on('click','.technmaps-brands-models-list__model-title',function(e){
		e.preventDefault();
		var $_this = $(this);
		var $parent = $_this.parents('.technmaps-brands-models-list');
		var $parentSettings = $_this.parents('.technmaps-brands-wrapper-cont').find('.technmaps-brands-settings');
		if(window.innerWidth >= 768) {
			if (!$_this.hasClass('active')) {
				$parentSettings.html('');
				$parent.find('.technmaps-brands-models-list__model-title.' + settings.unifActiveClass).removeClass(settings.unifActiveClass);
				$_this.addClass(settings.unifActiveClass);
				$parentSettings.append($_this.parents('.technmaps-brands-models-list__model').find('.technmaps-brands-models-list__model-settings').clone());
			}
		}
		if(window.innerWidth < 768){
			$_this.parents('.technmaps-brands-models-list').find('[data-modelSettings]').html('');
			if(window.innerWidth >= 480){
				$parentSettings = $_this.parents('.technmaps-brands-models-list').find('[data-modelSettings="'+$_this.parents('.technmaps-brands-models-list__model').attr('data-modelSettingsCont')+'"]');
				if (!$_this.hasClass('active')) {
					$parentSettings.html('');
					$parentSettings.append($_this.parents('.technmaps-brands-models-list__model').find('.technmaps-brands-models-list__model-settings').clone());
				}
			}
			else{
				$_this.parents('.technmaps-brands-models-list').find('.technmaps-brands-models-list__model-cont').removeAttr('style');
				$_this.parents('.technmaps-brands-models-list__model').find('.technmaps-brands-models-list__model-cont').show();
			}

			$parent.find('.technmaps-brands-models-list__model-title.' + settings.unifActiveClass).removeClass(settings.unifActiveClass);
			$_this.addClass(settings.unifActiveClass);
		}
	});
	$body.on('click','.documentation-list__item-serieslist__el-title,.documentation-list__item-serieslist__el-modellist__elem-name',function(e){
		e.preventDefault();
		if(!$(this).parent().hasClass(settings.unifActiveClass)){
			$(this).parent().addClass(settings.unifActiveClass);
		}
		else{
			$(this).parent().removeClass(settings.unifActiveClass);
			$(this).parent().find('.documentation-list__item-serieslist__el-modellist__elem').removeClass(settings.unifActiveClass);
		}
	});
	$body.on('click','.newsandevents-head-selected',function(e){
		e.preventDefault();
		if(!$(this).parent().hasClass('opened')){
			$(this).parent().addClass('opened');
		}
		else{
			$(this).parent().removeClass('opened');
		}
	});
	brandsClone();

	$('.foot-menu a').click(function(){
		return false;
	});

	get_button_up();
	$(window).scroll(function(){
		get_button_up();
	});

	$('.req_url').val(window.location.href);
	$('.reg_uri').val(0);

	var send = true;
	$('input,textarea').on('focus',function(){
		$(this).removeClass('error');
		send = true;
	});

	$('a[href="#send_message"]').click(function(){
		$('.popup').hide();
		$('.popup-bg,.popup_send,.pop_f').show();
		return false;
	});

	$('#tech_form,#form_call_me,#form_anketa,#form_popup_bottom').submit(function(){
		var _this = this;
		var datas = $(this).serializeArray();

		$(this).find('[data-required]').each(function(){
			if($(this).val() == ''){
				$(this).addClass('error');
				send = false;
			}
			if(typeof $(this).attr('data-required_email') != 'undefined' && $(this).val().indexOf('@') == -1){
				$(this).addClass('error');
				send = false;
			}
		});

		if(send){
			send = false;
			$.post(
				'',
				datas,
				function(data){
					send = true;
					var data = $.parseJSON(data);
					$('.pop_f').hide();
					if(!data.errors){
						if($(_this).attr('id') == 'form_call_me'){
							jQuery('.head-tel-drop').toggleClass('active');
						}
						$('.popup_send').find('h4').html('Ваша заявка принята!');
						$('.popup_send').find('.popup-txt').html(data.send_ok);
						$('.popup-bg,.popup_send').show();
						$('.popup').css('display','flex');
						$(_this)[0].reset();
					}
					else{
						$('.popup_send').find('h4').html('Ошибка отправки');
						$('.popup_send').find('.popup-txt').html('');
						$('.popup-bg,.popup_send,.popup').show();
					}
				}
			);
		}

		return false;
	});

	$('.popup-close,.popup-box .head-tel-drop-close').click(function(){
		$('.popup-bg,.popup_send,.pop_f').hide();
		$('.popup').show();
	});

	$('.btn-up').click(function(){
		$('html,body').animate({
			'scrollTop':0
		},{
			duration:1000,
			'easing':'linear'
		});
	});

	$('a[href="#price"]').click(function(){
		$('html,body').animate({
			scrollTop:($('.questionary').offset().top-$('.head-panel').height())
		},{
			duration:1000,
			'easing':'linear'
		});
		return false;
	});

	$('.head-nav-list li>a').click(function(){
		var _this = this;
		$('html,body').animate({
			scrollTop:($('#'+$(_this).attr('data-href')).offset().top-$('.head-panel').height())
		},{
			duration:1000,
			'easing':'linear'
		});
		return false;
	});

	$('a[href="#registration"]').click(function(){
		$('html,body').animate({
			scrollTop:($('.questionary').offset().top-$('.head-panel').height())
		},{
			duration:1000,
			'easing':'linear'
		});
		return false;
	});
});

function swiper_init(){

	if(typeof clientsSwiper != 'undefined'){
		if($('.partnership-clients-pagination .swiper-pagination-bullet').length !== 1){
			$('.partnership-clients-pagination').show();
		}
		else{
			$('.partnership-clients-pagination').hide();
		}
	}

	if($('.slider-wrapper').length){
		if(window.innerWidth < 768 && typeof mySwiper == 'undefined'){
			mySwiper = new Swiper('.slider-wrapper', {
				// Optional parameters
				wrapperClass: 'slider-wrapper-wr',
				direction: 'horizontal',
				slideClass: 'slider_slide',
				loop: true,
				//autoHeight: false,
				slidesPerView: 1,
				autoplay: 2000,
				speed: 1000,
				spaceBetween: 15,
				// If we need pagination
				pagination: '.pag',
				paginationClickable: true,
				//freeMode: true,
				grabCursor: true

				// And if we need scrollbar
				//scrollbar: '.swiper-scrollbar',
			});
		}
		if(window.innerWidth >= 768 && typeof mySwiper != 'undefined' && mySwiper){
			mySwiper.destroy();
			mySwiper = undefined;
			$('.slider-wrapper').attr('style', '');
			$('.slider-wrapper-wr').attr('style', '');
			$('.slider_slide').attr('style', '');
		}
	}

	if($('.slider-wrapper1').length){
		if(window.innerWidth < 768 && typeof mySwiper1 == 'undefined'){
			mySwiper1 = new Swiper('.slider-wrapper1', {
				// Optional parameters
				wrapperClass: 'slider-wrapper1 .slider-wrapper-wr',
				direction: 'horizontal',
				slideClass: 'slider-wrapper1 .slider_slide',
				loop: true,
				//autoHeight: false,
				slidesPerView: 1,
				autoplay: 2000,
				speed: 1000,
				spaceBetween: 15,
				// If we need pagination
				pagination: '.pag1',
				paginationClickable: true,
				//freeMode: true,
				grabCursor: true

				// And if we need scrollbar
				//scrollbar: '.swiper-scrollbar',
			});
		}
		if(window.innerWidth >= 768 && typeof mySwiper1 != 'undefined' && mySwiper1){
			mySwiper1.destroy();
			mySwiper1 = undefined;
			$('.slider-wrapper1').attr('style', '');
			$('.slider-wrapper1 .slider-wrapper-wr').attr('style', '');
			$('.slider-wrapper1 .slider_slide').attr('style', '');
		}
	}

	if($('.slider-wrapper2').length) {
		if (window.innerWidth < 768 && typeof mySwiper2 == 'undefined') {
			mySwiper2 = new Swiper('.slider-wrapper2', {
				// Optional parameters
				wrapperClass: 'slider-wrapper2 .slider-wrapper-wr',
				direction: 'horizontal',
				slideClass: 'slider-wrapper2 .slider_slide',
				loop: true,
				//autoHeight: false,
				slidesPerView: 1,
				autoplay: 2000,
				speed: 1000,
				spaceBetween: 15,
				// If we need pagination
				pagination: '.pag2',
				paginationClickable: true,
				//freeMode: true,
				grabCursor: true

				// And if we need scrollbar
				//scrollbar: '.swiper-scrollbar',
			});
		}
		if (window.innerWidth >= 768 && typeof mySwiper2 != 'undefined' && mySwiper2) {
			mySwiper2.destroy();
			mySwiper2 = undefined;
			$('.slider-wrapper2').attr('style', '');
			$('.slider-wrapper2 .slider-wrapper-wr').attr('style', '');
			$('.slider-wrapper2 .slider_slide').attr('style', '');
		}
	}

	if($('.slider-wrapper3').length) {
		if (window.innerWidth < 768 && typeof mySwiper3 == 'undefined') {
			mySwiper3 = new Swiper('.slider-wrapper3', {
				// Optional parameters
				wrapperClass: 'slider-wrapper3 .slider-wrapper-wr',
				direction: 'horizontal',
				slideClass: 'slider-wrapper3 .slider_slide',
				loop: true,
				//autoHeight: false,
				slidesPerView: 1,
				autoplay: 2000,
				speed: 1000,
				spaceBetween: 15,
				// If we need pagination
				pagination: '.pag3',
				paginationClickable: true,
				//freeMode: true,
				grabCursor: true

				// And if we need scrollbar
				//scrollbar: '.swiper-scrollbar',
			});
		}
		if (window.innerWidth >= 768 && typeof mySwiper3 != 'undefined' && mySwiper3) {
			mySwiper3.destroy();
			mySwiper3 = undefined;
			$('.slider-wrapper3').attr('style', '');
			$('.slider-wrapper3 .slider-wrapper-wr').attr('style', '');
			$('.slider-wrapper3 .slider_slide').attr('style', '');
		}
	}

	if($('.partnership-step-cont').length){
		if(window.innerWidth < 1024 && typeof partnershipSwiper == 'undefined'){
			partnershipSwiper = new Swiper ('.partnership-step-cont', {
				// Optional parameters
				direction: 'horizontal',
				wrapperClass: 'partnership-step-list',
				slideClass: 'partnership-step-item',
				loop: true,
				slidesPerView: 1,
				autoplay: 2000,
				speed: 1000,
				//spaceBetween: 15,

				// If we need pagination
				pagination: '.partnership-step-pagination',
				paginationClickable: true,
				grabCursor: true

			})
		}
		if(window.innerWidth >= 1024 && typeof partnershipSwiper != 'undefined' && partnershipSwiper){
			partnershipSwiper.destroy();
			partnershipSwiper = undefined;
			$('.partnership-step-cont').attr('style', '');
			$('.partnership-step-list').attr('style', '');
			$('.partnership-step-item').attr('style', '');
		}
	}
	/*if(window.innerWidth >= 1024 && typeof clientsSwiper != 'undefined' && clientsSwiper){
		clientsSwiper.destroy();
		clientsSwiper = undefined;
		$('.partnership-clients-cont').attr('style', '');
		$('.partnership-clients-list').attr('style', '');
		$('.partnership-clients-item').attr('style', '');
	}*/
	if($('.market-cont').length){
		if(window.innerWidth < 1024 && typeof marketSwiper == 'undefined'){
			marketSwiper = new Swiper ('.market-cont', {
				// Optional parameters
				direction: 'horizontal',
				wrapperClass: 'market-cont-list',
				slideClass: 'market-cont-item',
				loop: true,
				slidesPerView: 1,
				autoplay: 2000,
				speed: 1000,
				//spaceBetween: 15,

				// If we need pagination
				pagination: '.market-pagination',
				paginationClickable: true,
				grabCursor: true

			})
		}
		if(window.innerWidth >= 1024 && typeof marketSwiper != 'undefined' && marketSwiper){
			marketSwiper.destroy();
			marketSwiper = undefined;
			$('.market-cont').attr('style', '');
			$('.market-cont-list').attr('style', '');
			$('.market-cont-item').attr('style', '');
		}
	}


	var $frontSliderElm = $('.tile_vary');
	if($frontSliderElm.length){
		if(window.innerWidth >= 1024 && typeof frontSwiper != 'undefined' && frontSwiper){
			frontSwiper.destroy();
			frontSwiper = undefined;
			$frontSliderElm.attr('style', '');
			$frontSliderElm.find('.tile_vary-list').attr('style', '');
			$frontSliderElm.find('.tile_vary-list__elem').attr('style', '');
		}
		if(window.innerWidth < 1024 && typeof frontSwiper == 'undefined'){
			frontSwiper = new Swiper ('.tile_vary', {
				// Optional parameters
				direction: 'horizontal',
				wrapperClass: 'tile_vary-list',
				slideClass: 'tile_vary-list__elem',
				loop: true,
				slidesPerView: 2,
				autoplay: 2000,
				speed: 1000,
				spaceBetween: 30,

				// If we need pagination
				pagination: '.tile_vary-pagination',
				paginationClickable: true,
				grabCursor: true,
				breakpoints: {
					767: {
						slidesPerView: 2,
						spaceBetween: 20
					},
					479: {
						slidesPerView: 1,
						spaceBetween: 15
					}
				}

			})
		}
	}

	var $frontTeamElms = $('.team-group');
	if($frontTeamElms.length){
		if(typeof frontTeam === 'undefined'){
			frontTeam = [];
		}
		$frontTeamElms.each(function(e){
			var $_this = $(this);
			var $thisNameBl = $_this.find('.team-group-name');

			if(!$_this.hasClass('swiperIndx_'+e)){
				$_this.addClass('swiperIndx_'+e);
			}
			if(window.innerWidth >= 1024 && typeof frontTeam !== 'undefined' && typeof frontTeam[e] !== 'undefined'){
				frontTeam[e].destroy();
				frontTeam[e] = undefined;
				$_this.attr('style', '');
				$_this.find('.team-group-list').attr('style', '');
				$_this.find('.team-group-list__el').attr('style', '');
			}
			if(window.innerWidth < 1024 && typeof frontTeam !== 'undefined' && typeof frontTeam[e] === 'undefined'){
				frontTeam[e] = new Swiper ('.swiperIndx_'+e, {
					// Optional parameters
					direction: 'horizontal',
					wrapperClass: 'swiperIndx_'+e+' .team-group-list',
					slideClass: 'swiperIndx_'+e+' .team-group-list__el',
					loop: true,
					slidesPerView: 2,
					autoplay: 2000,
					speed: 1000,
					spaceBetween: 30,

					// If we need pagination
					pagination: '.swiperIndx_'+e+' .team-group-pagination',
					paginationClickable: true,
					grabCursor: true,
					breakpoints: {
						767: {
							slidesPerView: 2,
							spaceBetween: 20
						},
						479: {
							slidesPerView: 1,
							spaceBetween: 15
						}
					}

				})
			}

			if(window.innerWidth >= 1024){
				var $list = $_this.find('.team-group-list');
				var $elms = $_this.find('.team-group-list__el');
				var $elmsRow = $list.width()/$elms.eq(0).outerWidth(true);

				if($elms.length%$elmsRow !== 0){
					$thisNameBl.css({
						width: ($elms.length%$elmsRow)*$elms.eq(0).outerWidth(true)-parseInt($elms.eq(0).css('marginRight'))+'px'
					});
				}
			}
			else{
				$thisNameBl.removeAttr('style');
			}
		});
	}

	var $virtuesSwiperElm = $('.virtues-cont');
	if($virtuesSwiperElm.length){
		if(window.innerWidth >= 1024 && typeof virtuesSwiper !== 'undefined' && virtuesSwiper){
			virtuesSwiper.destroy();
			virtuesSwiper = undefined;
			$virtuesSwiperElm.attr('style', '');
			$virtuesSwiperElm.find('.virtues-list').attr('style', '');
			$virtuesSwiperElm.find('.virtues-list__item').attr('style', '');
		}
		if(window.innerWidth < 1024 && typeof virtuesSwiper === 'undefined'){
			virtuesSwiper = new Swiper ('.virtues-cont', {
				// Optional parameters
				direction: 'horizontal',
				wrapperClass: 'virtues-list',
				slideClass: 'virtues-list__item',
				loop: true,
				slidesPerView: 2,
				autoplay: 2000,
				speed: 1000,
				spaceBetween: 30,

				// If we need pagination
				pagination: '.virtues-pagination',
				paginationClickable: true,
				grabCursor: true,
				breakpoints: {
					767: {
						slidesPerView: 1,
						spaceBetween: 20
					},
					479: {
						slidesPerView: 1,
						spaceBetween: 10
					}
				}

			})
		}
	}

	var $numbersSwiperElm = $('.statistics');
	if($numbersSwiperElm.length){
		if(window.innerWidth >= 1024 && typeof numbersSwiper !== 'undefined' && numbersSwiper){
			numbersSwiper.destroy();
			numbersSwiper = undefined;
			$numbersSwiperElm.attr('style', '');
			$numbersSwiperElm.find('.statistics-list').attr('style', '');
			$numbersSwiperElm.find('.statistics-list__item').attr('style', '');
		}
		if(window.innerWidth < 1024 && typeof numbersSwiper === 'undefined'){
			numbersSwiper = new Swiper ('.statistics', {
				// Optional parameters
				direction: 'horizontal',
				wrapperClass: 'statistics-list',
				slideClass: 'statistics-list__item',
				loop: true,
				slidesPerView: 2,
				autoplay: 2000,
				speed: 1000,
				spaceBetween: 30,

				// If we need pagination
				pagination: '.statistics-pagination',
				paginationClickable: true,
				grabCursor: true,
				breakpoints: {
					767: {
						slidesPerView: 1,
						spaceBetween: 20
					},
					479: {
						slidesPerView: 1,
						spaceBetween: 15
					}
				}

			})
		}
	}
}

ymaps.ready(function () {
	var mapMain = document.querySelector('#map_contacts');

	if(mapMain){
		var positionMap = [
			mapMain.getAttribute('data-map_position_lat'),
			mapMain.getAttribute('data-map_position_long')
		];
		var myMap = new ymaps.Map('map_contacts', {
				center: positionMap,
				zoom: mapMain.getAttribute('data-map_zoom')
			}, {
				searchControlProvider: 'yandex#search'
			}),

			// Создаём макет содержимого.
			MyIconContentLayout = ymaps.templateLayoutFactory.createClass(
				'<div style="color: #FFFFFF; font-weight: bold;">$[properties.iconContent]</div>'
			),

			myPlacemarkWithContent = new ymaps.Placemark(positionMap, {
				//hintContent: 'Собственный значок метки с контентом',
				//balloonContent: 'А эта — новогодняя',
				//iconContent: ''
			}, {
				// Опции.
				// Необходимо указать данный тип макета.
				iconLayout: 'default#imageWithContent',
				// Своё изображение иконки метки.
				iconImageHref: '/img/pointer.png',
				// Размеры метки.
				iconImageSize: [33, 50],
				// Смещение левого верхнего угла иконки относительно
				// её "ножки" (точки привязки).
				iconImageOffset: [-16, -50],
				// Смещение слоя с содержимым относительно слоя с картинкой.
				//iconContentOffset: [15, 15],
				// Макет содержимого.
				//iconContentLayout: MyIconContentLayout
			});

		myMap.geoObjects
			.add(myPlacemarkWithContent);
		// Открываем балун на карте (без привязки к геообъекту).
		myMap.balloon.open(positionMap, mapMain.getAttribute('data-mapContent'), {
			// Опция: не показываем кнопку закрытия.
			offset: [-16, -50],
			closeButton: false
		});
		myMap.behaviors.disable('scrollZoom');
	}


});

function scrollFunc(){
	var scrollTopWindow = $(window).scrollTop();
	if(scrollTopWindow != 0 && !$('body').hasClass('head-panel-fixed') && window.innerWidth >=1024){
		$('body').addClass('head-panel-fixed');
	}
	if(scrollTopWindow == 0 && $('body').hasClass('head-panel-fixed')){
		$('body').removeClass('head-panel-fixed');
	}
}

function brandsClone(){
	var $brands = $('[data-brandId]');
	if($brands.length){
		$brands.each(function(){
			var $brandModelCont = $('[data-brandModelId="'+$(this).attr('data-brandId')+'"]');
			if($brandModelCont.length){
				$(this).find('.technmaps-brands-choice-list__element-cont').append($brandModelCont.find('.technmaps-brands-wrapper-cont').clone());
			}
		});
	}
}

function get_button_up(){
	if($(window).scrollTop() >= ($('.show_button_up').offset().top+ $('.show_button_up').height() + $('.head-panel').outerHeight(true))){
		$('.btn-up').show();
	}
	else{
		$('.btn-up').hide();
	}
}
