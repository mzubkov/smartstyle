<?php

namespace app\assets;

use yii\web\AssetBundle;

class SiteAsset extends AssetBundle
{
    public $sourcePath = '@app/assets';
    public $css = [
        'css/style.css',
        'css/common.css',
    ];

    public $js = [
        ['js/jquery-1.11.3.min.js', 'defer' => 'defer'],
        ['js/swiper.jquery.min.js', 'defer' => 'defer'],
        ['js/my.js', 'defer' => 'defer'],
        'https://api-maps.yandex.ru/2.1/?lang=ru_RU&amp;apikey=3fe6b0cf-2f53-4dd4-abbb-952483db2487'
    ];
}