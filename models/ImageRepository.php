<?php

namespace app\models;

use yii\base\Model;

class ImageRepository extends Model
{
    /** @var mixed */
    public $model;

    /** @var string */
    public $path;

    /** @var array */
    public $variants;

    /** @var string */
    public $ext_field = 'img_ext';

    /**
     * @param $variant
     * @param $ext
     * @return string
     */
    public function getFileName($variant, $ext)
    {
        return "{$this->model->id}_{$variant}.{$ext}";
    }

    /**
     * @param $variant
     * @return string
     */
    public function get($variant)
    {
        $ext_field = $this->ext_field;
        return "{$this->path}/{$this->model->id}_{$variant}.{$this->model->$ext_field}";
    }
}