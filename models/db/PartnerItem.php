<?php

namespace app\models\db;

use app\models\ImageModelInterface;
use app\models\ImageTrait;

/**
 * This is the model class for table "partner_item".
 *
 * @property int $id
 * @property string $text
 * @property string $img_ext
 * @property int $is_published
 * @property int $priority
 */
class PartnerItem extends BaseActiveRecord implements ImageModelInterface
{
    use ImageTrait;

    protected $imgConfig = '@app/config/image_settings/partner_item.php';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'partner_item';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['is_published', 'priority'], 'integer'],
            [['text'], 'string', 'max' => 255],
            [['img_ext'], 'string', 'max' => 6],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'text' => 'Текст',
            'img_ext' => 'Изображение',
            'is_published' => 'Статус',
            'priority' => 'Приоритет',
        ];
    }

    /**
     * {@inheritdoc}
     * @return PartnerItemQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PartnerItemQuery(get_called_class());
    }
}
