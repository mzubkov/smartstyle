<?php

namespace app\models\db;


/**
 * This is the model class for table "navigation".
 *
 * @property int $id
 * @property string $name
 * @property string $url
 * @property int $is_published
 * @property int $priority
 */
class Navigation extends BaseActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'navigation';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'url'], 'required'],
            [['is_published', 'priority'], 'integer'],
            [['name', 'url'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'url' => 'URL',
            'is_published' => 'Статус',
            'priority' => 'Порядок'
        ];
    }

    /**
     * @return NavigationQuery|object|\yii\db\ActiveQuery
     * @throws \yii\base\InvalidConfigException
     */
    public static function find()
    {
        return \Yii::createObject(NavigationQuery::class, [get_called_class()]);
    }
}
