<?php

namespace app\models\db;

/**
 * This is the model class for table "instruction_item".
 *
 * @property int $id
 * @property int $id_category
 * @property string $name
 * @property int $is_published
 * @property int $priority
 *
 * @property InstructionFile[] $files
 * @property InstructionCategory $category
 */
class InstructionItem extends BaseActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'instruction_item';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_category', 'is_published', 'priority'], 'integer'],
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
            [['id_category'], 'exist', 'skipOnError' => true, 'targetClass' => InstructionCategory::class, 'targetAttribute' => ['id_category' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_category' => 'Категория',
            'name' => 'Название',
            'is_published' => 'Статус',
            'priority' => 'Порядок',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFiles()
    {
        return $this->hasMany(InstructionFile::class, ['id_item' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(InstructionCategory::class, ['id' => 'id_category']);
    }

    /**
     * {@inheritdoc}
     * @return InstructionItemQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new InstructionItemQuery(get_called_class());
    }
}
