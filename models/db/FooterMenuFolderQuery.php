<?php

namespace app\models\db;

use yii\db\ActiveQuery;

/**
 * This is the ActiveQuery class for [[FooterMenuFolder]].
 *
 * @see FooterMenuFolder
 */
class FooterMenuFolderQuery extends ActiveQuery implements AdminQueryInterface
{
    /**
     * @return FooterMenuFolderQuery
     */
    public function forAdmin()
    {
        return $this->addOrderBy([
            'id' => SORT_ASC
        ]);
    }

    /**
     * @return FooterMenuFolderQuery
     */
    public function forSite()
    {
        return $this->andWhere(['is_published' => true]);
    }
}