<?php

namespace app\models\db;

/**
 * This is the model class for table "footer_menu_folder".
 *
 * @property int $id
 * @property string $name
 * @property int $is_published
 * @property int $priority
 *
 * @property FooterMenuItem[] $items
 * @property FooterMenuItem[] $publishedItems
 */
class FooterMenuFolder extends BaseActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'footer_menu_folder';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['is_published', 'priority'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'is_published' => 'Статус',
            'priority' => 'Порядок',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItems()
    {
        return $this->hasMany(FooterMenuItem::class, ['id_folder' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPublishedItems()
    {
        return $this->hasMany(FooterMenuItem::class, ['id_folder' => 'id'])->andWhere(['is_published' => 1]);
    }

    /**
     * {@inheritdoc}
     * @return FooterMenuFolderQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new FooterMenuFolderQuery(get_called_class());
    }
}
