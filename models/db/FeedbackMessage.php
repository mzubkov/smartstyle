<?php

namespace app\models\db;

use app\models\SendMailInterface;
use app\models\SysParamFinder;

/**
 * This is the model class for table "feedback_message".
 *
 * @property int $id
 * @property string $company_name
 * @property string $activity
 * @property string $buy_volume
 * @property string $address
 * @property string $url
 * @property string $name
 * @property string $phone
 * @property string $email
 * @property string $info
 * @property int $created_at
 * @property int $updated_at
 */
class FeedbackMessage extends BaseActiveRecord implements SendMailInterface
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'feedback_message';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['company_name', 'name', 'email'], 'required'],
            [['company_name', 'activity', 'buy_volume', 'address', 'url', 'name', 'phone', 'email'], 'string', 'max' => 255],
            ['email', 'email'],
            [['created_at', 'updated_at'], 'integer'],
            [['info'], 'string', 'max' => 1024],
        ];
    }

    public function init()
    {
        parent::init();
        $this->on(self::EVENT_AFTER_INSERT, [$this, 'sendMail']);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_name' => 'Ваше имя / Название компании',
            'activity' => 'Область деятельности',
            'buy_volume' => 'Объем закупок в месяц',
            'address' => 'Адрес',
            'url' => 'Веб-сайт',
            'name' => 'Контактное лицо (имя)',
            'phone' => 'Номер телефона',
            'email' => 'E-mail',
            'info' => 'Дополнительная информация',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * {@inheritdoc}
     * @return FeedbackMessageQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new FeedbackMessageQuery(get_called_class());
    }

    /**
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function sendMail() :void
    {
        /** @var SysParamFinder $spFinder */
        $spFinder = \Yii::$container->get('sysParamFinder');
        $site_title = $spFinder->get('site_title');
        \Yii::$app->mailer->compose('feedback', [
                'site_title' => $site_title,
                'message' => $this
            ])
            ->setFrom([$spFinder->get('from_mail') => $site_title])
            ->setTo($spFinder->get('contacts_email'))
            ->setSubject('Сообщение с сайта ' . $site_title)
            ->send();
    }
}
