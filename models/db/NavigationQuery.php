<?php

namespace app\models\db;

use yii\db\ActiveQuery;

/**
 * Class NavigationQuery
 * @package app\models\db
 * @see Navigation
 */
class NavigationQuery extends ActiveQuery implements AdminQueryInterface
{
    /**
     * @return NavigationQuery
     */
    public function forAdmin()
    {
        return $this->addOrderBy([
            'priority' => SORT_ASC
        ]);
    }

    /**
     * @return NavigationQuery
     */
    public function forSite()
    {
        return $this->andWhere(['is_published' => true])
            ->addOrderBy([
            'priority' => SORT_ASC
        ]);
    }
}