<?php

namespace app\models\db;

use app\models\FileModelInterface;
use app\models\FileTrait;

/**
 * This is the model class for table "instruction_file".
 *
 * @property int $id
 * @property int $id_item
 * @property string $name
 * @property string $file_ext
 * @property int $is_published
 * @property int $priority
 *
 * @property InstructionItem $item
 */
class InstructionFile extends BaseActiveRecord implements FileModelInterface
{
    use FileTrait;

    protected $fileConfig = '@app/config/file_settings/instruction_file.php';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'instruction_file';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_item', 'is_published', 'priority'], 'integer'],
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
            [['file_ext'], 'string', 'max' => 6],
            [['id_item'], 'exist', 'skipOnError' => true, 'targetClass' => InstructionItem::class, 'targetAttribute' => ['id_item' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_item' => 'Продукт',
            'name' => 'Название',
            'file_ext' => 'Файл',
            'is_published' => 'Статус',
            'priority' => 'Порядок',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItem()
    {
        return $this->hasOne(InstructionItem::class, ['id' => 'id_item']);
    }

    /**
     * {@inheritdoc}
     * @return InstructionFileQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new InstructionFileQuery(get_called_class());
    }
}
