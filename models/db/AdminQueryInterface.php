<?php

namespace app\models\db;

interface AdminQueryInterface
{
    public function forAdmin();
}