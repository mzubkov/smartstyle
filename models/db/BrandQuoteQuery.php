<?php

namespace app\models\db;

use yii\db\ActiveQuery;

/**
 * This is the ActiveQuery class for [[BrandQuote]].
 *
 * @see BrandQuote
 */
class BrandQuoteQuery extends ActiveQuery implements AdminQueryInterface
{
    /**
     * @return BrandQuoteQuery
     */
    public function forAdmin()
    {
        return $this->addOrderBy([
            'id' => SORT_ASC
        ]);
    }

    /**
     * @return BrandQuoteQuery
     */
    public function forSite()
    {
        return $this->andWhere(['is_published' => true])
            ->addOrderBy([
                'id' => SORT_ASC
            ]);
    }
}