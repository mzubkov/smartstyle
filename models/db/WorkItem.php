<?php

namespace app\models\db;

/**
 * This is the model class for table "work_item".
 *
 * @property int $id
 * @property string $name
 * @property int $is_published
 * @property int $priority
 */
class WorkItem extends BaseActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'work_item';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['is_published', 'priority'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'is_published' => 'Статус',
            'priority' => 'Порядок',
        ];
    }

    /**
     * {@inheritdoc}
     * @return WorkItemQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new WorkItemQuery(get_called_class());
    }
}
