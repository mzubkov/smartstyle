<?php

namespace app\models\db;

use app\components\behaviors\PriorityBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

abstract class BaseActiveRecord extends ActiveRecord
{
    public function init()
    {
        if ($this->hasAttribute('priority')) {
            $this->attachBehavior('priority', [
                    'class' => PriorityBehavior::class,
            ]);
        }

        if ($this->hasAttribute('created_at') && $this->hasAttribute('updated_at')) {
            $this->attachBehavior('timestamp', [
                'class' => TimestampBehavior::class
            ]);
        }
    }
}