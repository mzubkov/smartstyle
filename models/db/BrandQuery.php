<?php

namespace app\models\db;

use yii\db\ActiveQuery;

/**
 * This is the ActiveQuery class for [[Brand]].
 *
 * @see Brand
 */
class BrandQuery extends ActiveQuery implements AdminQueryInterface
{
    /**
     * @return BrandQuery
     */
    public function forAdmin()
    {
        return $this->addOrderBy([
            'priority' => SORT_ASC
        ]);
    }

    /**
     * @return BrandQuery
     */
    public function forSite()
    {
        return $this->andWhere(['is_published' => true])
            ->addOrderBy([
                'priority' => SORT_ASC
            ]);
    }
}