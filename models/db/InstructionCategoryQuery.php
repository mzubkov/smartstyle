<?php

namespace app\models\db;

use yii\db\ActiveQuery;

/**
 * This is the ActiveQuery class for [[InstructionCategory]].
 *
 * @see InstructionCategory
 */
class InstructionCategoryQuery extends ActiveQuery implements AdminQueryInterface
{
    /**
     * @return InstructionCategoryQuery
     */
    public function forAdmin()
    {
        return $this->addOrderBy([
            'priority' => SORT_ASC
        ]);
    }

    /**
     * @return InstructionCategoryQuery
     */
    public function rootCategories()
    {
        return $this->andWhere(['id_parent' => null]);
    }

    /**
     * @return InstructionCategoryQuery
     */
    public function forSite()
    {
        return $this->andWhere(['is_published' => true])
            ->addOrderBy([
                'priority' => SORT_ASC
            ]);
    }
}