<?php

namespace app\models\db;

use app\models\ImageModelInterface;
use app\models\ImageRepository;
use app\models\ImageTrait;
use Zelenin\yii\behaviors\Slug;

/**
 * This is the model class for table "brand".
 *
 * @property int $id
 * @property string $identifier
 * @property string $name
 * @property string $description
 * @property string $content
 * @property string $img_ext
 * @property string $logo_ext
 * @property string $page_img_ext
 * @property int $is_published
 * @property int $priority
 *
 * @property Catalog[] $catalogs
 * @property BrandAdvantage[] $publishedAdvantages
 * @property BrandItem[] $publishedItems
 * @property BrandQuote $publishedQuote
 */
class Brand extends BaseActiveRecord implements ImageModelInterface
{
    use ImageTrait;

    protected $imgConfig = '@app/config/image_settings/brand.php';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'brand';
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'slug' => [
                'class' => Slug::class,
                'slugAttribute' => 'identifier',
                'attribute' => 'name',
                // optional params
                'ensureUnique' => true,
                'replacement' => '-',
                'lowercase' => true,
                'immutable' => false,
                // If intl extension is enabled, see http://userguide.icu-project.org/transforms/general.
                'transliterateOptions' => 'Russian-Latin/BGN; Any-Latin; Latin-ASCII; NFD; [:Nonspacing Mark:] Remove; NFC;'
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['identifier', 'name', 'description'], 'required'],
            [['content'], 'string'],
            [['is_published', 'priority'], 'integer'],
            [['identifier', 'name', 'description'], 'string', 'max' => 255],
            [['img_ext', 'logo_ext', 'page_img_ext'], 'string', 'max' => 6],
            [['identifier'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'identifier' => 'Identifier',
            'name' => 'Название',
            'description' => 'Описание',
            'content' => 'Текст',
            'img_ext' => 'Изображение',
            'logo_ext' => 'Логотип',
            'page_img_ext' => 'Изображение в шапке',
            'is_published' => 'Статус',
            'priority' => 'Порядок',
        ];
    }

    /**
     * {@inheritdoc}
     * @return BrandQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new BrandQuery(get_called_class());
    }

    /**
     * @return ImageRepository
     */
    public function getLogoRepository()
    {
        $params = require(\Yii::getAlias('@app/config/image_settings/brand_logo.php'));
        return new ImageRepository([
            'model' => $this,
            'path' => $params['path'],
            'variants' => $params['variants'],
            'ext_field' => 'logo_ext'
        ]);
    }

    /**
     * @param
     * @return string|null
     */
    public function getLogo()
    {
        if (empty($this->logo_ext)) {
            return null;
        }

        return $this->getLogoRepository()->get('logo');
    }

    /**
     * @return ImageRepository
     */
    public function getPageImageRepository()
    {
        $params = require(\Yii::getAlias('@app/config/image_settings/brand_page_image.php'));
        return new ImageRepository([
            'model' => $this,
            'path' => $params['path'],
            'variants' => $params['variants'],
            'ext_field' => 'page_img_ext'
        ]);
    }

    /**
     * @return string|null
     */
    public function getPageImage()
    {
        if (empty($this->page_img_ext)) {
            return null;
        }

        return $this->getPageImageRepository()->get('page_image');
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return '/' . $this->identifier;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCatalogs()
    {
        return $this->hasMany(Catalog::class, ['id_brand' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPublishedAdvantages()
    {
        return $this->hasMany(BrandAdvantage::class, ['id_brand' => 'id'])->onCondition(['is_published' => true]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPublishedItems()
    {
        return $this->hasMany(BrandItem::class, ['id_brand' => 'id'])->onCondition(['is_published' => true]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPublishedQuote()
    {
        return $this->hasOne(BrandQuote::class, ['id_brand' => 'id'])->onCondition(['is_published' => true]);
    }
}
