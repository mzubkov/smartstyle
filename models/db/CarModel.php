<?php

namespace app\models\db;

use app\models\ImageModelInterface;
use app\models\ImageTrait;

/**
 * This is the model class for table "car_model".
 *
 * @property int $id
 * @property int $id_brand
 * @property string $name
 * @property string $img_ext
 * @property int $is_published
 * @property int $priority
 *
 * @property CarMap[] $maps
 * @property CarMap[] $publishedMaps
 * @property CarBrand $brand
 */
class CarModel extends BaseActiveRecord implements ImageModelInterface
{
    use ImageTrait;

    protected $imgConfig = '@app/config/image_settings/car_model.php';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'car_model';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_brand', 'name'], 'required'],
            [['id_brand', 'is_published', 'priority'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['img_ext'], 'string', 'max' => 6],
            [['id_brand'], 'exist', 'skipOnError' => true, 'targetClass' => CarBrand::class, 'targetAttribute' => ['id_brand' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_brand' => 'Марка автомобиля',
            'name' => 'Название',
            'img_ext' => 'Изображение',
            'is_published' => 'Статус',
            'priority' => 'Порядок',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMaps()
    {
        return $this->hasMany(CarMap::class, ['id_model' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPublishedMaps()
    {
        return $this->hasMany(CarMap::class, ['id_model' => 'id'])->onCondition(['is_published' => 1]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBrand()
    {
        return $this->hasOne(CarBrand::class, ['id' => 'id_brand']);
    }

    /**
     * {@inheritdoc}
     * @return CarModelQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CarModelQuery(get_called_class());
    }
}
