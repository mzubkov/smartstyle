<?php

namespace app\models\db;


/**
 * This is the model class for table "brand_video".
 *
 * @property int $id
 * @property int $id_brand
 * @property string $name
 * @property string $video_code
 * @property int $is_published
 * @property int $priority
 *
 * @property Brand $brand
 */
class BrandVideo extends BaseActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'brand_video';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_brand', 'name', 'video_code'], 'required'],
            [['id_brand', 'is_published', 'priority'], 'integer'],
            [['name', 'video_code'], 'string', 'max' => 255],
            [['id_brand'], 'exist', 'skipOnError' => true, 'targetClass' => Brand::class, 'targetAttribute' => ['id_brand' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_brand' => 'Бренд',
            'name' => 'Заголовок',
            'video_code' => 'Ссылка на видео (youtube)',
            'is_published' => 'Статус',
            'priority' => 'Порядок',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBrand()
    {
        return $this->hasOne(Brand::class, ['id' => 'id_brand']);
    }

    /**
     * {@inheritdoc}
     * @return BrandVideoQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new BrandVideoQuery(get_called_class());
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return "https://www.youtube.com/watch?v={$this->video_code}";
    }

    /**
     * @return string
     */
    public function getVideoPreview()
    {
        return '<iframe width="610" height="315" src="https://www.youtube.com/embed/' . $this->video_code . '?rel=0" frameborder="0" allowfullscreen></iframe>';
    }
}
