<?php

namespace app\models\db;

use yii\db\ActiveQuery;

/**
 * This is the ActiveQuery class for [[InstructionFile]].
 *
 * @see InstructionFile
 */
class InstructionFileQuery extends ActiveQuery implements AdminQueryInterface
{
    /**
     * @return InstructionFileQuery
     */
    public function forAdmin()
    {
        return $this->addOrderBy([
            'priority' => SORT_ASC
        ]);
    }

    /**
     * @return InstructionFileQuery
     */
    public function forSite()
    {
        return $this->andWhere(['is_published' => true])
            ->addOrderBy([
                'priority' => SORT_ASC
            ]);
    }
}
