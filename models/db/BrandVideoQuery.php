<?php

namespace app\models\db;

use yii\db\ActiveQuery;

/**
 * This is the ActiveQuery class for [[BrandVideo]].
 *
 * @see BrandVideo
 */
class BrandVideoQuery extends ActiveQuery implements AdminQueryInterface
{
    /**
     * @return BrandVideoQuery
     */
    public function forAdmin()
    {
        return $this->addOrderBy([
            'priority' => SORT_ASC
        ]);
    }

    /**
     * @return BrandVideoQuery
     */
    public function forSite()
    {
        return $this->andWhere(['is_published' => true])
            ->addOrderBy([
                'priority' => SORT_ASC
            ]);
    }
}
