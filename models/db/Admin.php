<?php

namespace app\models\db;

/**
 * This is the model class for table "admin.user".
 *
 * @property int $id
 * @property string $login
 * @property string $password
 * @property string $auth_key
 * @property string $last_login
 * @property string $ip
 * @property string $first_name
 * @property string $last_name
 * @property string $image
 * @property string $role
 */
class Admin extends BaseActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'admin';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['last_login'], 'safe'],
            [['login', 'password', 'ip', 'first_name', 'last_name', 'image', 'role'], 'string', 'max' => 255],
            [['auth_key'], 'string', 'max' => 32],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'first_name' => 'Имя',
            'last_name' => 'Фамилия',
            'login' => 'Логин',
            'password' => 'Пароль',
            'auth_key' => 'Ключ',
            'last_login' => 'Последний вход',
            'ip' => 'Ip',
            'role' => 'Роль'
        ];
    }

    /**
     * @return string
     */
    public function getFullName()
    {
        return "{$this->last_name} {$this->first_name}";
    }
}
