<?php

namespace app\models\db;

use app\models\ImageModelInterface;
use app\models\ImageTrait;
use Zelenin\yii\behaviors\Slug;

/**
 * This is the model class for table "catalog".
 *
 * @property int $id
 * @property int $id_brand
 * @property string $identifier
 * @property string $name
 * @property string $img_ext
 * @property int $is_published
 * @property int $priority
 *
 * @property Brand $brand
 * @property CatalogProduct[] $products
 * @property CatalogProduct[] publishedProducts
 */
class Catalog extends BaseActiveRecord implements ImageModelInterface
{
    use ImageTrait;

    /** @var integer */
    public $products_count;

    protected $imgConfig = '@app/config/image_settings/catalog.php';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'catalog';
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'slug' => [
                'class' => Slug::class,
                'slugAttribute' => 'identifier',
                'attribute' => 'name',
                // optional params
                'ensureUnique' => true,
                'replacement' => '-',
                'lowercase' => true,
                'immutable' => false,
                // If intl extension is enabled, see http://userguide.icu-project.org/transforms/general.
                'transliterateOptions' => 'Russian-Latin/BGN; Any-Latin; Latin-ASCII; NFD; [:Nonspacing Mark:] Remove; NFC;'
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_brand', 'identifier', 'name'], 'required'],
            [['id_brand', 'is_published', 'priority'], 'integer'],
            [['identifier', 'name'], 'string', 'max' => 255],
            [['img_ext'], 'string', 'max' => 6],
            [['identifier'], 'unique'],
            [['name'], 'unique'],
            [['id_brand'], 'exist', 'skipOnError' => true, 'targetClass' => Brand::class, 'targetAttribute' => ['id_brand' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_brand' => 'Бренд',
            'identifier' => 'Идентификатор',
            'name' => 'Название',
            'img_ext' => 'Изображение',
            'is_published' => 'Статус',
            'priority' => 'Порядок',
        ];
    }

    /**
     * {@inheritdoc}
     * @return CatalogQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CatalogQuery(get_called_class());
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBrand()
    {
        return $this->hasOne(Brand::class, ['id' => 'id_brand']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(CatalogProduct::class, ['id_catalog' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPublishedProducts()
    {
        return $this->hasMany(CatalogProduct::class, ['id_catalog' => 'id'])->onCondition(['is_published' => true]);
    }
}
