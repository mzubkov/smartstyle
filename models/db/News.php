<?php

namespace app\models\db;

use Zelenin\yii\behaviors\Slug;

/**
 * This is the model class for table "news".
 *
 * @property int $id
 * @property int $id_brand
 * @property string $identifier
 * @property string $name
 * @property string $annotation
 * @property string $content
 * @property string $news_date
 * @property int $is_published
 *
 * @property NewsImage[] $images
 * @property Brand $brand
 * @property string $url
 * @property string $date
 */
class News extends BaseActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'news';
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'slug' => [
                'class' => Slug::class,
                'slugAttribute' => 'identifier',
                'attribute' => 'name',
                // optional params
                'ensureUnique' => true,
                'replacement' => '-',
                'lowercase' => true,
                'immutable' => false,
                // If intl extension is enabled, see http://userguide.icu-project.org/transforms/general.
                'transliterateOptions' => 'Russian-Latin/BGN; Any-Latin; Latin-ASCII; NFD; [:Nonspacing Mark:] Remove; NFC;'
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_brand', 'is_published'], 'integer'],
            [['identifier', 'name'], 'required'],
            [['content'], 'string'],
            [['news_date'], 'safe'],
            [['identifier', 'name'], 'string', 'max' => 255],
            [['annotation'], 'string', 'max' => 1024],
            [['identifier'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_brand' => 'Бренд',
            'identifier' => 'Идентификатор',
            'name' => 'Заголовок',
            'annotation' => 'Аннотация',
            'content' => 'Текст',
            'news_date' => 'Дата',
            'is_published' => 'Статус',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getImages()
    {
        return $this->hasMany(NewsImage::class, ['id_news' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBrand()
    {
        return $this->hasOne(Brand::class, ['id' => 'id_brand']);
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        $year = date('Y', strtotime($this->news_date));
        return "/news/{$year}/{$this->identifier}";
    }

    /**
     * @return string
     */
    public function getDate()
    {
        return date('d.m.Y', strtotime($this->news_date));
    }

    /**
     * {@inheritdoc}
     * @return NewsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new NewsQuery(get_called_class());
    }

    public static function getYears()
    {
        $sql = <<<SQL
select distinct extract(year from news_date) as year from news where is_published = 1 order by year desc
SQL;
        return \Yii::$app->db->createCommand($sql)->queryColumn();
    }
}
