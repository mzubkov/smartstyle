<?php

namespace app\models\db;

/**
 * This is the model class for table "footer_menu_item".
 *
 * @property int $id
 * @property int $id_folder
 * @property string $name
 * @property string $url
 * @property int $is_published
 * @property int $priority
 *
 * @property FooterMenuFolder $folder
 */
class FooterMenuItem extends BaseActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'footer_menu_item';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_folder', 'name', 'url'], 'required'],
            [['id_folder', 'is_published', 'priority'], 'integer'],
            [['name', 'url'], 'string', 'max' => 255],
            [['id_folder'], 'exist', 'skipOnError' => true, 'targetClass' => FooterMenuFolder::class, 'targetAttribute' => ['id_folder' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_folder' => 'Раздел',
            'name' => 'Название',
            'url' => 'URL',
            'is_published' => 'Статус',
            'priority' => 'Порядок',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFolder()
    {
        return $this->hasOne(FooterMenuFolder::class, ['id' => 'id_folder']);
    }

    /**
     * {@inheritdoc}
     * @return FooterMenuItemQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new FooterMenuItemQuery(get_called_class());
    }
}
