<?php

namespace app\models\db;

use yii\db\ActiveQuery;

/**
 * This is the ActiveQuery class for [[CarModel]].
 *
 * @see CarModel
 */
class CarModelQuery extends ActiveQuery implements AdminQueryInterface
{
    /**
     * @return CarModelQuery
     */
    public function forAdmin()
    {
        return $this->addOrderBy([
            'priority' => SORT_ASC
        ]);
    }

    /**
     * @return CarModelQuery
     */
    public function forSite()
    {
        return $this->andWhere(['is_published' => true])
            ->addOrderBy([
                'priority' => SORT_ASC
            ]);
    }
}
