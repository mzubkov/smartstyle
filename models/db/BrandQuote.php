<?php

namespace app\models\db;

use app\models\ImageModelInterface;
use app\models\ImageTrait;

/**
 * This is the model class for table "brand_quote".
 *
 * @property int $id
 * @property int $id_brand
 * @property string $text
 * @property string $img_ext
 * @property int $is_published
 *
 * @property Brand $brand
 */
class BrandQuote extends BaseActiveRecord implements ImageModelInterface
{
    use ImageTrait;

    protected $imgConfig = '@app/config/image_settings/brand_quote.php';
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'brand_quote';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_brand'], 'required'],
            [['id_brand', 'is_published'], 'integer'],
            [['text'], 'string'],
            [['img_ext'], 'string', 'max' => 6],
            [['id_brand'], 'exist', 'skipOnError' => true, 'targetClass' => Brand::class, 'targetAttribute' => ['id_brand' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_brand' => 'Бренд',
            'text' => 'Текст',
            'img_ext' => 'Изображение',
            'is_published' => 'Статус',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBrand()
    {
        return $this->hasOne(Brand::class, ['id' => 'id_brand']);
    }

    /**
     * {@inheritdoc}
     * @return BrandQuoteQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new BrandQuoteQuery(get_called_class());
    }
}
