<?php

namespace app\models\db;

/**
 * This is the model class for table "brand_advantage".
 *
 * @property int $id
 * @property int $id_brand
 * @property string $text
 * @property int $is_published
 * @property int $priority
 *
 * @property Brand $brand
 */
class BrandAdvantage extends BaseActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'brand_advantage';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_brand', 'text'], 'required'],
            [['id_brand', 'is_published', 'priority'], 'integer'],
            [['text'], 'string', 'max' => 255],
            [['id_brand'], 'exist', 'skipOnError' => true, 'targetClass' => Brand::class, 'targetAttribute' => ['id_brand' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_brand' => 'Бренд',
            'text' => 'Текст',
            'is_published' => 'Статус',
            'priority' => 'Порядок',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBrand()
    {
        return $this->hasOne(Brand::className(), ['id' => 'id_brand']);
    }

    /**
     * {@inheritdoc}
     * @return BrandAdvantageQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new BrandAdvantageQuery(get_called_class());
    }
}
