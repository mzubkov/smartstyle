<?php

namespace app\models\db;

use yii\db\ActiveQuery;

/**
 * This is the ActiveQuery class for [[BrandAdvantage]].
 *
 * @see BrandAdvantage
 */
class BrandAdvantageQuery extends ActiveQuery implements AdminQueryInterface
{
    /**
     * @return BrandAdvantageQuery
     */
    public function forAdmin()
    {
        return $this->addOrderBy([
            'priority' => SORT_ASC
        ]);
    }

    /**
     * @return BrandAdvantageQuery
     */
    public function forSite()
    {
        return $this->andWhere(['is_published' => true])
            ->addOrderBy([
                'priority' => SORT_ASC
            ]);
    }
}