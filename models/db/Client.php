<?php

namespace app\models\db;

/**
 * This is the model class for table "client".
 *
 * @property int $id
 * @property string $name
 * @property string $login
 * @property string $password
 * @property int $is_published
 * @property string $auth_key
 * @property string $last_login
 * @property string $ip
 */
class Client extends BaseActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'client';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['is_published'], 'integer'],
            [['last_login'], 'safe'],
            [['name', 'login', 'password', 'ip'], 'string', 'max' => 255],
            [['auth_key'], 'string', 'max' => 32],
            [['login'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Имя',
            'login' => 'Логин',
            'password' => 'Пароль',
            'is_published' => 'Активен',
            'auth_key' => 'Auth Key',
            'last_login' => 'Последний вход',
            'ip' => 'IP',
        ];
    }

    /**
     * {@inheritdoc}
     * @return ClientQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ClientQuery(get_called_class());
    }
}
