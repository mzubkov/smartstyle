<?php

namespace app\models\db;

use yii\db\ActiveQuery;

/**
 * This is the ActiveQuery class for [[CatalogProduct]].
 *
 * @see CatalogProduct
 */
class CatalogProductQuery extends ActiveQuery implements AdminQueryInterface
{
    /**
     * @return CatalogProductQuery
     */
    public function forAdmin()
    {
        return $this->addOrderBy([
            'priority' => SORT_ASC
        ]);
    }

    /**
     * @return CatalogProductQuery
     */
    public function forSite()
    {
        return $this->andWhere(['is_published' => true])
            ->addOrderBy([
                'priority' => SORT_ASC
            ]);
    }
}
