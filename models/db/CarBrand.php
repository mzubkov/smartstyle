<?php

namespace app\models\db;

use app\models\ImageModelInterface;
use app\models\ImageTrait;

/**
 * This is the model class for table "car_brand".
 *
 * @property int $id
 * @property string $name
 * @property string $img_ext
 * @property int $is_published
 * @property int $priority
 *
 * @property CarModel[] $Models
 * @property CarModel[] $publishedModels
 */
class CarBrand extends BaseActiveRecord implements ImageModelInterface
{
    use ImageTrait;

    protected $imgConfig = '@app/config/image_settings/car_brand.php';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'car_brand';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['is_published', 'priority'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['img_ext'], 'string', 'max' => 6],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'img_ext' => 'Изображение',
            'is_published' => 'Статус',
            'priority' => 'Порядок',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModels()
    {
        return $this->hasMany(CarModel::class, ['id_brand' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPublishedModels()
    {
        return $this->hasMany(CarModel::class, ['id_brand' => 'id'])->onCondition(['is_published' => 1]);
    }

    /**
     * {@inheritdoc}
     * @return CarBrandQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CarBrandQuery(get_called_class());
    }
}
