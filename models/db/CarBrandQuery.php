<?php

namespace app\models\db;

use yii\db\ActiveQuery;

/**
 * This is the ActiveQuery class for [[CarBrand]].
 *
 * @see CarBrand
 */
class CarBrandQuery extends ActiveQuery implements AdminQueryInterface
{
    /**
     * @return CarBrandQuery
     */
    public function forAdmin()
    {
        return $this->addOrderBy([
            'priority' => SORT_ASC
        ]);
    }

    /**
     * @return CarBrandQuery
     */
    public function forSite()
    {
        return $this->andWhere(['is_published' => true])
            ->addOrderBy([
                'priority' => SORT_ASC
            ]);
    }
}
