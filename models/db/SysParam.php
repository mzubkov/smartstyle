<?php

namespace app\models\db;

/**
 * This is the model class for table "sys_param".
 *
 * @property int $id
 * @property string $name
 * @property string $identifier
 * @property string $value
 */
class SysParam extends BaseActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sys_param';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'identifier', 'value'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'identifier' => 'Identifier',
            'value' => 'Value',
        ];
    }

    /**
     * {@inheritdoc}
     * @return SysParamQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new SysParamQuery(get_called_class());
    }
}
