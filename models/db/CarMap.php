<?php

namespace app\models\db;

use app\models\FileModelInterface;
use app\models\FileTrait;

/**
 * This is the model class for table "car_map".
 *
 * @property int $id
 * @property int $id_model
 * @property string $name
 * @property string $file_ext
 * @property int $is_published
 *
 * @property CarModel $model
 */
class CarMap extends BaseActiveRecord implements FileModelInterface
{
    use FileTrait;

    protected $fileConfig = '@app/config/file_settings/car_map.php';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'car_map';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_model', 'name'], 'required'],
            [['id_model', 'is_published'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['file_ext'], 'string', 'max' => 6],
            [['id_model'], 'exist', 'skipOnError' => true, 'targetClass' => CarModel::class, 'targetAttribute' => ['id_model' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_model' => 'Модель автомобиля',
            'name' => 'Название',
            'file_ext' => 'Файл',
            'is_published' => 'Статус',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModel()
    {
        return $this->hasOne(CarModel::class, ['id' => 'id_model']);
    }

    /**
     * {@inheritdoc}
     * @return CarMapQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CarMapQuery(get_called_class());
    }
}
