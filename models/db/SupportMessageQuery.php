<?php

namespace app\models\db;

/**
 * This is the ActiveQuery class for [[SupportMessage]].
 *
 * @see SupportMessage
 */
class SupportMessageQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return SupportMessage[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return SupportMessage|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
