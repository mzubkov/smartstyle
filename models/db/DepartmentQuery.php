<?php

namespace app\models\db;

use yii\db\ActiveQuery;

/**
 * This is the ActiveQuery class for [[Department]].
 *
 * @see Department
 */
class DepartmentQuery extends ActiveQuery implements AdminQueryInterface
{
    /**
     * @return DepartmentQuery
     */
    public function forAdmin()
    {
        return $this->addOrderBy([
            'id' => SORT_ASC
        ]);
    }

    /**
     * @return DepartmentQuery
     */
    public function forSite()
    {
        return $this->andWhere(['is_published' => true]);
    }
}
