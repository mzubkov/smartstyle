<?php

namespace app\models\db;

use Yii;

/**
 * This is the model class for table "instruction_category".
 *
 * @property int $id
 * @property int $id_parent
 * @property string $name
 * @property int $is_published
 * @property int $priority
 *
 * @property InstructionCategory $parent
 * @property InstructionCategory[] $categories
 * @property InstructionItem[] $items
 */
class InstructionCategory extends BaseActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'instruction_category';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_parent', 'is_published', 'priority'], 'integer'],
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
            [['id_parent'], 'exist', 'skipOnError' => true, 'targetClass' => InstructionCategory::class, 'targetAttribute' => ['id_parent' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_parent' => 'Родительская категория',
            'name' => 'Название',
            'is_published' => 'Статус',
            'priority' => 'Порядок',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParent()
    {
        return $this->hasOne(InstructionCategory::class, ['id' => 'id_parent']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategories()
    {
        return $this->hasMany(InstructionCategory::class, ['id_parent' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItems()
    {
        return $this->hasMany(InstructionItem::class, ['id_category' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return InstructionCategoryQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new InstructionCategoryQuery(get_called_class());
    }
}
