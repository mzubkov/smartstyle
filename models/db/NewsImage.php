<?php

namespace app\models\db;

use app\models\ImageModelInterface;
use app\models\ImageTrait;

/**
 * This is the model class for table "news_image".
 *
 * @property int $id
 * @property int $id_news
 * @property string $name
 * @property string $img_ext
 * @property int $is_published
 * @property int $priority
 *
 * @property News $news
 */
class NewsImage extends BaseActiveRecord implements ImageModelInterface
{
    use ImageTrait;

    protected $imgConfig = '@app/config/image_settings/news_image.php';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'news_image';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_news'], 'required'],
            [['id_news', 'is_published', 'priority'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['img_ext'], 'string', 'max' => 6],
            [['id_news'], 'exist', 'skipOnError' => true, 'targetClass' => News::class, 'targetAttribute' => ['id_news' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_news' => 'Новость',
            'name' => 'Название',
            'img_ext' => 'Изображение',
            'is_published' => 'Статус',
            'priority' => 'Порядок',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNews()
    {
        return $this->hasOne(News::class, ['id' => 'id_news']);
    }

    /**
     * {@inheritdoc}
     * @return NewsImageQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new NewsImageQuery(get_called_class());
    }
}
