<?php

namespace app\models\db;

use app\models\ImageModelInterface;
use app\models\ImageTrait;

/**
 * This is the model class for table "partner".
 *
 * @property int $id
 * @property string $name
 * @property string $img_ext
 * @property int $is_published
 * @property int $priority
 */
class Partner extends BaseActiveRecord implements ImageModelInterface
{
    use ImageTrait;

    protected $imgConfig = '@app/config/image_settings/partner.php';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'partner';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['is_published', 'priority'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['img_ext'], 'string', 'max' => 6],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'img_ext' => 'Изображение',
            'is_published' => 'Статус',
            'priority' => 'Порядок',
        ];
    }

    /**
     * {@inheritdoc}
     * @return PartnerQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PartnerQuery(get_called_class());
    }
}
