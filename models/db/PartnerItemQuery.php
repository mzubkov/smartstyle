<?php

namespace app\models\db;

use yii\db\ActiveQuery;

/**
 * This is the ActiveQuery class for [[PartnerItem]].
 *
 * @see PartnerItem
 */
class PartnerItemQuery extends ActiveQuery implements AdminQueryInterface
{
    /**
     * @return PartnerItemQuery
     */
    public function forAdmin()
    {
        return $this->addOrderBy([
            'priority' => SORT_ASC
        ]);
    }

    /**
     * @return PartnerItemQuery
     */
    public function forSite()
    {
        return $this->andWhere(['is_published' => true])
            ->addOrderBy([
                'priority' => SORT_ASC
            ]);
    }
}
