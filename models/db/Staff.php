<?php

namespace app\models\db;

use app\models\ImageTrait;
use app\models\ImageModelInterface;

/**
 * This is the model class for table "staff".
 *
 * @property int $id
 * @property int $id_department
 * @property string $name
 * @property string $post
 * @property string $phone
 * @property string $img_ext
 * @property int $is_published
 *
 * @property Department $department
 * @property string $image
 */
class Staff extends BaseActiveRecord implements ImageModelInterface
{
    use ImageTrait;

    protected $imgConfig = '@app/config/image_settings/staff.php';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'staff';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_department'], 'required'],
            [['id_department'], 'integer'],
            [['is_published'], 'integer'],
            [['name', 'post', 'phone'], 'string', 'max' => 255],
            [['img_ext'], 'string', 'max' => 6],
            [['id_department'], 'exist', 'skipOnError' => true, 'targetClass' => Department::class, 'targetAttribute' => ['id_department' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_department' => 'Отдел',
            'name' => 'Имя',
            'post' => 'Должность',
            'phone' => 'Телефон',
            'img_ext' => 'Изображение',
            'is_published' => 'Статус',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDepartment()
    {
        return $this->hasOne(Department::class, ['id' => 'id_department']);
    }

    /**
     * {@inheritdoc}
     * @return StaffQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new StaffQuery(get_called_class());
    }
}
