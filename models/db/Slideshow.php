<?php

namespace app\models\db;

use app\models\ImageModelInterface;
use app\models\ImageTrait;

/**
 * This is the model class for table "slideshow".
 *
 * @property int $id
 * @property string $name
 * @property string $img_ext
 * @property int $is_published
 * @property int $priority
 * @property int $position
 */
class Slideshow extends BaseActiveRecord implements ImageModelInterface
{
    const
        POSITION_HEADER = 0,
        POSITION_CENTER = 1,
        POSITION_BOTTOM = 2
    ;

    use ImageTrait;

    protected $imgConfig = '@app/config/image_settings/slideshow.php';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'slideshow';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['is_published', 'priority', 'position'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['img_ext'], 'string', 'max' => 6],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'position' => 'Позиция',
            'name' => 'Название',
            'img_ext' => 'Изображение',
            'is_published' => 'Статус',
            'priority' => 'Приоритет',
        ];
    }

    /**
     * {@inheritdoc}
     * @return SlideshowQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new SlideshowQuery(get_called_class());
    }

    /**
     * @return string
     */
    public function getPositionName()
    {
        switch ($this->position) {
            case self::POSITION_HEADER:
                return 'Вверху страницы';

            case self::POSITION_CENTER:
                return 'Центр';

            case self::POSITION_BOTTOM:
                return 'Низ страницы';
        }
    }
}
