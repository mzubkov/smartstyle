<?php

namespace app\models\db;

use app\models\SendMailInterface;
use app\models\SysParamFinder;

/**
 * This is the model class for table "callback_message".
 *
 * @property int $id
 * @property string $name
 * @property string $phone
 * @property int $created_at
 * @property int $updated_at
 */
class CallbackMessage extends BaseActiveRecord implements SendMailInterface
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'callback_message';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'phone'], 'required'],
            [['created_at', 'updated_at'], 'integer'],
            [['name', 'phone'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Имя',
            'phone' => 'Телефон',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * {@inheritdoc}
     * @return CallbackMessageQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CallbackMessageQuery(get_called_class());
    }

    /**
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function sendMail() :void
    {
        /** @var SysParamFinder $spFinder */
        $spFinder = \Yii::$container->get('sysParamFinder');
        $site_title = $spFinder->get('site_title');
        \Yii::$app->mailer->compose('callback', [
                'site_title' => $site_title,
                'message' => $this
            ])
            ->setFrom([$spFinder->get('from_mail') => $site_title])
            ->setTo($spFinder->get('contacts_email'))
            ->setSubject('Сообщение с сайта ' . $site_title)
            ->send();
    }
}
