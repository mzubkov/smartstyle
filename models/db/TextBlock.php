<?php

namespace app\models\db;

/**
 * This is the model class for table "text_block".
 *
 * @property int $id
 * @property string $identifier
 * @property string $name
 * @property string $content
 * @property string $column_type
 */
class TextBlock extends BaseActiveRecord
{
    const
        TYPE_WYSIWYG = 'wysiwyg',
        TYPE_INPUT = 'input',
        TYPE_TEXTAREA = 'textarea'
    ;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'text_block';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['identifier', 'name'], 'required'],
            [['content'], 'string'],
            [['column_type'], 'string'],
            [['identifier', 'name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'identifier' => 'Идентификатор',
            'name' => 'Название',
            'content' => 'Текст',
            'column_type' => 'Тип',
        ];
    }

    /**
     * {@inheritdoc}
     * @return TextBlockQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TextBlockQuery(get_called_class());
    }
}
