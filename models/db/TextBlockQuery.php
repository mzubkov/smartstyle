<?php

namespace app\models\db;

/**
 * This is the ActiveQuery class for [[TextBlock]].
 *
 * @see TextBlock
 */
class TextBlockQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return TextBlock[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return TextBlock|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
