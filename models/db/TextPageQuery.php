<?php

namespace app\models\db;

use yii\db\ActiveQuery;

/**
 * This is the ActiveQuery class for [[TextPage]].
 *
 * @see TextPage
 */
class TextPageQuery extends ActiveQuery implements AdminQueryInterface
{
    /**
     * @return TextPageQuery
     */
    public function forAdmin()
    {
        return $this->addOrderBy([
            'id' => SORT_ASC
        ]);
    }

    /**
     * @return TextPageQuery
     */
    public function forSite()
    {
        return $this->andWhere(['is_published' => true]);
    }
}
