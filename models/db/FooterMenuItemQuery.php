<?php

namespace app\models\db;

use yii\db\ActiveQuery;

/**
 * This is the ActiveQuery class for [[FooterMenuItem]].
 *
 * @see FooterMenuItem
 */
class FooterMenuItemQuery extends ActiveQuery implements AdminQueryInterface
{
    /**
     * @return FooterMenuItemQuery
     */
    public function forAdmin()
    {
        return $this->addOrderBy([
            'id' => SORT_ASC
        ]);
    }

    /**
     * @return FooterMenuItemQuery
     */
    public function forSite()
    {
        return $this->andWhere(['is_published' => true]);
    }
}
