<?php

namespace app\models\db;

use app\models\ImageModelInterface;
use app\models\ImageTrait;

/**
 * This is the model class for table "sell_item".
 *
 * @property int $id
 * @property string $text
 * @property string $img_ext
 * @property int $is_published
 * @property int $priority
 */
class SellItem extends BaseActiveRecord implements ImageModelInterface
{
    use ImageTrait;

    protected $imgConfig = '@app/config/image_settings/sell_item.php';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sell_item';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['is_published', 'priority'], 'integer'],
            [['text'], 'string', 'max' => 255],
            [['img_ext'], 'string', 'max' => 6],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'text' => 'Текст',
            'img_ext' => 'Изображение',
            'is_published' => 'Статус',
            'priority' => 'Порядок',
        ];
    }

    /**
     * {@inheritdoc}
     * @return SellItemQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new SellItemQuery(get_called_class());
    }
}
