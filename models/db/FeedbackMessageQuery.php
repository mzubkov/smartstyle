<?php

namespace app\models\db;

/**
 * This is the ActiveQuery class for [[FeedbackMessage]].
 *
 * @see FeedbackMessage
 */
class FeedbackMessageQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return FeedbackMessage[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return FeedbackMessage|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
