<?php

namespace app\models\db;

use app\models\ImageModelInterface;
use app\models\ImageTrait;

/**
 * This is the model class for table "catalog_product".
 *
 * @property int $id
 * @property int $id_catalog
 * @property string $name
 * @property string $img_ext
 * @property int $is_published
 * @property int $priority
 *
 * @property Catalog $catalog
 */
class CatalogProduct extends BaseActiveRecord implements ImageModelInterface
{
    use ImageTrait;

    protected $imgConfig = '@app/config/image_settings/catalog_product.php';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'catalog_product';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_catalog', 'name'], 'required'],
            [['id_catalog', 'is_published', 'priority'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['img_ext'], 'string', 'max' => 6],
            [['id_catalog'], 'exist', 'skipOnError' => true, 'targetClass' => Catalog::class, 'targetAttribute' => ['id_catalog' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_catalog' => 'Каталог',
            'name' => 'Название',
            'img_ext' => 'Изображение',
            'is_published' => 'Статус',
            'priority' => 'Порядок',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCatalog()
    {
        return $this->hasOne(Catalog::class, ['id' => 'id_catalog']);
    }

    /**
     * {@inheritdoc}
     * @return CatalogProductQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CatalogProductQuery(get_called_class());
    }
}
