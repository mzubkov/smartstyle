<?php

namespace app\models\db;

use app\models\SendMailInterface;
use app\models\SysParamFinder;

/**
 * This is the model class for table "support_message".
 *
 * @property int $id
 * @property string $text
 * @property string $contact
 * @property int $created_at
 * @property int $updated_at
 */
class SupportMessage extends BaseActiveRecord implements SendMailInterface
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'support_message';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['text', 'contact'], 'required'],
            [['text'], 'string'],
            [['created_at', 'updated_at'], 'integer'],
            [['contact'], 'string', 'max' => 512],
        ];
    }

    public function init()
    {
        parent::init();
        $this->on(self::EVENT_AFTER_INSERT, [$this, 'sendMail']);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'text' => 'Сообщение',
            'contact' => 'Контакты',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * {@inheritdoc}
     * @return SupportMessageQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new SupportMessageQuery(get_called_class());
    }

    /**
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function sendMail() :void
    {
        /** @var SysParamFinder $spFinder */
        $spFinder = \Yii::$container->get('sysParamFinder');
        $site_title = $spFinder->get('site_title');
        \Yii::$app->mailer->compose('support', [
                'site_title' => $site_title,
                'message' => $this
            ])
            ->setFrom([$spFinder->get('from_mail') => $site_title])
            ->setTo($spFinder->get('contacts_email'))
            ->setSubject('Сообщение с сайта ' . $site_title)
            ->send();
    }
}
