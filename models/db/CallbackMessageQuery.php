<?php

namespace app\models\db;

/**
 * This is the ActiveQuery class for [[CallbackMessage]].
 *
 * @see CallbackMessage
 */
class CallbackMessageQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return CallbackMessage[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return CallbackMessage|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
