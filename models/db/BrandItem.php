<?php

namespace app\models\db;

use app\models\ImageModelInterface;
use app\models\ImageTrait;

/**
 * This is the model class for table "brand_item".
 *
 * @property int $id
 * @property int $id_brand
 * @property string $name
 * @property string $text
 * @property string $img_ext
 * @property int $is_published
 * @property int $priority
 *
 * @property Brand $brand
 */
class BrandItem extends BaseActiveRecord implements ImageModelInterface
{
    use ImageTrait;

    protected $imgConfig = '@app/config/image_settings/brand_item.php';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'brand_item';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_brand', 'name', 'text'], 'required'],
            [['id_brand', 'is_published', 'priority'], 'integer'],
            [['name', 'text'], 'string', 'max' => 255],
            [['img_ext'], 'string', 'max' => 6],
            [['id_brand'], 'exist', 'skipOnError' => true, 'targetClass' => Brand::class, 'targetAttribute' => ['id_brand' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_brand' => 'Бренд',
            'name' => 'Название',
            'text' => 'Текст',
            'img_ext' => 'Изображение',
            'is_published' => 'Статус',
            'priority' => 'Порядок',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBrand()
    {
        return $this->hasOne(Brand::class, ['id' => 'id_brand']);
    }

    /**
     * {@inheritdoc}
     * @return BrandItemQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new BrandItemQuery(get_called_class());
    }
}
