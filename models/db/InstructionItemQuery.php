<?php

namespace app\models\db;

use yii\db\ActiveQuery;

/**
 * This is the ActiveQuery class for [[InstructionItem]].
 *
 * @see InstructionItem
 */
class InstructionItemQuery extends ActiveQuery implements AdminQueryInterface
{
    /**
     * @return InstructionItemQuery
     */
    public function forAdmin()
    {
        return $this->addOrderBy([
            'priority' => SORT_ASC
        ]);
    }

    /**
     * @return InstructionItemQuery
     */
    public function forSite()
    {
        return $this->andWhere(['is_published' => true])
            ->addOrderBy([
                'priority' => SORT_ASC
            ]);
    }
}