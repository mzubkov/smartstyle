<?php

namespace app\models\db;

/**
 * This is the model class for table "map_item".
 *
 * @property int $id
 * @property int $number
 * @property string $text
 * @property int $is_published
 * @property int $priority
 */
class MapItem extends BaseActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'map_item';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['number', 'text'], 'required'],
            [['number', 'is_published', 'priority'], 'integer'],
            [['text'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'number' => 'Цифра',
            'text' => 'Текст',
            'is_published' => 'Статус',
            'priority' => 'Порядок',
        ];
    }

    /**
     * {@inheritdoc}
     * @return MapItemQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new MapItemQuery(get_called_class());
    }
}
