<?php

namespace app\models\db;

use yii\db\ActiveQuery;

/**
 * This is the ActiveQuery class for [[NewsImage]].
 *
 * @see NewsImage
 */
class NewsImageQuery extends ActiveQuery implements AdminQueryInterface
{
    /**
     * @return NewsImageQuery
     */
    public function forAdmin()
    {
        return $this->addOrderBy([
            'priority' => SORT_ASC
        ]);
    }

    /**
     * @return NewsImageQuery
     */
    public function forSite()
    {
        return $this->andWhere(['is_published' => true])
            ->addOrderBy([
                'priority' => SORT_ASC
            ]);
    }

    /**
     * @param News $news
     * @return NewsImageQuery
     */
    public function forNews(News $news)
    {
        return $this->andWhere(['id_news' => $news->id]);
    }
}
