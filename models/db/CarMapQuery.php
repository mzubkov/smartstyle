<?php

namespace app\models\db;

use yii\db\ActiveQuery;

/**
 * This is the ActiveQuery class for [[CarMap]].
 *
 * @see CarMap
 */
class CarMapQuery extends ActiveQuery implements AdminQueryInterface
{
    /**
     * @return CarMapQuery
     */
    public function forAdmin()
    {
        return $this->addOrderBy([
            'name' => SORT_ASC
        ]);
    }

    /**
     * @return CarMapQuery
     */
    public function forSite()
    {
        return $this->andWhere(['is_published' => true])
            ->addOrderBy([
                'name' => SORT_ASC
            ]);
    }
}
