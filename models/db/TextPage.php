<?php

namespace app\models\db;

/**
 * This is the model class for table "text_page".
 *
 * @property int $id
 * @property string $identifier
 * @property string $name
 * @property string $content
 * @property int $is_published
 */
class TextPage extends BaseActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'text_page';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['identifier'], 'required'],
            [['content'], 'string'],
            [['is_published'], 'boolean'],
            [['identifier', 'name'], 'string', 'max' => 255],
            [['identifier'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'identifier' => 'Идентификатор',
            'name' => 'Название',
            'content' => 'Текст',
            'is_published' => 'Статус'
        ];
    }

    /**
     * {@inheritdoc}
     * @return TextPageQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TextPageQuery(get_called_class());
    }
}
