<?php

namespace app\models\db;

use yii\db\ActiveQuery;

/**
 * This is the ActiveQuery class for [[WorkItem]].
 *
 * @see WorkItem
 */
class WorkItemQuery extends ActiveQuery implements AdminQueryInterface
{
    /**
     * @return WorkItemQuery
     */
    public function forAdmin()
    {
        return $this->addOrderBy([
            'priority' => SORT_ASC
        ]);
    }

    /**
     * @return WorkItemQuery
     */
    public function forSite()
    {
        return $this->andWhere(['is_published' => true])
            ->addOrderBy([
                'priority' => SORT_ASC
            ]);
    }
}
