<?php

namespace app\models\db;

use yii\db\ActiveQuery;
use yii\db\Expression;

/**
 * This is the ActiveQuery class for [[News]].
 *
 * @see News
 */
class NewsQuery extends ActiveQuery implements AdminQueryInterface
{
    /**
     * @return NewsQuery
     */
    public function forAdmin()
    {
        return $this->addOrderBy([
            'news_date' => SORT_DESC
        ]);
    }

    /**
     * @return NewsQuery
     */
    public function forSite()
    {
        return $this->andWhere(['is_published' => true])
            ->addOrderBy([
                'news_date' => SORT_DESC
            ]);
    }

    /**
     * @param $year
     * @return NewsQuery
     */
    public function forYear($year)
    {
        return $this->andWhere(new Expression("extract(year from news_date) = :year", ['year' => $year]));
    }

    /**
     * @param int $limit
     * @return NewsQuery
     */
    public function last($limit = 2)
    {
        return $this->limit($limit);
    }
}