<?php

namespace app\models\db;

use yii\db\ActiveQuery;

/**
 * This is the ActiveQuery class for [[Catalog]].
 *
 * @see Catalog
 */
class CatalogQuery extends ActiveQuery implements AdminQueryInterface
{
    /**
     * @return CatalogQuery
     */
    public function forAdmin()
    {
        return $this->addOrderBy([
            'priority' => SORT_ASC
        ]);
    }

    /**
     * @return CatalogQuery
     */
    public function forSite()
    {
        return $this->andWhere(['is_published' => true])
            ->addOrderBy([
                'priority' => SORT_ASC
            ]);
    }

    public function withPublishedProductsCount($id_brand)
    {
        $sql = <<<SQL
select 
	c.*, 
    count(*) as products_count
from catalog as c 
join catalog_product as cp on cp.id_catalog = c.id and cp.is_published = 1
where 
	id_brand = :id_brand 
    and c.is_published = 1 
group by c.id
SQL;

        $this->sql = $sql;
        $this->params(['id_brand' => $id_brand]);

        return $this;
    }
}