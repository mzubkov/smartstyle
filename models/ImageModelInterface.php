<?php

namespace app\models;

interface ImageModelInterface
{
    /**
     * @return ImageRepository
     */
    public function getImageRepository();

    /**
     * @param string $variant
     * @return string|null
     */
    public function getImage(string $variant);
}