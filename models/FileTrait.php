<?php

namespace app\models;

trait FileTrait
{
    /**
     * @return FileRepository
     */
    public function getFileRepository()
    {
        $params = require(\Yii::getAlias($this->fileConfig));
        return new FileRepository([
            'model' => $this,
            'path' => $params['path']
        ]);
    }

    /**
     * @return string|null
     */
    public function getFile()
    {
        if (empty($this->file_ext)) {
            return null;
        }

        return $this->getFileRepository()->get();
    }
}