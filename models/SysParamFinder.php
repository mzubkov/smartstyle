<?php

namespace app\models;

use app\models\db\SysParam;
use app\models\db\TextBlock;

/**
 * Class SysParamFinder
 * @package app\models
 */
class SysParamFinder
{
    /** @var SysParam[]  */
    private $sysParams;

    /**
     * SysParamFinder constructor.
     */
    public function __construct()
    {
        $this->init();
    }

    /**
     *
     */
    protected function init()
    {
        $params = require_once(\Yii::getAlias('@app/config/settings/sys_param.php'));
        $dbParams = SysParam::find()->indexBy('identifier')->all();

        $models = [];
        foreach ($params as $identifier => $param) {
            if (isset($dbParams[$identifier])) {
                $models[$identifier] = $dbParams[$identifier];
            } else {
                $models[$identifier] = new SysParam([
                    'identifier' => $identifier,
                    'name' => $param['name'],
                    'value' => $param['value']
                ]);
            }
        }

        $this->sysParams = $models;
    }

    /**
     * @param $identifier
     * @return string
     */
    public function get($identifier)
    {
        return $this->sysParams[$identifier]->value;
    }

    /**
     * @return array
     */
    public function getAll()
    {
        return $this->sysParams;
    }
}