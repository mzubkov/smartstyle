<?php

namespace app\models;

use app\models\db\TextBlock;

/**
 * Class TextBlockFinder
 * @package app\models
 */
class TextBlockFinder
{
    /** @var TextBlock[]  */
    private $textBlocks;

    /**
     * TextBlockFinder constructor.
     */
    public function __construct()
    {
        $this->init();
    }

    /**
     *
     */
    protected function init()
    {
        $blocks = require_once(\Yii::getAlias('@app/config/settings/text_blocks.php'));
        $dbBlocks = TextBlock::find()->indexBy('identifier')->all();

        $models = [];
        foreach ($blocks as $identifier => $block) {
            if (isset($dbBlocks[$identifier])) {
                $models[$identifier] = $dbBlocks[$identifier];
            } else {
                $models[$identifier] = new TextBlock([
                    'identifier' => $identifier,
                    'name' => $block['name'],
                    'content' => $block['content'],
                    'column_type' => $block['column_type']
                ]);
            }
        }
        $this->textBlocks = $models;
    }

    /**
     * @param $identifier
     * @return string
     */
    public function get($identifier)
    {
        return $this->textBlocks[$identifier]->content;
    }

    /**
     * @return TextBlock[]
     */
    public function getAll()
    {
        return $this->textBlocks;
    }
}