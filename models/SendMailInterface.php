<?php

namespace app\models;

/**
 * Interface SendMailInterface
 * @package app\models
 */
interface SendMailInterface
{
    public function sendMail() :void;
}