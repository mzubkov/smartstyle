<?php

namespace app\models;

interface FileModelInterface
{
    /**
     * @return FileRepository
     */
    public function getFileRepository();

    /**
     * @return string|null
     */
    public function getFile();
}