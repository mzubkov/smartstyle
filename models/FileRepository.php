<?php

namespace app\models;

use yii\base\Model;

class FileRepository extends Model
{
    /** @var mixed */
    public $model;

    /** @var string */
    public $path;

    /** @var string */
    public $ext_field = 'file_ext';

    /**
     * @param $variant
     * @param $ext
     * @return string
     */
    public function getFileName($ext)
    {
        return "{$this->model->id}.{$ext}";
    }

    /**
     * @param $variant
     * @return string
     */
    public function get()
    {
        $ext_field = $this->ext_field;
        return "{$this->path}/{$this->model->id}.{$this->model->$ext_field}";
    }
}