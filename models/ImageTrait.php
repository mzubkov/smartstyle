<?php

namespace app\models;

trait ImageTrait
{
    /**
     * @return ImageRepository
     */
    public function getImageRepository()
    {
        $params = require(\Yii::getAlias($this->imgConfig));
        return new ImageRepository([
            'model' => $this,
            'path' => $params['path'],
            'variants' => $params['variants']
        ]);
    }

    /**
     * @param string $variant
     * @return string|null
     */
    public function getImage(string $variant)
    {
        if (empty($this->img_ext)) {
            return null;
        }

        return $this->getImageRepository()->get($variant);
    }
}